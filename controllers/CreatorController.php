<?php
  abstract class CreatorController extends Controller {
    protected $step = NULL; // set this in child class
    protected $assets = array();

    # pages navigation, prevous step and next step
    protected $previous_step;
    protected $next_step;
    protected $this_step;

    public function __construct($step) {
      $this -> step = $step;

      # check if we are editing
      if (isset($_SESSION["$step"]) && !empty($_SESSION["$step"])) {
        $this -> unserializeObjects(); // load objects to internal data structure
        $this -> recoverForEdit();
        unset($_SESSION["$step"]);
        $this -> displayView($this -> view);
      } else {
        $this -> init();
      }
    }

    protected function recoverForEdit() {
      $rowCount = count($this -> assets);
      $this -> data['smarty_row_count'] = $rowCount;

      $key = 0;
      foreach ($this -> assets as $asset) {
        foreach ($asset::$fieldset as $field) {
          $this -> data["smarty_$field"][$key] = $asset -> $field;
        }

        $key++;
      }
    }

    protected function postToSmartyVars() {
      $step = $this -> this_step;
      foreach ($step::$fieldset as $field) {
        foreach ($_POST['id'] as $key => $value) {
          $this -> data["smarty_$field"][$key] = $_POST["$field"][$key];
        }
      }
    }

    protected function validationErrorsToSmartyVars($assets) {
      $step = $this -> this_step;

      foreach ($step::$fieldset as $field) {
        $validation_field_name = "validated_$field";
        foreach ($_POST['id'] as $key => $value) {
          $this -> data["smarty_validation_$field"][$key] = $assets[$key] -> $validation_field_name;
        }
      }
    }

    # load from memory
    # in case we're editing previously saved step
    public function unserializeObjects() {
      $step = $this -> step;
      if ($step) {
        $step = $this -> step;
        foreach ($_SESSION["$step"] as $objectToUnserialize) {
          array_push($this -> assets, unserialize($objectToUnserialize));
        }
        unset($_SESSION["$step"]);
      } else {
        die('Step is not set for CreatorController!'); // step we're currently in must be set!
      }
    }

    # save objects to memory for later use
    public function serializeObjects() {
      $step = $this -> step;
      if ($step) {
        $_SESSION["$step"] = array();
        foreach ($this -> assets as $objectToSerialize) {
          array_push($_SESSION["$step"], serialize($objectToSerialize));
        }
      } else {
        die('Step is not set for CreatorController!');
      }
    }

    function loadFormData() {
      $assets = array();
      # creating objects & setting it's properties
      foreach ($_POST['id'] as $key => $value) {
        $asset = CreatorAssetFactory::create($this -> this_step);
        foreach ($asset::$fieldset as $field) {
          $asset -> $field = $_POST["$field"][$key];
        }

        # push into temporary array
        array_push($assets, $asset);
      } // here are all objects in temporary array and it's properties set

      # set the row count for the view
      $rowCount = count($_POST['id']);
      $this -> data['smarty_row_count'] = ($rowCount > 1) ? $rowCount : 1;

      return $assets;
    }

    function checkBackButton() {
      if (isset($_POST['creator_form_back'])) {
        $this -> assets = $this -> loadFormData($_POST);
        $this -> serializeObjects();
        $this -> redirectToUrl('cv/' . $this -> previous_step);
      }
    }

    function parseParameters($parameters) {
      $this -> init();
    }

    abstract function init();
  }
?>
