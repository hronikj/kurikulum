<?php
  class CreatorControllerTemplate extends CreatorController {
    protected $previous_step = 'hobbies';
    protected $next_step = 'sumarization';
    protected $this_step = 'Template';
    protected $view = 'creator_template';

    private $templates_array;

    function setViewVariables() {
    }

    function __construct() {
      # this is here so that data from DB is loaded everytime when advancing from previous step
      $this -> loadTemplatesFromDb();
      $this -> data['smarty_templates_array'] = $this -> templates_array;
      $this -> step = $this -> this_step;
      $this -> init();
    }

    function init() {
      $this -> loadTemplatesFromDb();
      $this -> data['smarty_templates_array'] = $this -> templates_array;

      $this -> checkBackButton();

      $assets = array();

      $this -> data['smarty_row_count'] = 1;

      if (isset($_POST['id'])) {
        $assets = $this -> loadFormData();

        # set POST variables to the form
        $this -> postToSmartyVars();

        # find out whether there is at least one invalid field
        $formIsValid = TRUE;
        foreach ($assets as $asset) {
          if (!$asset -> isValid()) {
            $formIsValid = FALSE;
            break;
          }
        }

        if ($formIsValid) {
          # serialize and continue with next step
          $this -> assets = $assets;
          $this -> serializeObjects();
          $this -> redirectToUrl('cv/' . $this -> next_step);
        } else {
          # form is invalid
          $this -> validationErrorsToSmartyVars($assets);
        }

      } else { // $_POST is not set
        # just display form
        $this -> data['smarty_row_count'] = 1;
      }

      $this -> displayView();
    }

    public function loadTemplatesFromDb() {
      $templates = dibi::query('SELECT * FROM [cv_templates]') -> fetchAll('id');
      $templates_array = array();

      foreach ($templates as $key => $value) {
        $templates_array[$key]['id'] = $templates[$key] -> id;
        $templates_array[$key]['thumbnail'] = $templates[$key] -> thumbnail;
        $templates_array[$key]['name'] = $templates[$key] -> name;
      }

      $this -> templates_array = $templates_array;
    }

  }
?>
