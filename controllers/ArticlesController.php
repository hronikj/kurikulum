<?php
  class ArticlesController extends Controller {
    public function parseParameters($parameters) {
      $message = new Message();
      $session = new Session();

      if (!empty($parameters[0])) {
        $category = new Category();
        if ($category -> exists($parameters[0])) {

          if (!empty($parameters[1])) {               // category exists
            $article = new Article();

            if ($article -> exists($parameters[1])) {
              $visibility = $article -> checkPermissions($parameters[1]);
              $display_article = FALSE;

              if ($visibility == 0) {
                $display_article = TRUE;
              } elseif ($visibility == 1 && $session -> isLoggedIn()) {
                $display_article = TRUE;
              } else $this -> view = 'article_no_access';

              if ($display_article) {
                $article = $article -> get($article -> getArticleId($parameters[1]));
                $this -> data['smarty_article_headline'] = $article['headline'];
                $this -> data['smarty_article_perex']    = $article['perex'];
                $this -> data['smarty_article_text']     = $article['text'];
                $this -> data['smarty_article_category'] = $parameters[0];

                $this -> setHeader($article['headline'], $article['keywords'], $article['description']);
                $this -> view = 'article';
              }

            } else $this -> redirectToUrl('NotFound');    // article with that id doesn't exist
          } else {                                        // display category
            $article = new Article();
            $includeRestrictedArticles = ($session -> isLoggedIn()) ? TRUE : FALSE;
            $articleList = $article -> getListFromCategory($parameters[0], $includeRestrictedArticles);

            $smarty_article_list = array();

            foreach ($articleList as $rowId => $rowObject) {
              $smarty_article_list[$rowId]['rewrite_url'] = $rowObject -> rewrite_url;
              $smarty_article_list[$rowId]['perex']       = $rowObject -> perex;
              $smarty_article_list[$rowId]['headline']    = $rowObject -> headline;
            }

            $this -> data['smarty_category'] = $parameters[0];
            $this -> data['smarty_article_list'] = $smarty_article_list;
            $this -> view = 'category';

          }

            /* article name is not set  */
            /* TODO: display all articles from the category */
        } else $this -> redirectToUrl('NotFound');  // category does not exist

        $this -> displayView();
      }
    }
  }

?>
