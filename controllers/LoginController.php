<?php
  class LoginController extends Controller {
    public function __construct() {
        $this -> accessLevelRequired = 0;
    }

    public function parseParameters($parameters) {

      if (!isset($parameters[0])) {
        $this -> setHeader('Přihlášení', '', '');
        $this -> view = 'login';
        $this -> data['smarty_display_login_form'] = TRUE;

        if (User::isLoggedIn()) {
          $this -> data['smarty_display_login_form'] = FALSE;
        } else {
          if (isset($_POST['login_form_submit'])) {
            if (!empty($_POST['login_form_login']) && !empty($_POST['login_form_password'])) {
              // login and password are set, now validate
              $login = htmlspecialchars(trim($_POST['login_form_login']));

              $user = new User($login);

              if ($user -> login($_POST['login_form_password'])) { // login with password
                $this -> data['smarty_display_login_form'] = FALSE;
              } else Message::getInstance() -> add('Neplatná kombinace přihlašovacího jména a hesla nebo je účet zablokován!', 'Chyba', 'error');
            } else Message::getInstance() -> add('Je třeba zadat přihlašovací jméno a heslo!', 'Chyba', 'error');
          }
        }
      } elseif ($parameters[0] == 'logout') {
        $this -> data['smarty_logged_out'] = 0;       // check if user is logged in first
        $this -> view = 'logout';

        if (User::isLoggedIn()) {
          User::logout();
          $this -> data['smarty_logged_out'] = 1;
        }
      }

      $this -> displayView();
    }
  }
?>
