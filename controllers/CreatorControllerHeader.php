<?php
  class CreatorControllerHeader extends CreatorController {
    protected $previous_step = 'contact';
    protected $next_step = 'education';
    protected $this_step = 'Header';
    protected $view = 'creator_header';

    function setViewVariables() {
    }

    function init() {
      $this -> checkBackButton();
      $assets = array();

      $this -> data['smarty_row_count'] = 1;

      if (isset($_POST['id'])) {
        $assets = $this -> loadFormData();

        # set POST variables to the form
        $this -> postToSmartyVars();

        # find out whether there is at least one invalid field
        $formIsValid = TRUE;
        foreach ($assets as $asset) {
          if (!$asset -> isValid()) {
            $formIsValid = FALSE;
            break;
          }
        }

        if ($formIsValid) {
          # serialize and continue with next step
          $this -> assets = $assets;
          $this -> serializeObjects();
          $this -> redirectToUrl('cv/' . $this -> next_step);
        } else {
          # form is invalid
          $this -> validationErrorsToSmartyVars($assets);
        }

      } else { // $_POST is not set
        # just display form
        $this -> data['smarty_row_count'] = 1;
      }

      $this -> displayView();
    }
  }
?>
