<?php
  class CreatorControllerPhoto extends CreatorController {
    protected $previous_step = 'header';
    protected $next_step = 'contact';
    protected $this_step = 'Photo';
    protected $view = 'creator_photo';

    protected $upload_path = '/var/run/php-uploads/';
    protected $file_sizeLimit = '100000';
    protected $file_id = 'fileToUpload';
    protected $file_typeLimit = array('jpg', 'jpeg', 'gif', 'png', 'JPEG', 'JPG', 'GIF', 'PNG');
    protected $file_uploaded = FALSE;

    function init() {
      $this -> checkBackButton();
      $assets = array();

      # check if user has image already uploaded, then display it
      // $image_uploaded_check = dibi::query('SELECT [image] FROM [photos] WHERE [userid] = %i', $_SESSION['user'] -> getId()) -> fetchSingle();
      // if ($image_uploaded_check) {
      //   $this -> data['smarty_image_display'] = TRUE;
      // }

      if (isset($_POST['creator_form_submit'])) {
        if (isset($_POST['creator_form_checkbox'])) {
          if (isset($_FILES[$this -> file_id])) {
            $target_file = $this -> upload_path . basename($_FILES[$this -> file_id]["name"]);
            $filename = $_SESSION['user'] -> getId();

            $file = new File($filename, $this -> upload_path, $this -> file_id, $this -> file_sizeLimit, $this -> file_typeLimit);
            if ($file -> checkSize()) {
              if ($file -> checkFileType()) {
                Message::getInstance() -> add('Nepovolený typ souboru!', 'ok', 'ok');
              } elseif ($file -> put()) {
                Message::getInstance() -> add('Soubor byl úspěšně nahrán!', 'ok', 'ok');
                $this -> file_uploaded = TRUE;

                # continue with next step
                // $this -> redirectToUrl('cv/' . $this -> next_step);
              } else {
                if (empty($_FILES[$this -> file_id]['name'])) {
                  Message::getInstance() -> add('Nebyl vybrán žádný soubor!', 'error', 'error');
                } else Message::getInstance() -> add('Interní chyba v uploadu!', 'error', 'error');
              }
            } else Message::getInstance() -> add('Nevyhovuje velikost souboru!', 'error', 'error');
          } else {
            Message::getInstance() -> add('Nebyl vybrán žádný soubor!', 'error', 'error');
          }
        } else {
          # continue with next step
          // $this -> redirectToUrl('cv/' . $this -> next_step);
        }
      }
      $this -> displayView();
    }

  }
 ?>
