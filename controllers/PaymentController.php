<?php
  class PaymentController extends Controller {
    public function parseParameters($parameters) {
      $this -> view = 'payment';
      $message = new Message();

      # check if user subscription is active
      $subscription = new Payment($_SESSION['id']);
      $active = $subscription -> isActive();

      if ($active) {
        $this -> data['smarty_display'] = FALSE;
        $this -> data['smarty_subscription_to'] = date_format(date_create($active), 'd.m. Y');
      } else {
        $this -> data['smarty_display'] = TRUE;
      }

      if (isset($_POST['payment_create_form_submit'])) {
        if ($subscription -> create(NULL, NULL, NULL)) {
           $message -> add('Předplatné úspěšně vytvořeno!', 'OK', 'ok');
           $this -> redirectToUrl('payment');
         }
        else $message -> add('Vyskytla se chyba při zavádění předplatného!', 'Chyba', 'error');
      }

      $this -> displayView();
    }
  }
?>
