<?php
  class NotFoundController extends Controller {
    public function __construct() {
      // send 404 response
      header('HTTP/1.0 404 Not Found');

      $this -> header['title'] = 'Nenalezeno!';
      $this -> header['keywords'] = '404, nenalezeno';
      $this -> header['description'] = 'Oh snap. The page you were looking for is not here!';
      
      $this -> view = 'NotFound';
      $this -> displayView('NotFound');
    }

    public function parseParameters($parameters) {

    }
  }

?>
