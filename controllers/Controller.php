<?php
  abstract class Controller {
    protected $data = array();
    protected $view = '';
    protected $accessLevelRequired;

    protected $header = array('title' => '', 'keywords' => '', 'description' => '');

    abstract function parseParameters($parameters);

    public function displayView() {
      if ($this -> view) {
        $smarty = new Smarty();

        $accessControlResult = $this -> checkAccessLevelWithUser();
        if ($accessControlResult === TRUE) $smarty -> assign('smarty_access', TRUE);
        elseif ($accessControlResult == 'REGISTERED') $smarty -> assign('smarty_access_registered', TRUE);
        elseif ($accessControlResult == 'PAID') $smarty -> assign('smarty_access_paid', TRUE);

        # SMARTY variables has to be labeled as smarty_(...)
        foreach ($this -> data as $smarty_var => $actual_data) {
          if (strpos($smarty_var, 'smarty') === 0)
            $smarty -> assign($smarty_var, $actual_data);
        }

        $smarty -> assign('smarty_header_title', $this -> header['title']);
        $smarty -> assign('smarty_header_keywords', $this -> header['keywords']);
        $smarty -> assign('smarty_header_description', $this -> header['description']);

        # check if user is logged in
        if (User::isLoggedIn()) {
          # set variables to display respective login message
          $smarty -> assign('smarty_login_user', $_SESSION['user'] -> getUsername());
          $smarty -> assign('smarty_loggedin', 1);
        } else $smarty -> assign('smarty_loggedin', 0);

        # collect messages and assign to smarty variables
        $messages_collection = array();
        $smarty -> assign('smarty_display_messages', 0);

        if (!empty($_SESSION['messages'])) {
          foreach ($_SESSION['messages'] as $msg_id => $msg_index) {
            $messages_collection[$msg_id]['type']    = $msg_index['type'];
            $messages_collection[$msg_id]['class']   = $msg_index['class'];
            $messages_collection[$msg_id]['message'] = $msg_index['message'];

            unset($_SESSION['messages'][$msg_id]);
          }

          $smarty -> assign('smarty_display_messages', 1);
          $smarty -> assign('smarty_msg', $messages_collection);
        }

        $smarty -> display($this -> view . '.tpl');
      }
    }

    public function redirectToUrl($url) {
      header("Location: /$url");
      header("Connection: close");
      exit;
    }

    public function setHeader($title, $keywords, $description) {
      $this -> header['title'] = $title;
      $this -> header['keywords'] = $keywords;
      $this -> header['description'] = $description;
    }

    private function checkAccessLevelWithUser() {
      $access = 'REGISTERED';

      if ($this -> accessLevelRequired == 0) $access = TRUE;
      elseif ($this -> accessLevelRequired > 0 && isset($_SESSION['user'])) {
        $userAccessLevel = $_SESSION['user'] -> getAccessLevel();
        switch ($this -> accessLevelRequired) {
          case '1':
            if ($userAccessLevel == 0) $access = 'REGISTERED';
            else $access = TRUE;
            break;

          case '2':
            if ($userAccessLevel <= 1) $access = 'PAID';
            else $access = TRUE;
            break;

          case '3': $access = TRUE;
        }
      }

      return $access;
    }

    protected function setValidationFieldNames($validationFields) {
      foreach ($validationFields as $fieldName => $newFieldName) {
        Validation::set_field_name($fieldName, $newFieldName);
      }
    }

  }

?>
