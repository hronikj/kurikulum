<?php
  class CreatorController extends Controller {
    protected $message;
    protected $isSubscriptionActive;

    public function __construct() {
      $this -> message = new Message();
      $this -> data['smarty_subscription'] = TRUE;
      $this -> data['smarty_row_count'] = 1;

      # check whether user has active subscription
      if (isset($_SESSION['id'])) {
        $subscription = new Payment($_SESSION['id']);
        $this -> isSubscriptionActive = $subscription -> isActive();
      } else $this -> isSubscriptionActive = FALSE;

      $this -> forRegisteredOnly = TRUE;
    }

    public function parseParameters($parameters) {
      if ($this -> isSubscriptionActive) {
        # DESTROY FORM
        if (isset($_POST['creator_form_control_destroy'])) $this -> destroyForm();

        if (isset($parameters[0]) && $parameters[0] == 'edit') {
          if (isset($parameters[1])) {
            $configuration = new Creator();
            $configuration -> loadConfiguration($parameters[1], $_SESSION['id']);
            $_SESSION['edit_id'] = $parameters[1];
            $this -> redirectToUrl('creator');
          } else {
            # no ID provided
          }
        }

        # STEP FIRST
        if (!isset($parameters[0])) $this -> stepFirst();

        # STEP EDUCATION
        if (isset($parameters[0]) && $parameters[0] == 2) $this -> stepEducation();

        # STEP WORK
        if (isset($parameters[0]) && $parameters[0] == 3) $this -> stepWork();

        # STEP SKILLS
        if (isset($parameters[0]) && $parameters[0] == 4) $this -> stepSkills();

        # STEP LANGUAGES
        if (isset($parameters[0]) && $parameters[0] == 5) $this -> stepLanguages();

        # STEP CERTIFICATIONS
        if (isset($parameters[0]) && $parameters[0] == 6) $this -> stepCertification();

        # STEP FINAL
        if (isset($parameters[0]) && $parameters[0] == 7) $this -> stepFinal();

      } else {
        # subscription is not active
        $this -> data['smarty_subscription'] = FALSE;
        $this -> view = 'creator';
      }

      $this -> displayView();
    }

    private function stepFirst() {
      if (isset($_SESSION['creator_form']['name'])) {
        $this -> data['smarty_name'] = $_SESSION['creator_form']['name'];
        $this -> data['smarty_graphics'] = $_SESSION['creator_form']['graphics'];

        unset($_SESSION['creator_form']['graphics']);
        unset($_SESSION['creator_form']['name']);
      }


      if (isset($_POST['creator_form_submit'])) {
        unset($_SESSION['creator_form']);

        if (!empty($_POST['creator_form_name']) && !empty($_POST['creator_form_graphics'])) {
          $_POST['creator_form_name'] = htmlspecialchars(trim($_POST['creator_form_name']));

          if (isset($_SESSION['edit']) && $_SESSION['edit'] === TRUE) {
            $_SESSION['creator_form']['name'] = $_POST['creator_form_name'];
            $_SESSION['creator_form']['graphics'] = $_POST['creator_form_graphics'];
            unset($_POST['creator_form_name']);
            unset($_POST['creator_form_graphics']);

            $this -> redirectToUrl('creator/2');

          } elseif (Creator::configurationExists($_POST['creator_form_name'], $_SESSION['id'])) {
            $this -> message -> add('Konfigurace s tímto názvem již existuje! Zvolte prosím jiné jméno.', 'Chyba', 'error');
            $this -> data['smarty_name'] = $_POST['creator_form_name'];
          } else {
            $_SESSION['creator_form']['name'] = $_POST['creator_form_name'];
            $_SESSION['creator_form']['graphics'] = $_POST['creator_form_graphics'];
            unset($_POST['creator_form_name']);
            unset($_POST['creator_form_graphics']);

            $this -> redirectToUrl('creator/2');
          }
        } else {
          $this -> message -> add('Nebylo vyplněno povinné pole jméno konfigurace nebo vybrána grafická šablona!', 'Chyba', 'error');
        }
      }

      $this -> view = 'creator';
    }


    private function stepEducation() {
      if (isset($_SESSION['creator_form_education'])) {
        $this -> data['smarty_creator_form_education_school'] = $_SESSION['creator_form_education']['school'];
        $this -> data['smarty_creator_form_education_title'] = $_SESSION['creator_form_education']['title'];
        $this -> data['smarty_creator_form_education_startYear'] = $_SESSION['creator_form_education']['startYear'];
        $this -> data['smarty_creator_form_education_endYear'] = $_SESSION['creator_form_education']['endYear'];
        $this -> data['smarty_creator_form_education_degree'] = $_SESSION['creator_form_education']['degree'];

        $this -> data['smarty_row_count'] = count($_SESSION['creator_form_education']['school']);
      }

      # Go back from education to first step
      if (isset($_POST['creator_form_education_back'])) {
        $this -> data['smarty_name'] = $_SESSION['creator_form']['name'];
        $this -> data['smarty_graphics'] = $_SESSION['creator_form']['graphics'];

        $this -> redirectToUrl('creator');
      }

      if (isset($_POST['creator_form_education_submit'])) {
        if (isset($_SESSION['creator_form_education']['id'])) { $field_ids = $_SESSION['creator_form_education']['id']; }
        unset($_SESSION['creator_form_education']);

        if(isset($_SESSION['creator_form_education']['id'])) $_SESSION['creator_form_education']['id'] = $field_ids;

        $degree    = $_POST['creator_form_education_degree'];
        $school    = $_POST['creator_form_education_school'];
        $startYear = $_POST['creator_form_education_startYear'];
        $endYear   = $_POST['creator_form_education_endYear'];
        $title     = $_POST['creator_form_education_title'];

        $education = new CreatorEducation();
        $validation = $education -> validate($_POST);

        $this -> data['smarty_row_count'] = count($_POST['creator_form_education_degree']);

        foreach ($school as $key => $value) {
          $this -> data['smarty_creator_form_education_school'][$key]    = $value;
          $this -> data['smarty_creator_form_education_startYear'][$key] = $startYear[$key];
          $this -> data['smarty_creator_form_education_endYear'][$key]   = $endYear[$key];
          $this -> data['smarty_creator_form_education_degree'][$key]    = $degree[$key];
          $this -> data['smarty_creator_form_education_title'][$key]     = $title[$key];
        }

        if ($validation === TRUE) {
          foreach ($degree as $key => $value) {
            $_SESSION['creator_form_education']['degree'][$key]    = $value;
            $_SESSION['creator_form_education']['school'][$key]    = $school[$key];
            $_SESSION['creator_form_education']['startYear'][$key] = $startYear[$key];
            $_SESSION['creator_form_education']['endYear'][$key]   = $endYear[$key];
            $_SESSION['creator_form_education']['title'][$key]     = $title[$key];
          }

          $this -> redirectToUrl('creator/3');
        } else {
          $fieldset = array('startYear', 'endYear', 'title', 'school');

          foreach ($fieldset as $fieldkey) {
            foreach ($_SESSION["validation_$fieldkey"] as $fkey => $value) {
              if ($_SESSION["validation_$fieldkey"][$fkey] !== TRUE) {
                foreach ($_SESSION["validation_$fieldkey"][$fkey] as $key => $value) {
                  if (isset($this -> data["smarty_validation_$fieldkey"][$fkey]))
                    $this -> data["smarty_validation_$fieldkey"][$fkey] .=  '<li>' . $value . '</li>';
                  else $this -> data["smarty_validation_$fieldkey"][$fkey] =  '<li>' . $value . '</li>';
                }
              }
            }

            unset($_SESSION["validation_$fieldkey"]);
          }


          $this -> message -> add('Opravte prosím pole formuláře podle popisu!', 'Chyba', 'error');
        }

      }

      $this -> checkIntegrity('education');
      $this -> view = 'creator_education';
    }

    private function stepWork() {
      var_dump($_SESSION['creator_form_education']);
      if (isset($_SESSION['creator_form_work'])) {
        $this -> data['smarty_creator_form_work_employer']    = $_SESSION['creator_form_work']['employer'];
        $this -> data['smarty_creator_form_work_position']    = $_SESSION['creator_form_work']['position'];
        $this -> data['smarty_creator_form_work_startYear']   = $_SESSION['creator_form_work']['startYear'];
        $this -> data['smarty_creator_form_work_endYear']     = $_SESSION['creator_form_work']['endYear'];
        $this -> data['smarty_creator_form_work_description'] = $_SESSION['creator_form_work']['description'];

        $this -> data['smarty_row_count'] = count($_SESSION['creator_form_work']['employer']);
      }

      # Go back from work to step education
      if (isset($_POST['creator_form_work_back'])) {
        $this -> redirectToUrl('creator/2');
      }

      if (isset($_POST['creator_form_work_submit'])) {
        if (isset($_SESSION['creator_form_work']['id'])) { $field_ids = $_SESSION['creator_form_work']['id']; }
        unset($_SESSION['creator_form_work']);

        $_SESSION['creator_form_work']['id'] = $field_ids;

        $employer    = $_POST['creator_form_work_employer'];
        $position    = $_POST['creator_form_work_position'];
        $startYear   = $_POST['creator_form_work_startYear'];
        $endYear     = $_POST['creator_form_work_endYear'];
        $description = $_POST['creator_form_work_description'];

        $work = new CreatorWork();
        $validation = $work -> validate($_POST);

        $this -> data['smarty_row_count'] = count($_POST['creator_form_work_employer']);

        foreach ($employer as $key => $value) {
          $this -> data['smarty_creator_form_work_employer'][$key]    = $value;
          $this -> data['smarty_creator_form_work_position'][$key]    = $position[$key];
          $this -> data['smarty_creator_form_work_endYear'][$key]     = $endYear[$key];
          $this -> data['smarty_creator_form_work_startYear'][$key]   = $startYear[$key];
          $this -> data['smarty_creator_form_work_description'][$key] = $description[$key];
        }

        if ($validation === TRUE) {
          foreach ($employer as $key => $value) {
            $_SESSION['creator_form_work']['employer'][$key]    = $value;
            $_SESSION['creator_form_work']['position'][$key]    = $position[$key];
            $_SESSION['creator_form_work']['startYear'][$key]   = $startYear[$key];
            $_SESSION['creator_form_work']['endYear'][$key]     = $endYear[$key];
            $_SESSION['creator_form_work']['description'][$key] = $description[$key];
          }

          $this -> redirectToUrl('creator/4');
        } else {
          $fieldset = array('employer', 'position', 'endYear', 'startYear');

          foreach ($fieldset as $fieldkey) {
            foreach ($_SESSION["validation_$fieldkey"] as $fkey => $value) {
              if ($_SESSION["validation_$fieldkey"][$fkey] !== TRUE) {
                foreach ($_SESSION["validation_$fieldkey"][$fkey] as $key => $value) {
                  if (isset($this -> data["smarty_validation_$fieldkey"][$fkey]))
                    $this -> data["smarty_validation_$fieldkey"][$fkey] .=  '<li>' . $value . '</li>';
                  else $this -> data["smarty_validation_$fieldkey"][$fkey] =  '<li>' . $value . '</li>';
                }
              }
            }
          }

          $this -> message -> add('Opravte prosím pole formuláře podle popisu!', 'Chyba', 'error');
        }

      }

      $this -> view = 'creator_work';

      # Go back from education to first step
      if (isset($_POST['creator_form_education_back'])) {
        $this -> redirectToUrl('creator/2');
      }
    }

    public function stepSkills() {
      var_dump($_SESSION['creator_form_education']);
      if (isset($_SESSION['creator_form_skills'])) {
        $this -> data['smarty_creator_form_skills_name']  = $_SESSION['creator_form_skills']['name'];
        $this -> data['smarty_creator_form_skills_level'] = $_SESSION['creator_form_skills']['level'];

        $this -> data['smarty_row_count'] = count($_SESSION['creator_form_skills']['name']);
      }

      if (isset($_POST['creator_form_skills_submit'])) {
        if (isset($_SESSION['creator_form_skills']['id'])) { $field_ids = $_SESSION['creator_form_skills']['id']; }
        unset($_SESSION['creator_form_skills']);

        $_SESSION['creator_form_skills']['id'] = $field_ids;


        $name  = $_POST['creator_form_skills_name'];
        $level = $_POST['creator_form_skills_level'];

        $skills = new CreatorSkills();
        $validation = $skills -> validate($_POST);

        $this -> data['smarty_row_count'] = count($_POST['creator_form_skills_name']);

        foreach ($name as $key => $value) {
          $this -> data['smarty_creator_form_skills_name'][$key]  = $value;
          $this -> data['smarty_creator_form_skills_level'][$key] = $level[$key];
        }

        if ($validation === TRUE) {
          foreach ($name as $key => $value) {
            $_SESSION['creator_form_skills']['name'][$key]  = $value;
            $_SESSION['creator_form_skills']['level'][$key] = $level[$key];
          }

          $this -> redirectToUrl('creator/5');
        } else {
          $fieldset = array('name');

          foreach ($fieldset as $fieldkey) {
            foreach ($_SESSION["validation_$fieldkey"] as $fkey => $value) {
              if ($_SESSION["validation_$fieldkey"][$fkey] !== TRUE) {
                foreach ($_SESSION["validation_$fieldkey"][$fkey] as $key => $value) {
                  if (isset($this -> data["smarty_validation_$fieldkey"][$fkey]))
                    $this -> data["smarty_validation_$fieldkey"][$fkey] .=  '<li>' . $value . '</li>';
                  else $this -> data["smarty_validation_$fieldkey"][$fkey] =  '<li>' . $value . '</li>';
                }
              }
            }
          }

          $this -> message -> add('Opravte prosím pole formuláře podle popisu!', 'Chyba', 'error');
        }

      }

      $this -> view = 'creator_skills';

      # Go back from skills to step work
      if (isset($_POST['creator_form_skills_back'])) {
        $this -> redirectToUrl('creator/3');
      }
    }

    private function stepLanguages() {
      var_dump($_SESSION['creator_form_education']);
      if (isset($_SESSION['creator_form_languages'])) {
        $this -> data['smarty_creator_form_languages_name']  = $_SESSION['creator_form_languages']['name'];
        $this -> data['smarty_creator_form_languages_level'] = $_SESSION['creator_form_languages']['level'];

        $this -> data['smarty_row_count'] = count($_SESSION['creator_form_languages']['name']);
      }

      # Go back from languages to step skills
      if (isset($_POST['creator_form_languages_back'])) {
        $this -> redirectToUrl('creator/4');
      }

      if (isset($_POST['creator_form_languages_submit'])) {
        if (isset($_SESSION['creator_form_languages']['id'])) { $field_ids = $_SESSION['creator_form_languages']['id']; }
        unset($_SESSION['creator_form_languages']);

        $_SESSION['creator_form_languages']['id'] = $field_ids;

        $name  = $_POST['creator_form_languages_name'];
        $level = $_POST['creator_form_languages_level'];

        $languages = new CreatorLanguages();
        $validation = $languages -> validate($_POST);

        $this -> data['smarty_row_count'] = count($_POST['creator_form_languages_name']);

        foreach ($name as $key => $value) {
          $this -> data['smarty_creator_form_languages_name'][$key]  = $value;
          $this -> data['smarty_creator_form_languages_level'][$key] = $level[$key];
        }

        if ($validation === TRUE) {
          foreach ($name as $key => $value) {
            $_SESSION['creator_form_languages']['name'][$key]    = $value;
            $_SESSION['creator_form_languages']['level'][$key]    = $level[$key];
          }

          $this -> redirectToUrl('creator/6');
        } else {
          $fieldset = array('name');

          foreach ($fieldset as $fieldkey) {
            foreach ($_SESSION["validation_$fieldkey"] as $fkey => $value) {
              if ($_SESSION["validation_$fieldkey"][$fkey] !== TRUE) {
                foreach ($_SESSION["validation_$fieldkey"][$fkey] as $key => $value) {
                  if (isset($this -> data["smarty_validation_$fieldkey"][$fkey]))
                    $this -> data["smarty_validation_$fieldkey"][$fkey] .=  '<li>' . $value . '</li>';
                  else $this -> data["smarty_validation_$fieldkey"][$fkey] =  '<li>' . $value . '</li>';
                }
              }
            }
          }

          $this -> message -> add('Opravte prosím pole formuláře podle popisu!', 'Chyba', 'error');
        }

      }

      $this -> view = 'creator_languages';

      # Go back from languages to skills step
      if (isset($_POST['creator_form_education_back'])) {
        $this -> redirectToUrl('creator/4');
      }
    }


    private function stepCertification() {
      var_dump($_SESSION['creator_form_education']);
      if (isset($_SESSION['creator_form_certification'])) {
        $this -> data['smarty_creator_form_certification_name']   = $_SESSION['creator_form_certification']['name'];
        $this -> data['smarty_creator_form_certification_year']   = $_SESSION['creator_form_certification']['year'];
        $this -> data['smarty_creator_form_certification_school'] = $_SESSION['creator_form_certification']['school'];

        $this -> data['smarty_row_count'] = count($_SESSION['creator_form_certification']['name']);
      }

      # Go back from certification to step languages
      if (isset($_POST['creator_form_certification_back'])) {
        $this -> redirectToUrl('creator/5');
      }

      if (isset($_POST['creator_form_certification_submit'])) {
        if (isset($_SESSION['creator_form_certification']['id'])) { $field_ids = $_SESSION['creator_form_certification']['id']; }
        unset($_SESSION['creator_form_certification']);

        $_SESSION['creator_form_certification']['id'] = $field_ids;

        $name   = $_POST['creator_form_certification_name'];
        $year   = $_POST['creator_form_certification_year'];
        $school = $_POST['creator_form_certification_school'];

        $certification = new CreatorCertification();
        $validation = $certification -> validate($_POST);

        $this -> data['smarty_row_count'] = count($_POST['creator_form_certification_name']);

        foreach ($name as $key => $value) {
          $this -> data['smarty_creator_form_certification_name'][$key]   = $value;
          $this -> data['smarty_creator_form_certification_year'][$key]   = $year[$key];
          $this -> data['smarty_creator_form_certification_school'][$key] = $school[$key];
        }

        if ($validation === TRUE) {
          foreach ($name as $key => $value) {
            $_SESSION['creator_form_certification']['name'][$key]   = $value;
            $_SESSION['creator_form_certification']['year'][$key]   = $year[$key];
            $_SESSION['creator_form_certification']['school'][$key] = $school[$key];
          }

          $this -> redirectToUrl('creator/7');
        } else {
          $fieldset = array('name', 'year', 'school');

          foreach ($fieldset as $fieldkey) {
            foreach ($_SESSION["validation_$fieldkey"] as $fkey => $value) {
              if ($_SESSION["validation_$fieldkey"][$fkey] !== TRUE) {
                foreach ($_SESSION["validation_$fieldkey"][$fkey] as $key => $value) {
                  if (isset($this -> data["smarty_validation_$fieldkey"][$fkey]))
                    $this -> data["smarty_validation_$fieldkey"][$fkey] .=  '<li>' . $value . '</li>';
                  else $this -> data["smarty_validation_$fieldkey"][$fkey] =  '<li>' . $value . '</li>';
                }
              }
            }
          }

          $this -> message -> add('Opravte prosím pole formuláře podle popisu!', 'Chyba', 'error');
        }

      }

      $this -> view = 'creator_certification';

      # Go back from languages to skills step
      if (isset($_POST['creator_form_certification_back'])) {
        $this -> redirectToUrl('creator/5');
      }
    }

    private function stepFinal() {

      # display all step values for evaluation
      if (isset($_POST['creator_form_final_submit'])) {
        # save to database
        $creator = new Creator();

        if (isset($_SESSION['edit']) && $_SESSION['edit'] === TRUE) {
            $creator -> editConfiguration($_SESSION['edit_id'], $_SESSION);

            unset($_SESSION['edit']);
            $this -> destroyForm();
        } else {
          $creator -> createCofiguration($_SESSION['creator_form']['name'], $_SESSION, $_SESSION['id']);
          # finalize
          $this -> message -> add('Konfigurace byla úspěšně vytvořena!', 'ok', 'ok');
          $this -> destroyForm();
          $this -> redirectToUrl('configurations');
        }

      }

      if (isset($_POST['creator_form_final_back'])) {
        $this -> redirectToUrl('creator/6');
      }

      # certification
      $this -> data['smarty_certification_row_count'] = count($_SESSION['creator_form_certification']['name']);
      $this -> data['smarty_creator_form_certification_year']   = $_SESSION['creator_form_certification']['year'];
      $this -> data['smarty_creator_form_certification_name']   = $_SESSION['creator_form_certification']['name'];
      $this -> data['smarty_creator_form_certification_school'] = $_SESSION['creator_form_certification']['school'];

      # languages
      $this -> data['smarty_languages_row_count'] = count($_SESSION['creator_form_languages']['name']);
      $this -> data['smarty_creator_form_languages_name']  = $_SESSION['creator_form_languages']['name'];
      $this -> data['smarty_creator_form_languages_level'] = $_SESSION['creator_form_languages']['level'];

      # skills
      $this -> data['smarty_skills_row_count'] = count($_SESSION['creator_form_skills']['name']);
      $this -> data['smarty_creator_form_skills_name']  = $_SESSION['creator_form_skills']['name'];
      $this -> data['smarty_creator_form_skills_level'] = $_SESSION['creator_form_skills']['level'];

      # work
      $this -> data['smarty_work_row_count'] = count($_SESSION['creator_form_work']['employer']);
      $this -> data['smarty_creator_form_work_employer']    = $_SESSION['creator_form_work']['employer'];
      $this -> data['smarty_creator_form_work_startYear']   = $_SESSION['creator_form_work']['startYear'];
      $this -> data['smarty_creator_form_work_endYear']     = $_SESSION['creator_form_work']['endYear'];
      $this -> data['smarty_creator_form_work_position']    = $_SESSION['creator_form_work']['position'];
      $this -> data['smarty_creator_form_work_description'] = $_SESSION['creator_form_work']['description'];

      # education
      $this -> data['smarty_education_row_count'] = count($_SESSION['creator_form_education']['school']);
      $this -> data['smarty_creator_form_education_school']    = $_SESSION['creator_form_education']['school'];
      $this -> data['smarty_creator_form_education_startYear'] = $_SESSION['creator_form_education']['startYear'];
      $this -> data['smarty_creator_form_education_endYear']   = $_SESSION['creator_form_education']['endYear'];
      $this -> data['smarty_creator_form_education_degree']    = $_SESSION['creator_form_education']['degree'];
      $this -> data['smarty_creator_form_education_title']     = $_SESSION['creator_form_education']['title'];

      $this -> view = 'creator_final';
    }

    private function checkIntegrity($step) {
      $return = FALSE;

      switch ($step) {
        case 'language':
        # check previous step (work)

        case 'work':
        # check previous step (education)
          if (!isset($_SESSION['creator_form_education']['degree']))    { $return = TRUE; break; }
          if (!isset($_SESSION['creator_form_education']['startYear'])) { $return = TRUE; break; }
          if (!isset($_SESSION['creator_form_education']['endYear']))   { $return = TRUE; break; }
          if (!isset($_SESSION['creator_form_education']['title']))     { $return = TRUE; break; }
          if (!isset($_SESSION['creator_form_education']['school']))    { $return = TRUE; break; }

        case 'education':
        # check previous step (first step)
          if (!isset($_SESSION['creator_form']['name']))      { $return = TRUE; break; }
          if (!isset($_SESSION['creator_form']['graphics']))  { $return = TRUE; break; }

          break;

        default:
          break;
      }

      if ($return) $this -> redirectToUrl('creator');
    }

    private function destroyForm() {
      if (isset($_SESSION['creator_form'])) unset($_SESSION['creator_form']);
      if (isset($_SESSION['creator_form_education'])) unset($_SESSION['creator_form_education']);
      if (isset($_SESSION['creator_form_work'])) unset($_SESSION['creator_form_work']);
      if (isset($_SESSION['creator_form_language'])) unset($_SESSION['creator_form_language']);
      if (isset($_SESSION['creator_form_skills'])) unset($_SESSION['creator_form_skills']);
      if (isset($_SESSION['creator_form_certification'])) unset($_SESSION['creator_form_certification']);
      if (isset($_SESSION['edit'])) unset($_SESSION['edit']);

      // $this -> message -> add('Formulář resetován, data vymazána!', 'OK', 'ok');
      $this -> redirectToUrl('creator');
    }

  }
?>
