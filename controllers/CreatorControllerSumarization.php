<?php
  class CreatorControllerSumarization extends CreatorController {
    protected $previous_step = 'hobbies';
    protected $next_step = 'finish';
    protected $this_step = 'Sumarization';
    protected $view = 'creator_sumarization';

    function setViewVariables() {
    }

    function init() {
      $this -> checkBackButton();
      $assets_education = array();
      $education_array = array();

      // var_dump($_SESSION['Education']);

      # EDUCATION
      foreach ($_SESSION['Education'] as $key => $education_asset) {
        $education_asset = unserialize($education_asset);
        foreach (Education::$fieldset as $field) {
          $education_array[$key][$field] = $education_asset -> $field;
        }
      }
      $this -> data['smarty_education_array'] = $education_array;

      # WORK
      foreach ($_SESSION['Work'] as $key => $work_asset) {
        $work_asset = unserialize($work_asset);
        foreach (Work::$fieldset as $field) {
          $work_array[$key][$field] = $work_asset -> $field;
        }
      }
      $this -> data['smarty_work_array'] = $work_array;

      # LANGUAGE
      foreach ($_SESSION['Language'] as $key => $language_asset) {
        $language_asset = unserialize($language_asset);
        foreach (Language::$fieldset as $field) {
          $language_array[$key][$field] = $language_asset -> $field;
        }
      }
      $this -> data['smarty_language_array'] = $language_array;

      # PCSKILLS
      foreach ($_SESSION['Pcskills'] as $key => $pcskills_asset) {
        $pcskills_asset = unserialize($pcskills_asset);
        foreach (Pcskills::$fieldset as $field) {
          $pcskills_array[$key][$field] = $pcskills_asset -> $field;
        }
      }
      $this -> data['smarty_pcskills_array'] = $pcskills_array;

      # SKILLS
      foreach ($_SESSION['Skills'] as $key => $skills_asset) {
        $skills_asset = unserialize($skills_asset);
        foreach (Skills::$fieldset as $field) {
          $skills_array[$key][$field] = $skills_asset -> $field;
        }
      }
      $this -> data['smarty_skills_array'] = $skills_array;

      # CERTIFICATION
      foreach ($_SESSION['Certification'] as $key => $certification_asset) {
        $certification_asset = unserialize($certification_asset);
        foreach (Certification::$fieldset as $field) {
          $certification_array[$key][$field] = $certification_asset -> $field;
        }
      }
      $this -> data['smarty_certification_array'] = $certification_array;

      // var_dump($_SESSION);

      # HEADER
      $header_asset = unserialize($_SESSION['Header'][0]);
      $this -> data['smarty_header_motto'] = $header_asset -> motto;
      $this -> data['smarty_header_header'] = $header_asset -> header;

      # HOBBIES
      $hobbies_asset = unserialize($_SESSION['Hobbies'][0]);
      $this -> data['smarty_hobbies'] = $hobbies_asset -> hobbies;

      # DRIVING
      $driving_asset = unserialize($_SESSION['Driving'][0]);
      $this -> data['smarty_driving'] = $driving_asset -> level;

      # TEMPLATE
      $template_asset = unserialize($_SESSION['Template'][0]);
      $this -> data['smarty_template'] = $template_asset -> templateid;


      if (isset($_POST['id'])) {
        # save all data to DB in transaction and continue with next step

        $cv_configuration = array(
          'graphics' => '',
          'userid'   => $_SESSION['user'] -> getId(),
          'name'     => ''
        );

        # first - prepare data, unserialize and add to arrays
        foreach ($_SESSION['Education'] as $key => $asset) {
          $asset = unserialize($asset);
          foreach (Education::$fieldset as $field) {
            $array[$field] = $asset -> $field;
          }
        }
        $education_array = $array;

        foreach ($_SESSION['Work'] as $key => $asset) {
          $asset = unserialize($asset);
          foreach (Education::$fieldset as $field) {
            $array[$field] = $asset -> $field;
          }
        }
        $work_array = $array;

        foreach ($_SESSION['Skills'] as $key => $asset) {
          $asset = unserialize($asset);
          foreach (Education::$fieldset as $field) {
            $array[$field] = $asset -> $field;
          }
        }
        $skills_array = $array;

        foreach ($_SESSION['Certification'] as $key => $asset) {
          $asset = unserialize($asset);
          foreach (Education::$fieldset as $field) {
            $array[$field] = $asset -> $field;
          }
        }
        $certification_array = $array;
        if (0 and 0 and 0) {
        dibi::begin();
          dibi::query('INSERT INTO [cv_configurations]', $cv_configuration);
          dibi::query('INSERT INTO [cv_assets_education]', $education_array);
          dibi::query('INSERT INTO [cv_assets_work]', $work_array);
          dibi::query('INSERT INTO [cv_assets_skills]', $skills_array);
          dibi::query('INSERT INTO [cv_assets_certification]', $certification_array);
          dibi::query('INSERT INTO [cv_assets_language]', $language_array);
        dibi::commit();
        }

        # display finalization page
        // $this -> redirectToUrl('cv/' . $this -> next_step);
      } else { // $_POST is not set
        # just display form
        $this -> data['smarty_row_count'] = 1;
      }

      $this -> displayView();
    } // end of init()
  }
?>
