<?php
  class CvController extends Controller {
    private $steps = array('Photo', 'Contact', 'Header', 'Education', 'Work', 'Language', 'Skills', 'Pcskills', 'Driving', 'Certification', 'Hobbies', 'Template', 'Sumarization');
    private $wants_photo = FALSE;
    private $wants_header = FALSE;

    function parseParameters($parameters) {
      # autoload controllers for each step in wizard
      if (isset($parameters[0]) && $parameters[0] && in_array(ucfirst($parameters[0]), $this -> steps)) {
        $stepClassName = ucfirst($parameters[0]);
        $controllerName = "CreatorController" . $stepClassName;
        $stepController = new $controllerName($stepClassName);  // from now on, custom controller handles the flow
      } else {
        $this -> redirectToUrl('not-found');
      }

      //
      // if (isset($_POST['creator_form_submit'])) {
      //   $education_array = array();
      //
      //   foreach ($_POST['school'] as $row => $value) {
      //     $education_instance = new Education();
      //     foreach (Education::getFieldset() as $field) {
      //       $education_instance -> $field = $_POST["$field"][$row];
      //       $this -> data["smarty_$field"] = $education_instance -> $field;
      //     }
      //
      //     $education_instance -> validate();
      //     array_push($education_array, $education_instance);
      //   }
      //
      //   foreach ($education_array as $education) {
      //     if (!$education -> isValid()) {
      //       foreach (Education::$fieldset as $field) {
      //         $validated_var_name = "validated_$field";
      //         if ($education -> $validated_var_name) {
      //           echo $education -> $validated_var_name;
      //         }
      //       }
      //     }
      //   }
      //
      // }

      // $this -> view = 'creator_education2';
      // $this -> displayView();
    }
  }

?>
