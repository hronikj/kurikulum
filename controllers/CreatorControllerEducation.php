<?php
  class CreatorControllerEducation extends CreatorController {
    protected $previous_step = 'header';
    protected $next_step = 'work';
    protected $this_step = 'Education';
    protected $view = 'creator_education2';

    function setViewVariables() {
    }

    function init() {
      $this -> checkBackButton();
      $assets = array();

      if (isset($_POST['id'])) {
        $assets = $this -> loadFormData();

        # set POST variables to the form
        $this -> postToSmartyVars();

        # find out whether there is at least one invalid field
        $formIsValid = TRUE;
        foreach ($assets as $asset) {
          if (!$asset -> isValid()) {
            $formIsValid = FALSE;
            break;
          }
        }

        if ($formIsValid) {
          # serialize and continue with next step
          $this -> assets = $assets;
          $this -> serializeObjects();
          $this -> redirectToUrl('cv/' . $this -> next_step);
        } else {
          # form is invalid
          $this -> validationErrorsToSmartyVars($assets);
        }

      } else { // $_POST is not set
        # just display form
        $this -> data['smarty_row_count'] = 1;
      }

      $this -> displayView();
    } // end of init()
  }
?>
