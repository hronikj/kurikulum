<?php
  class CreatorControllerContact extends CreatorController {
    protected $previous_step = 'photo';
    protected $next_step = 'header';
    protected $this_step = 'Contact';
    protected $view = 'creator_contact';

    private $fieldset = array(
      'firstname', 'lastname', 'permanent_address', 'permanent_city',
      'permanent_zipcode', 'contact_address', 'contact_city', 'contact_zipcode',
      'contact_number', 'telephone', 'email', 'nationality', 'birthday', 'birthmonth', 'birthyear');

    private $validationRules = array(
      'firstname'          => 'required',
      'lastname'           => 'required',
      'permanent_address'  => 'required',
      'permanent_city'     => 'required',
      'contact_zipcode'    => 'numeric',
      'permanent_zipcode'  => 'numeric|required',
      'birthday'           => 'numeric|required',
      'birthmonth'         => 'numeric|required',
      'birthyear'          => 'numeric|required'
    );

    function init() {
      $this -> checkBackButton();
      $assets = array();

      # load previously saved contact details
      $profile = new Profile($_SESSION['user'] -> getId());
      $user_details = $profile -> getProfileDetails();

      if (isset($_POST['creator_form_submit'])) {
        $validation = Validation::is_valid($_POST, $this -> validationRules);
        if ($validation === TRUE) {
          # save to the database
          $profile_data = $_POST;
          unset($profile_data['creator_form_back']);
          unset($profile_data['creator_form_submit']);

          if ($profile -> update($_SESSION['user'] -> getId(), $profile_data)) {
            # continue with next step
            $this -> redirectToUrl('cv/' . $this -> next_step);
          } else Message::getInstance() -> add('Chyba při ukládání dat do databáze!', 'error', 'error');
        } else {
          $return_string = '';
          foreach ($validation as $key => $value) { $return_string = $return_string . ' <br /> ' . $value; }
          Message::getInstance() -> add($return_string, 'Chyba', 'error');

          foreach ($this -> fieldset as $field) {
            if (isset($_POST[$field]))
              $this -> data["smarty_$field"] = $_POST[$field];
          }
        }
      } else {
        foreach ($this -> fieldset as $field) {
          $this -> data["smarty_$field"] = $user_details[0] -> $field;
        }
      }

      $this -> displayView();
    }
  }
 ?>
