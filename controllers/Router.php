<?php
  class Router extends Controller {
    protected $controller;

    public function parseParameters($parameters) {
      $parsed_url = $this -> parseUrl($parameters[0]);

      switch ($parsed_url[0]) {
        case '':
          $this -> controller = new StaticPageController('index');
          $this -> controller -> parseParameters($parsed_url);

          break;

        case 'index':
          $this -> controller = new StaticPageController('index');
          $this -> controller -> parseParameters($parsed_url);

          break;

        default:
          $controller_class = $this -> dashesToCamelNotation(array_shift($parsed_url) . 'Controller');

          if (file_exists(CONTROLLER_PATH . $controller_class . '.php'))
            $this -> controller = new $controller_class;
          else $this -> redirectToUrl('not-found');

          $this -> controller -> parseParameters($parsed_url);
          break;
      }
    }

    # Structure for parsed parameters:
    # [0] => controller name
    # [1] => parameter 1
    # [2] => parameter 2
    # [3] => paramter 3
    # etc...

    private function parseUrl($url) {
      $parsed_url = parse_url($url);
      $parsed_url['path'] = ltrim($parsed_url['path'], '/'); // odstraneni lomitka
      $parsed_url['path'] = trim($parsed_url['path']);       // odstraneni bileho znaku
      $parsed_path = explode('/', $parsed_url['path']);      // rozdeleni cesty do pole

      return $parsed_path;
    }

    # source: http://www.itnetwork.cz/objektovy-mvc-redakcni-system-v-php-router-smerovac
    private function dashesToCamelNotation($text) {
      $sentence = str_replace('-', ' ', $text);
      $sentence = ucwords($sentence);
      $sentence = str_replace(' ', '', $sentence);

      return $sentence;
    }
  }
?>
