<?php
  class ConfigurationsController extends Controller {
    private $userid;
    private $isSubscriptionActive;
    private $configurations;
    private $message;

    public function __construct() {
      if (isset($_SESSION['id'])) {
        $this -> userid = $_SESSION['id'];
        $subscription = new Payment($_SESSION['id']);
        $this -> isSubscriptionActive = $subscription -> isActive();
      } else $this -> isSubscriptionActive = FALSE;

      $this -> forRegisteredOnly = TRUE;
      $this -> configurations = new Creator();
      $this -> message = new Message();
    }


    public function parseParameters($parameters) {
      if ($this -> isSubscriptionActive) {
        if (empty($parameters[0])) {
          # get all configurations
          $data = $this -> configurations -> getAllConfigurations($this -> userid);

          if ($data) {
            foreach ($data as $key => $value) {
              $this -> data['smarty_configurations_data'][$key]['id'] = $data[$key]['id'];
              $this -> data['smarty_configurations_data'][$key]['graphics'] = $data[$key]['graphics'];
              $this -> data['smarty_configurations_data'][$key]['name'] = $data[$key]['name'];
            }
            $this -> data['smarty_configurations_display'] = TRUE;
          } else $this -> data['smarty_configurations_display'] = FALSE;


          $this -> view = 'configurations';

        } elseif ($parameters[0] == 'generate' && !empty($parameters[1])) {
          # load configuration to session
          $this -> configurations -> loadConfiguration($parameters[1], $_SESSION['id']);
          $pdf = new Pdf('test'); // generate PDF file

          # we have to reset session variables here, otherwise when creating new CV,
          # it will automatically fill in the data from this configuration
          if (isset($_SESSION['creator_form'])) unset($_SESSION['creator_form']);
          if (isset($_SESSION['creator_form_education'])) unset($_SESSION['creator_form_education']);
          if (isset($_SESSION['creator_form_work'])) unset($_SESSION['creator_form_work']);
          if (isset($_SESSION['creator_form_skills'])) unset($_SESSION['creator_form_skills']);
          if (isset($_SESSION['creator_form_certification'])) unset($_SESSION['creator_form_certification']);
          if (isset($_SESSION['creator_form_languages'])) unset($_SESSION['creator_form_languages']);
          if (isset($_SESSION['edit'])) unset($_SESSION['edit']);

        } elseif ($parameters[0] == 'delete' && !empty($parameters[1])) {
          # delete configuration
          if ($this -> configurations -> deleteConfiguration($parameters[1], $_SESSION['id'])) {
            $this -> message -> add('Odstraněno!', 'OK', 'ok');
            $this -> redirectToUrl('configurations');
          } else {
            $this -> message -> add('Nepodařilo se odstranit zvolenou konfiguraci!', 'Chyba', 'error');
            $this -> redirectToUrl('configurations');
          }
        }
      } else {
        # subscription is not active
        $this -> data['smarty_subscription'] = FALSE;
        $this -> view = 'creator';
      }

      $this -> displayView();
    }

  }
?>
