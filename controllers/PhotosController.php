<?php
  class PhotosController extends Controller {
    public function parseParameters($parameters) {
      if (isset($_SESSION['user'])) {
        # get image from database
        $image_data = dibi::query('SELECT [image], [image_type] FROM [photos] WHERE [userid] = %i', $_SESSION['user'] -> getId()) -> fetchAll();
        // var_dump($image_data);
        // var_dump($image_data[0] -> image_type);
        if ($image_data) {
          header("Content-type: ". $image_data[0] -> image_type);
          echo $image_data[0] -> image;
        } else return FALSE;
      } else return FALSE;
    }

  }
?>
