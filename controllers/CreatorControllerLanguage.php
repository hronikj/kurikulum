<?php
  class CreatorControllerLanguage extends CreatorController {
    protected $previous_step = 'work';
    protected $next_step = 'skills';
    protected $this_step = 'Language';
    protected $view = 'creator_languages2';

    protected $languages = array();

    function setViewVariables() {
    }

    // function __construct() {
    //   # this is here so that data from DB is loaded everytime when advancing from previous step
    //   $this -> loadLanguagesFromDb();
    //   $this -> data['smarty_languages'] = $this -> languages;
    //   $this -> step = $this -> this_step;
    //   $this -> init();
    // }

    function init() {
      // $this -> loadLanguagesFromDb();
      // $this -> data['smarty_languages'] = $this -> languages;

      $this -> checkBackButton();
      $assets = array();

      if (isset($_POST['id'])) {
        $assets = $this -> loadFormData();

        # set POST variables to the form
        $this -> postToSmartyVars();

        # find out whether there is at least one invalid field
        $formIsValid = TRUE;

        foreach ($assets as $asset) {
          if (!$asset -> isValid()) {
            $formIsValid = FALSE;
            break;
          }
        }

        // $this -> validationErrorsToSmartyVars($assets);
        if ($formIsValid) {
          # serialize and continue with next step
          $this -> assets = $assets;
          $this -> serializeObjects();
          $this -> redirectToUrl('cv/' . $this -> next_step);
        } else {
          # form is invalid
          $this -> validationErrorsToSmartyVars($assets);
        }

      } else { // $_POST is not set
        # just display form
        $this -> data['smarty_row_count'] = 1;
      }

      $this -> displayView();
    } // end of init()

    private function loadLanguagesFromDb() {
      $languages = dibi::query('SELECT [name], [id] FROM languages ORDER BY [id]') -> fetchAll();
      foreach ($languages as $key => $value) {
        $languages_array[$key]['name'] = $languages[$key] -> name;
        $languages_array[$key]['id'] = $languages[$key] -> id;
      }

      $this -> languages = $languages_array;
    }
  }
?>
