<?php
  class RegisterController extends Controller {
    protected $accessLevelRequired = 0;
    private $fieldset = array('login',
                              'password',
                              'password2',
                              'firstname',
                              'lastname',
                              'permanent_address',
                              'permanent_city',
                              'permanent_zipcode',
                              'contact_address',
                              'contact_city',
                              'contact_zipcode',
                              'telephone',
                              'nationality',
                              'birthdate'
                             );

    private $validationRules = array('login' => 'required|valid_email', 'password' => 'required', 'password2' => 'required', 'contact_zipcode' => 'numeric', 'permanent_zipcode' => 'numeric');

    public function __construct() {
      $validationFieldNames = array('password' => 'heslo', 'password2' => 'potvrzení hesla');
      $this -> setValidationFieldNames($validationFieldNames);
      $this -> setHeader('Registrace!', 'registrace, registrace', 'Registracni stranka');
    }

    public function parseParameters($parameters) {
      if (!empty($parameters[0])) {
        switch ($parameters[0]) {
          case 'success':
            $this -> view = 'register_success';
            $this -> setHeader('Registrace úspěšná!', 'registrace, registrace, uspech', 'Registrace byla úspěšná');
            break;

          case 'validate':
            # second parameter (token) is set, begin validation
            if (!empty($parameters[1])) {
              $token = $parameters[1];
              $registration = new Registration();
              if ($registration -> finish($token)) {
                  $this -> view = 'register_validation_success';      // token validation and registration complete
              } else $this -> view = 'register_validation_failed';    // invalid token
            } else $this -> view = 'register_validation_failed';      // token not provided
            break;

          default:
            $this -> redirectToUrl('NotFound');                       // redirect to NOTFOUND
            break;
        }
      } else {
        $this -> view = 'register';

        // register form is sent
        if (isset($_POST['submit'])) {
          foreach ($_POST as $key => $value) {
            if (isset($_POST[$key])) {
              $_POST[$key] = htmlspecialchars(trim($value));
              $this -> data['smarty_' . $key] = $_POST[$key];
            }
          }

          $validation = Validation::is_valid($_POST, $this -> validationRules);

          // $validation = Validation::is_valid($_POST, array(
          //     'register_form_zipcode'   => 'required|numeric|min_len,5|max_len,5',
          //     'register_form_address'   => 'required|street_address',
          //     'register_form_email'     => 'required|valid_email',
          //     'register_form_firstname' => 'required',
          //     'register_form_lastname'  => 'required',
          //     'register_form_city'      => 'required',
          //     'register_form_password'  => 'required',
          //     'register_form_password2' => 'required',
          //     'register_form_telephone' => 'required',
          //   )
          // );

          if ($validation === TRUE) {
            if ($_POST['password'] == $_POST['password2']) {
              // check if login exists
              // $user = new User();
              $registration = new Registration();

              $registration_data = array('login' => $_POST['login'], 'password' => $_POST['password']);

              foreach ($this -> fieldset as $field) {
                $userDetails[$field] = $_POST[$field];
              }

              unset($userDetails['password2']);
              unset($userDetails['password']);
              unset($userDetails['login']);

              if (empty($_POST['birthdate'])) unset($userDetails['birthdate']);
              if (empty($_POST['permanent_zipcode'])) unset($userDetails['permanent_zipcode']);
              if (empty($_POST['contact_zipcode'])) unset($userDetails['contact_zipcode']);

              var_dump($userDetails);
              var_dump($registration_data);
              if ($registration -> create($registration_data, $userDetails)) {
                // redirect to REGISTER/SUCCESS
                $this -> redirectToUrl('register/success');
              } else Message::getInstance() -> add('Uživatel s tímto emailem již u nás má registraci!', 'Chyba', 'error');
            } else Message::getInstance() -> add('Zadaná hesla nesouhlasí!', 'Chyba', 'error');

          } else {
            $return_string = '';
            foreach ($validation as $key => $value) { $return_string = $return_string . ' <br /> ' . $value; }
            Message::getInstance() -> add($return_string, 'Chyba', 'error');
          }
        }
      }

      // display the view
      $this -> displayView();
    }
  }
?>
