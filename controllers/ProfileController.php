<?php
  class ProfileController extends Controller {

    public function parseParameters($parameters) {
      $this -> view = 'profile';
      $this -> forRegisteredOnly = TRUE;

      $session = new Session();
      $message = new Message();
      $user    = new User();

      if ($session -> isLoggedIn()) {
        if (isset($parameters[0])) {
          switch ($parameters[0]) {
            case 'edit':
              if (isset($parameters[1])) {
                switch ($parameters[1]) {
                  ############################################################## EDIT - EDUCATION
                  case 'education':
                    $detail = Profile::getDetail($parameters[2], $_SESSION['id'], 'education');

                    $this -> data['smarty_startYear'] = $detail[0] -> startYear;
                    $this -> data['smarty_endYear']   = $detail[0] -> endYear;
                    $this -> data['smarty_school']    = $detail[0] -> school;
                    $this -> data['smarty_title']     = $detail[0] -> title;
                    $this -> data['smarty_degree']    = $detail[0] -> degree;

                    if (isset($_POST['profile_edit_education_submit'])) {
                      $duplicity = Duplicity::evaluateEditForm($detail, $_POST);
                      $profile = new ProfileEducation($_POST);

                      $validation = $profile -> validate();
                      if ($validation === TRUE && !$duplicity) {
                        if ($profile -> edit($parameters[2], $parameters[1], $profile -> getProfileDetails())) {
                          $message -> add('Uloženo!', 'OK', 'ok');
                        } else $message -> add('Nepodařilo se upravit informace!', 'Chyba', 'error');
                      } elseif ($duplicity) {
                        $message -> add('Odesílají se duplicitní data!', 'Chyba', 'error');
                      } else $message -> add($validation, 'Chyba', 'error');
                    }

                    $this -> view = 'profile_edit_education';
                    break;

                  ############################################################## EDIT - CERTIFICATION
                  case 'certification':
                    $detail = Profile::getDetail($parameters[2], $_SESSION['id'], 'certification');

                    $this -> data['smarty_year']   = $detail[0] -> year;
                    $this -> data['smarty_school'] = $detail[0] -> school;
                    $this -> data['smarty_name']   = $detail[0] -> name;

                    if (isset($_POST['profile_edit_certification_submit'])) {
                      $duplicity = Duplicity::evaluateEditForm($detail, $_POST);
                      $profile = new ProfileCertification($_POST);

                      $validation = $profile -> validate();
                      if ($validation === TRUE && !$duplicity) {
                        if ($profile -> edit($parameters[2], $parameters[1], $profile -> getProfileDetails())) {
                          $message -> add('Uloženo!', 'OK', 'ok');
                        } else $message -> add('Nepodařilo se upravit informace!', 'Chyba', 'error');
                      } elseif ($duplicity) {
                        $message -> add('Odesílají se duplicitní data!', 'Chyba', 'error');
                      } else $message -> add($validation, 'Chyba', 'error');
                    }

                    $this -> view = 'profile_edit_certification';
                    break;

                    ############################################################## EDIT - WORK
                    case 'work':
                      $detail = Profile::getDetail($parameters[2], $_SESSION['id'], 'work');

                      $this -> data['smarty_startYear']   = $detail[0] -> startYear;
                      $this -> data['smarty_endYear']     = $detail[0] -> endYear;
                      $this -> data['smarty_employer']    = $detail[0] -> employer;
                      $this -> data['smarty_position']    = $detail[0] -> position;
                      $this -> data['smarty_description'] = $detail[0] -> description;

                      if (isset($_POST['profile_edit_work_submit'])) {
                        $profile = new ProfileWork($_POST);
                        $duplicity = Duplicity::evaluateEditForm($detail, $_POST);

                        $validation = $profile -> validate();
                        if ($validation === TRUE && !$duplicity) {
                          if ($profile -> edit($parameters[2], $parameters[1], $profile -> getProfileDetails())) {
                            $message -> add('Uloženo!', 'OK', 'ok');
                          } else $message -> add('Nepodařilo se upravit informace!', 'Chyba', 'error');
                        } elseif ($duplicity) {
                          $message -> add('Odesílají se duplicitní data!', 'Chyba', 'error');
                        } else $message -> add($validation, 'Chyba', 'error');
                      }

                      $this -> view = 'profile_edit_work';
                      break;

                  ############################################################## EDIT - SKILLS
                  case 'skills':
                    $detail = Profile::getDetail($parameters[2], $_SESSION['id'], 'skills');

                    $this -> data['smarty_name']  = $detail[0] -> name;
                    $this -> data['smarty_level'] = $detail[0] -> level;

                    if (isset($_POST['profile_edit_skills_submit'])) {
                      $profile = new ProfileSkills($_POST);
                      $duplicity = Duplicity::evaluateEditForm($detail, $_POST);

                      $validation = $profile -> validate();
                      if ($validation === TRUE && !$duplicity) {
                        if ($profile -> edit($parameters[2], $parameters[1], $profile -> getProfileDetails())) {
                          $message -> add('Uloženo!', 'OK', 'ok');
                        } else $message -> add('Nepodařilo se upravit informace!', 'Chyba', 'error');
                      } elseif ($duplicity) {
                        $message -> add('Odesílají se duplicitní data!', 'Chyba', 'error');
                      } else $message -> add($validation, 'Chyba', 'error');
                    }

                    $this -> view = 'profile_edit_skills';
                    break;

                  ############################################################## EDIT - LANGUAGE
                  case 'language':
                    $detail = Profile::getDetail($parameters[2], $_SESSION['id'], 'language');

                    $this -> data['smarty_name']  = $detail[0] -> name;
                    $this -> data['smarty_level'] = $detail[0] -> level;

                    if (isset($_POST['profile_edit_language_submit'])) {
                      $profile = new ProfileLanguage($_POST);
                      $duplicity = Duplicity::evaluateEditForm($detail, $_POST);

                      $validation = $profile -> validate();
                      if ($validation === TRUE && !$duplicity) {
                        if ($profile -> edit($parameters[2], $parameters[1], $profile -> getProfileDetails())) {
                          $message -> add('Uloženo!', 'OK', 'ok');
                          $this -> redirectToUrl('profile');
                        } else $message -> add('Nepodařilo se upravit informace!', 'Chyba', 'error');
                      } elseif ($duplicity) {
                        $message -> add('Odesílají se duplicitní data!', 'Chyba', 'error');
                      } else $message -> add($validation, 'Chyba', 'error');
                    }

                    $this -> view = 'profile_edit_language';
                    break;

                  default:
                    $this -> view = 'NotFound';
                    break;
                }
              } else { // if (isset($parameters[1]) ~ just edit profile
                if (isset($_POST['profile_edit_form_submit'])) {
                  $this -> view = 'profile_edit_form';

                  foreach ($_POST as $key => $value) {
                    if (isset($_POST[$key])) {
                      $_POST[$key] = htmlspecialchars(trim($value));
                      $this -> data['smarty_' . $key] = $_POST[$key];
                    }
                  }

                  if (!empty($_POST['profile_edit_form_firstname']) &&
                      !empty($_POST['profile_edit_form_lastname']) &&
                      !empty($_POST['profile_edit_form_email']) &&
                      !empty($_POST['profile_edit_form_zipcode']) &&
                      !empty($_POST['profile_edit_form_address']) &&
                      !empty($_POST['profile_edit_form_telephone'])) {

                    $userdata = array(
                      'firstName' => $_POST['profile_edit_form_firstname'],
                      'lastName'  => $_POST['profile_edit_form_lastname'],
                      'email'     => $_POST['profile_edit_form_email'],
                      'address'   => $_POST['profile_edit_form_address'],
                      'address2'  => $_POST['profile_edit_form_address2'],
                      'zipCode'   => $_POST['profile_edit_form_zipcode'],
                      'telephone' => $_POST['profile_edit_form_telephone'],
                      'city'      => $_POST['profile_edit_form_city']
                    );

                    if ($user -> edit($_SESSION['id'], $userdata)) { $message -> add('Změny byly uloženy!', 'ok', 'ok'); }
                    else { $message -> add('Nepovedlo se uložit změny!', 'Chyba', 'error'); }
                  } else { $message -> add('Nebyla vyplněna všechna povinná pole!', 'Chyba', 'error'); }

                } else {
                  $profileData = $user -> getById($_SESSION['id']);

                  $this -> data['smarty_profile_edit_form_firstname']       = $profileData['firstName'];
                  $this -> data['smarty_profile_edit_form_lastname']        = $profileData['lastName'];
                  $this -> data['smarty_profile_edit_form_address']         = $profileData['address'];
                  $this -> data['smarty_profile_edit_form_address2']        = $profileData['address2'];
                  $this -> data['smarty_profile_edit_form_zipcode']         = $profileData['zipCode'];
                  $this -> data['smarty_profile_edit_form_telephone']       = $profileData['telephone'];
                  $this -> data['smarty_profile_edit_form_city']            = $profileData['city'];
                  $this -> data['smarty_profile_edit_form_registeredsince'] = $profileData['registeredSince'];
                  $this -> data['smarty_profile_edit_form_email']           = $profileData['email'];

                  $this -> view = 'profile_edit_form';
                }
              }

              break;


            case 'add':
              if (isset($parameters[1])) {
                switch ($parameters[1]) {

                  ############################################################## ADD - EDUCATION
                  case 'education':
                    if (isset($_POST['profile_add_education_submit'])) {
                      $profile = new ProfileEducation($_POST);

                      $validation = $profile -> validate();
                      if ($validation === TRUE) {
                        if ($profile -> add($_SESSION['id'], 'profile_education', $profile -> getProfileDetails())) {
                          $message -> add('Uloženo', 'OK', 'ok');
                          $this -> redirectToUrl('profile');
                        } else $message -> add('Nepodařilo se uložit detail o vzdělání!', 'Chyba', 'error');
                      } else {
                        $message -> add($validation, 'Chyba', 'error');
                        $this -> data['smarty_profile_add_education_startYear'] = $_POST['profile_add_education_startYear'];
                        $this -> data['smarty_profile_add_education_endYear']   = $_POST['profile_add_education_endYear'];
                        $this -> data['smarty_profile_add_education_school']    = $_POST['profile_add_education_school'];
                        $this -> data['smarty_profile_add_education_title']     = $_POST['profile_add_education_title'];
                        $this -> data['smarty_profile_add_education_startYear'] = $_POST['profile_add_education_startYear'];
                        $this -> data['smarty_profile_add_education_startYear'] = $_POST['profile_add_education_startYear'];

                        $this -> view = 'profile_add_education';
                      }
                    } else {
                      $this -> view = 'profile_add_education';
                    }
                    break;

                  ############################################################## ADD - WORK
                  case 'work':
                    if (isset($_POST['profile_add_work_submit'])) {
                      $profile = new ProfileWork($_POST);

                      $validation = $profile -> validate();
                      if ($validation === TRUE) {
                        if ($profile -> add($_SESSION['id'], 'profile_work', $profile -> getProfileDetails())) {
                          $message -> add('Uloženo', 'OK', 'ok');
                          $this -> redirectToUrl('profile');
                        } else $message -> add('Nepodařilo se uložit detail o předchozí praxi!', 'Chyba', 'error');
                      } else {
                        $message -> add($validation, 'Chyba', 'error');
                        $this -> data['smarty_profile_add_work_startYear'] = $_POST['profile_add_work_startYear'];
                        $this -> data['smarty_profile_add_work_endYear'] = $_POST['profile_add_work_endYear'];
                        $this -> data['smarty_profile_add_work_employer'] = $_POST['profile_add_work_employer'];
                        $this -> data['smarty_profile_add_work_position'] = $_POST['profile_add_work_position'];
                        $this -> data['smarty_profile_add_work_description'] = $_POST['profile_add_work_description'];

                        $this -> view = 'profile_add_work';
                      }
                    } else {
                      $this -> view = 'profile_add_work';
                    }

                    break;

                  ############################################################## ADD - CERTIFICATION
                  case 'certification':
                    if (isset($_POST['profile_add_certification_submit'])) {
                      $profile = new ProfileCertification($_POST);

                      $validation = $profile -> validate();
                      if ($validation === TRUE) {
                        if ($profile -> add($_SESSION['id'], 'profile_certification', $profile -> getProfileDetails())) {
                          $message -> add('Uloženo', 'OK', 'ok');
                          $this -> redirectToUrl('profile');
                        } else $message -> add('Nepodařilo se uložit detail o certifikaci!', 'Chyba', 'error');
                      } else {
                        $message -> add($validation, 'Chyba', 'error');
                        $this -> data['smarty_profile_add_certification_name']   = $_POST['profile_add_certification_name'];
                        $this -> data['smarty_profile_add_certification_school'] = $_POST['profile_add_certification_school'];
                        $this -> data['smarty_profile_add_certification_year']   = $_POST['profile_add_certification_year'];
                        $this -> view = 'profile_add_certification';
                      }
                    } else {
                      $this -> view = 'profile_add_certification';
                    }
                    break;

                    ############################################################ ADD - SKILLS
                    case 'skills':
                      if (isset($_POST['profile_add_skills_submit'])) {
                        $profile = new ProfileSkills($_POST);

                        $validation = $profile -> validate();
                        if ($validation === TRUE) {
                          if ($profile -> add($_SESSION['id'], 'profile_skills', $profile -> getProfileDetails())) {
                            $message -> add('Uloženo', 'OK', 'ok');
                            $this -> redirectToUrl('profile');
                          } else $message -> add('Nepodařilo se uložit detail o dovednostech!', 'Chyba', 'error');
                        } else {
                          $message -> add($validation, 'Chyba', 'error');
                          $this -> data['smarty_profile_add_skills_name']   = $_POST['profile_add_skills_name'];
                          $this -> data['smarty_profile_add_skills_level'] = $_POST['profile_add_skills_level'];
                          $this -> view = 'profile_add_skills';
                        }
                      } else {
                        $this -> view = 'profile_add_skills';
                      }
                      break;

                    ############################################################ ADD - LANGUAGE
                    case 'language':
                      if (isset($_POST['profile_add_language_submit'])) {
                        $profile = new ProfileLanguage($_POST);

                        $validation = $profile -> validate();
                        if ($validation === TRUE) {
                          if ($profile -> add($_SESSION['id'], 'profile_language', $profile -> getProfileDetails())) {
                            $message -> add('Uloženo', 'OK', 'ok');
                            $this -> redirectToUrl('profile');
                          } else $message -> add('Nepodařilo se uložit detail o jazyku!', 'Chyba', 'error');
                        } else {
                          $message -> add($validation, 'Chyba', 'error');
                          $this -> data['smarty_profile_add_language_name']  = $_POST['profile_add_language_name'];
                          $this -> data['smarty_profile_add_language_level'] = $_POST['profile_add_language_level'];
                          $this -> view = 'profile_add_language';
                        }
                      } else {
                        $this -> view = 'profile_add_language';
                      }
                      break;

                  default:
                    $this -> view = 'NotFound';
                    break;
                }
              } else $this -> view = 'NotFound';
              break;

            #################################################################### DELETE
            case 'delete':
              if (isset($parameters[1])) {
                if (isset($parameters[2])) {
                  if (Profile::deleteDetail($parameters[2], $_SESSION['id'], $parameters[1])) {
                    $message -> add('Smazáno!', 'OK', 'ok');
                    $this -> redirectToUrl('profile');
                  } else {
                    $message -> add('Detail se nepodařilo vymazat!', 'Chyba', 'error');
                    $this -> redirectToUrl('profile');
                  }
                }
              }

              break;

            default:
              $this -> view = 'NotFound';
              break;
          }
        } else {
          # just display user profile
          $this -> displayProfile($user, $session, $message);
        }
      }

      $this -> displayView();
    }

    public function displayProfile($user, $session, $message) {
      $profileData = $user -> getById($_SESSION['id']);

      $this -> data['smarty_profile_firstname']       = $profileData['firstName'];
      $this -> data['smarty_profile_lastname']        = $profileData['lastName'];
      $this -> data['smarty_profile_address']         = $profileData['address'];
      $this -> data['smarty_profile_address2']        = $profileData['address2'];
      $this -> data['smarty_profile_zipcode']         = $profileData['zipCode'];
      $this -> data['smarty_profile_telephone']       = $profileData['telephone'];
      $this -> data['smarty_profile_city']            = $profileData['city'];
      $this -> data['smarty_profile_registeredsince'] = $profileData['registeredSince'];
      $this -> data['smarty_profile_email']           = $profileData['email'];

      # check payment status

      $payment_status = new Payment($_SESSION['id']);
      $active = $payment_status -> isActive();
      if ($active) $this -> data['smarty_profile_payment_status'] = 'Zaplaceno do ' . date_format(date_create($active), 'd.m. Y');
      else $this -> data['smarty_profile_payment_status'] = 'Nezaplaceno (<a href="/payment">zaplatit</a>)';

      # check if there are any other data for the profile

      # display education info on profile
      $education = Profile::getData('profile_education', $_SESSION['id']);
      if (!empty($education)) {
        $education_array = array();

        foreach ($education as $rowId => $rowObject) {
          $education_array[$rowId]['degree']    = $education[$rowId]['degree'];
          $education_array[$rowId]['startYear'] = $education[$rowId]['startYear'];
          $education_array[$rowId]['endYear']   = $education[$rowId]['endYear'];
          $education_array[$rowId]['school']    = $education[$rowId]['school'];
          $education_array[$rowId]['title']     = $education[$rowId]['title'];
          $education_array[$rowId]['id']        = $education[$rowId]['id'];
        }

        $this -> data['smarty_education_data'] = $education_array;
        $this -> data['smarty_education_display'] = TRUE;
      } else $this -> data['smarty_education_display'] = FALSE;

      # display work info on profile
      $work = Profile::getData('profile_work', $_SESSION['id']);
      if (!empty($work)) {
        $work_array = array();

        foreach ($work as $rowId => $rowObject) {
          $work_array[$rowId]['employer']    = $work[$rowId]['employer'];
          $work_array[$rowId]['id']          = $work[$rowId]['id'];
          $work_array[$rowId]['startYear']   = $work[$rowId]['startYear'];
          $work_array[$rowId]['endYear']     = $work[$rowId]['endYear'];
          $work_array[$rowId]['description'] = $work[$rowId]['description'];
          $work_array[$rowId]['position']    = $work[$rowId]['position'];
        }

        $this -> data['smarty_work_data'] = $work_array;
        $this -> data['smarty_work_display'] = TRUE;
      } else $this -> data['smarty_work_display'] = FALSE;

      # display certification info on profile
      $certification = Profile::getData('profile_certification', $_SESSION['id']);
      if (!empty($certification)) {
        $certification_array = array();
        $this -> data['smarty_certification_display'] = TRUE;

        foreach ($certification as $rowId => $rowObject) {
          $certification_array[$rowId]['year']   = $certification[$rowId]['year'];
          $certification_array[$rowId]['school'] = $certification[$rowId]['school'];
          $certification_array[$rowId]['name']   = $certification[$rowId]['name'];
          $certification_array[$rowId]['id']     = $certification[$rowId]['id'];
        }

        $this -> data['smarty_certification_data'] = $certification_array;

      } else $this -> data['smarty_certification_display'] = FALSE;

      # display skills info on profile
      $skills = Profile::getData('profile_skills', $_SESSION['id']);
      if (!empty($skills)) {
        $skills_array = array();
        $this -> data['smarty_skills_display'] = TRUE;

        foreach ($skills as $rowId => $rowObject) {
          $skills_array[$rowId]['name']   = $skills[$rowId]['name'];
          $skills_array[$rowId]['level'] = $skills[$rowId]['level'];
          $skills_array[$rowId]['id']     = $skills[$rowId]['id'];
        }

        $this -> data['smarty_skills_data'] = $skills_array;

      } else $this -> data['smarty_skills_display'] = FALSE;

      # display language info on profile
      $language = Profile::getData('profile_language', $_SESSION['id']);
      if (!empty($language)) {
        $language_array = array();
        $this -> data['smarty_language_display'] = TRUE;

        foreach ($language as $rowId => $rowObject) {
          $language_array[$rowId]['name']  = $language[$rowId]['name'];
          $language_array[$rowId]['level'] = $language[$rowId]['level'];
          $language_array[$rowId]['id']    = $language[$rowId]['id'];
        }

        $this -> data['smarty_language_data'] = $language_array;

      } else $this -> data['smarty_language_display'] = FALSE;

    }

  }
?>
