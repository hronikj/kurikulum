<?php
  class StaticPageController extends Controller {
    private $page;

    public function __construct($page) {
      $this -> page = $page;
    }

    public function parseParameters($parameters) {
      switch ($this -> page) {
        case 'index':
          $this -> getSmartyVariables();
          $this -> view = 'index';
          break;

        default:
          # code...
          break;
      }

      $this -> displayView();
    }

    private function getSmartyVariables() {
      $pageid = dibi::query('SELECT [id] FROM [static_pages] WHERE [name] = %s', $this -> page) -> fetchSingle();
      $data = dibi::query('SELECT * FROM [static_pages_texts] WHERE [pageid] = %i', $pageid) -> fetchAll();

      foreach ($data as $n => $row) {
        $this -> data['smarty_' . $row['key']] = $row['value'];
      }
    }

  }
?>
