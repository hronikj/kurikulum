# ************************************************************
# Sequel Pro SQL dump
# Version 4096
#
# http://www.sequelpro.com/
# http://code.google.com/p/sequel-pro/
#
# Host: localhost (MySQL 5.5.42)
# Database: kurikulum
# Generation Time: 2015-05-16 08:28:15 +0000
# ************************************************************


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


# Dump of table articles
# ------------------------------------------------------------

DROP TABLE IF EXISTS `articles`;

CREATE TABLE `articles` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `headline` varchar(255) DEFAULT '',
  `perex` text,
  `text` text,
  `keywords` varchar(255) DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  `categoryid` int(11) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `categoryid->categories.id` (`categoryid`),
  CONSTRAINT `categoryid->categories.id` FOREIGN KEY (`categoryid`) REFERENCES `categories` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `articles` WRITE;
/*!40000 ALTER TABLE `articles` DISABLE KEYS */;

INSERT INTO `articles` (`id`, `headline`, `perex`, `text`, `keywords`, `description`, `categoryid`)
VALUES
	(1,'Toto je titulek clanku','Toto je zkraceny uryvek clanku...','Toto jsou samotna data clanku!','klicove slovo 1, klicoveslovo2, klicoveslov3','This is description',1),
	(2,'Toto je titulek clanku','Toto je zkraceny uryvek clanku...','Toto jsou samotna data clanku!','klicove slovo 1, klicoveslovo2, klicoveslov3','This is description',1),
	(3,'Toto je titulek clanku','Toto je zkraceny uryvek clanku...','Toto jsou samotna data clanku!','klicove slovo 1, klicoveslovo2, klicoveslov3','This is description',1),
	(4,'Toto je titulek clanku','Toto je zkraceny uryvek clanku...','Toto jsou samotna data clanku!','klicove slovo 1, klicoveslovo2, klicoveslov3','This is description',1),
	(5,'Toto je titulek clanku','Toto je zkraceny uryvek clanku...','Toto jsou samotna data clanku!','klicove slovo 1, klicoveslovo2, klicoveslov3','This is description',1),
	(6,'Toto je titulek clanku','Toto je zkraceny uryvek clanku...','Toto jsou samotna data clanku!','klicove slovo 1, klicoveslovo2, klicoveslov3','This is description',1),
	(7,'Toto je titulek clanku','Toto je zkraceny uryvek clanku...','Toto jsou samotna data clanku!','klicove slovo 1, klicoveslovo2, klicoveslov3','This is description',1),
	(8,'Toto je titulek clanku','Toto je zkraceny uryvek clanku...','Toto jsou samotna data clanku!','klicove slovo 1, klicoveslovo2, klicoveslov3','This is description',1),
	(9,'Toto je titulek clanku','Toto je zkraceny uryvek clanku...','Toto jsou samotna data clanku!','klicove slovo 1, klicoveslovo2, klicoveslov3','This is description',1);

/*!40000 ALTER TABLE `articles` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table authlogs
# ------------------------------------------------------------

DROP TABLE IF EXISTS `authlogs`;

CREATE TABLE `authlogs` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `userid` int(11) unsigned NOT NULL,
  `ts` datetime DEFAULT NULL,
  `httpUserAgent` varchar(255) DEFAULT NULL,
  `requestMethod` varchar(10) DEFAULT NULL,
  `username` varchar(255) DEFAULT NULL,
  `success` tinyint(1) unsigned DEFAULT NULL,
  `httpClientHost` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `authlogs.userid->users.id` (`userid`),
  CONSTRAINT `authlogs.userid->users.id` FOREIGN KEY (`userid`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `authlogs` WRITE;
/*!40000 ALTER TABLE `authlogs` DISABLE KEYS */;

INSERT INTO `authlogs` (`id`, `userid`, `ts`, `httpUserAgent`, `requestMethod`, `username`, `success`, `httpClientHost`)
VALUES
	(1,16,'0000-00-00 00:00:00','afdasfa','GET','adfasf',1,'fasdfdsads');

/*!40000 ALTER TABLE `authlogs` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table categories
# ------------------------------------------------------------

DROP TABLE IF EXISTS `categories`;

CREATE TABLE `categories` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `categories` WRITE;
/*!40000 ALTER TABLE `categories` DISABLE KEYS */;

INSERT INTO `categories` (`id`, `name`)
VALUES
	(1,'Testovaci kategorie');

/*!40000 ALTER TABLE `categories` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table errorMessages
# ------------------------------------------------------------

DROP TABLE IF EXISTS `errorMessages`;

CREATE TABLE `errorMessages` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `text` varchar(255) NOT NULL DEFAULT '',
  `type` varchar(255) NOT NULL DEFAULT '',
  `class` varchar(255) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table noticeMessages
# ------------------------------------------------------------

DROP TABLE IF EXISTS `noticeMessages`;

CREATE TABLE `noticeMessages` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `text` varchar(255) NOT NULL DEFAULT '',
  `type` varchar(255) NOT NULL DEFAULT '',
  `class` varchar(255) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table passwordTokens
# ------------------------------------------------------------

DROP TABLE IF EXISTS `passwordTokens`;

CREATE TABLE `passwordTokens` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `userid` int(11) unsigned DEFAULT NULL,
  `token` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `passwordTokens.userid->users.id` (`userid`),
  CONSTRAINT `passwordTokens.userid->users.id` FOREIGN KEY (`userid`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `passwordTokens` WRITE;
/*!40000 ALTER TABLE `passwordTokens` DISABLE KEYS */;

INSERT INTO `passwordTokens` (`id`, `userid`, `token`)
VALUES
	(42,15,'d0fb6bd4319ff0b10661a3aaf6f81abb'),
	(43,15,'afc7407a7ca35e22b9f1893367fcb919');

/*!40000 ALTER TABLE `passwordTokens` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table registrations
# ------------------------------------------------------------

DROP TABLE IF EXISTS `registrations`;

CREATE TABLE `registrations` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `email` varchar(255) DEFAULT '',
  `token` varchar(255) DEFAULT '',
  `firstName` varchar(255) DEFAULT NULL,
  `lastName` varchar(255) DEFAULT NULL,
  `address` varchar(255) DEFAULT NULL,
  `address2` varchar(255) DEFAULT NULL,
  `zipCode` int(6) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  `telephone` varchar(20) DEFAULT NULL,
  `registeredSince` datetime DEFAULT NULL,
  `city` varchar(50) DEFAULT NULL,
  `blocked` tinyint(1) NOT NULL DEFAULT '0',
  `userlevel` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `registrations` WRITE;
/*!40000 ALTER TABLE `registrations` DISABLE KEYS */;

INSERT INTO `registrations` (`id`, `email`, `token`, `firstName`, `lastName`, `address`, `address2`, `zipCode`, `password`, `telephone`, `registeredSince`, `city`, `blocked`, `userlevel`)
VALUES
	(1,'test@test.cc','2d3a3a4597a2c34fbf4378e3f4d78625','Jmeno','Prijmeni','Address 1','Address 2',12345,'test','+420123456789',NULL,'Brno',0,0);

/*!40000 ALTER TABLE `registrations` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table successMessages
# ------------------------------------------------------------

DROP TABLE IF EXISTS `successMessages`;

CREATE TABLE `successMessages` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `text` varchar(255) NOT NULL DEFAULT '',
  `type` varchar(255) NOT NULL DEFAULT '',
  `class` varchar(255) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `successMessages` WRITE;
/*!40000 ALTER TABLE `successMessages` DISABLE KEYS */;

INSERT INTO `successMessages` (`id`, `text`, `type`, `class`)
VALUES
	(1,'Toto je testovaci zprava zobrazovana pri uspechu','ok','ok');

/*!40000 ALTER TABLE `successMessages` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table test
# ------------------------------------------------------------

DROP TABLE IF EXISTS `test`;

CREATE TABLE `test` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `test` WRITE;
/*!40000 ALTER TABLE `test` DISABLE KEYS */;

INSERT INTO `test` (`id`, `name`)
VALUES
	(1,'testovaci data z databaze');

/*!40000 ALTER TABLE `test` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table users
# ------------------------------------------------------------

DROP TABLE IF EXISTS `users`;

CREATE TABLE `users` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `firstName` varchar(255) DEFAULT NULL,
  `lastName` varchar(255) DEFAULT NULL,
  `registeredSince` datetime DEFAULT NULL,
  `address` varchar(255) DEFAULT NULL,
  `address2` varchar(255) DEFAULT NULL,
  `zipCode` int(6) unsigned NOT NULL,
  `password` varchar(255) DEFAULT NULL,
  `userlevel` int(1) unsigned NOT NULL DEFAULT '0',
  `email` varchar(255) NOT NULL DEFAULT '',
  `telephone` varchar(20) DEFAULT NULL,
  `blocked` int(1) unsigned NOT NULL DEFAULT '0',
  `city` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;

INSERT INTO `users` (`id`, `firstName`, `lastName`, `registeredSince`, `address`, `address2`, `zipCode`, `password`, `userlevel`, `email`, `telephone`, `blocked`, `city`)
VALUES
	(15,'atestatsa','testadsafa',NULL,'asdfsafa dfs',NULL,12345,'6655a82577604ddb635c7ee1b86ab4ece0a0f06b',0,'&lt;skfsjsadfsak&gt;///?&gt;',NULL,0,NULL),
	(16,NULL,NULL,NULL,NULL,NULL,0,'a94a8fe5ccb19ba61c4c0873d391e987982fbbd3',0,'test@test.cz',NULL,0,NULL),
	(17,NULL,NULL,NULL,NULL,NULL,0,NULL,0,'',NULL,0,NULL),
	(18,NULL,NULL,NULL,NULL,NULL,0,NULL,0,'',NULL,0,NULL),
	(19,'Jmeno','Prijmeni',NULL,'Address 1','Address 2',12345,'test',0,'test@test.ac','+420123456789',0,'Brno'),
	(20,'Jmeno','Prijmeni',NULL,'Address 1','Address 2',12345,'test',0,'test@test.ac','+420123456789',0,'Brno'),
	(21,'Jmeno','Prijmeni',NULL,'Address 1','Address 2',12345,'test',0,'test@test.ac','+420123456789',0,'Brno'),
	(22,'Jmeno','Prijmeni','2015-05-11 13:33:37','Address 1','Address 2',12345,'a94a8fe5ccb19ba61c4c0873d391e987982fbbd3',0,'test@test.aa','+420123456789',0,'Brno'),
	(23,'Jmeno','Prijmeni','2015-05-11 13:33:37','Address 1','Address 2',12345,'a94a8fe5ccb19ba61c4c0873d391e987982fbbd3',0,'test@test.aa','+420123456789',0,'Brno'),
	(24,'Jmeno','Prijmeni','2015-05-11 13:33:37','Address 1','Address 2',12345,'a94a8fe5ccb19ba61c4c0873d391e987982fbbd3',0,'test@test.aa','+420123456789',0,'Brno'),
	(25,'Jmeno','Prijmeni',NULL,'Address 1','Address 2',12345,'test',0,'test@test.ac','+420123456789',0,'Brno');

/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;



/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
