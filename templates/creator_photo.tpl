{include file='default_header.tpl'}
  {include file='default_messages.tpl'}
  <h1> Photo upload test </h1>

  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
  <script>
    $(document).ready(function(){
        $("#checkbox").click(function(){
            $("#upload-form").toggle();
        });
    });
  </script>
  <img src="" width="300" />
  {if isset($smarty_image_display) && $smarty_image_display == TRUE}
    <img src="/photos/" width="300" />
  {/if}

  <form action="" method="POST" enctype="multipart/form-data" name="creator_form">
    <div id="upload-form">
      Select image to upload:
      <input type="file" name="fileToUpload" id="fileToUpload">
    </div>

    <input type="checkbox" id="checkbox" name="creator_form_checkbox" checked="checked"> Nahrát obrázek </input> <br />
    <input type="submit" value="< Zpět" name="creator_form_back" class="btn" />
    <input type="submit" value="Pokračovat >" name="creator_form_submit" class="btn" />

  </form>



{include file='default_footer.tpl'}
