{include file='default_header.tpl'}
  {include file='default_messages.tpl'}

  <form name="profile_add_work_form" method="POST" action="">
    <table>
      <tr>
        <td> Rok zahájení </td> <td> <input type="text" name="profile_add_work_startYear" value="{if isset($smarty_profile_add_work_startYear)}{$smarty_profile_add_work_startYear}{/if}"/> </td>
      </tr>

      <tr>
        <td> Rok ukončení: </td> <td> <input type="text" name="profile_add_work_endYear" value="{if isset($smarty_profile_add_work_endYear)}{$smarty_profile_add_work_endYear}{/if}" /> </td>
      </tr>

      <tr>
        <td> Zaměstnavatel </td> <td> <input type="text" name="profile_add_work_employer" value="{if isset($smarty_profile_add_work_employer)}{$smarty_profile_add_work_employer}{/if}" /> </td>
      </tr>

      <tr>
        <td> Pozice </td> <td> <input type="text" name="profile_add_work_position" value="{if isset($smarty_profile_add_work_position)}{$smarty_profile_add_work_position}{/if}" /> </td>
      </tr>

      <tr>
        <td> Popis </td> <td> <textarea type="text" name="profile_add_work_description">{if isset($smarty_profile_add_work_description)}{$smarty_profile_add_work_description}{/if}</textarea> </td>
      </tr>

    </table>
    <input type="submit" value="Odeslat" name="profile_add_work_submit" class="btn" />
  </form>

{include file='default_footer.tpl'}
