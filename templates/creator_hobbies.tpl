{include file='default_header.tpl'}
  {include file='default_messages.tpl'}
    <style>
        .fieldgroup label.error {
        color: #FB3A3A;
        display: inline-block;
        margin: 4px 0 5px 125px;
        padding: 0;
        text-align: left;
        width: 220px;
      }

      .error {
        color: red;
      }

    </style>

    <h1> Průvodce vytvořením CV - záliby, volnočasové aktivity </h1>

    {for $row_count=0 to $smarty_row_count - 1}
    <form name="creator_form_header" id="creator_form_header" action="" method="POST">
      <input type="text" name="id[]" value="{$row_count}" hidden="true" />
      <table>
        <tr>  <td> <textarea name="hobbies[]">{if isset($smarty_hobbies[$row_count])}{$smarty_hobbies[$row_count]}{/if}</textarea> <br />
                  {if isset($smarty_validation_hobbies[$row_count])}
                    {$smarty_validation_hobbies[$row_count]}
                  {/if}
              </td>
        </tr>
      </table>
      <input type="submit" value="< Zpět" name="creator_form_back" class="btn" />
      <input type="submit" value="Pokračovat >" name="creator_form_submit" class="btn" />
    </form>
    {/for}


{include file='default_footer.tpl'}
