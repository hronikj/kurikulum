{include file='default_header.tpl'}
  {include file='default_messages.tpl'}
    <h1> Průvodce vytvořením CV - poslední krok </h1>

    <h2> Vzdělání </h2>
    <table id="dataTable" class="form" border="1">
      <tbody>
        {for $row_count=0 to $smarty_education_row_count - 1}
        <tr>
          <p>
            <td> <label for="creator_form_education_school"> Instituce: </label> <input disabled required="required" type="text" name="creator_form_education_school[]" value="{if isset($smarty_creator_form_education_school[$row_count])}{$smarty_creator_form_education_school[$row_count]}{/if}" />  </td>
            <td> <label for="creator_form_education_title"> Titul: </label> <input disabled required="required" type="text" name="creator_form_education_title[]" value="{if isset($smarty_creator_form_education_title[$row_count])}{$smarty_creator_form_education_title[$row_count]}{/if}" /> </td>
            <td> <label for="creator_form_education_startYear"> Od: </label> <input disabled required="required" type="text" name="creator_form_education_startYear[]" value="{if isset($smarty_creator_form_education_startYear[$row_count])}{$smarty_creator_form_education_startYear[$row_count]}{/if}" /> </td>
            <td> <label for="creator_form_education_endYear"> Do: </label> <input disabled required="required" type="text" name="creator_form_education_endYear[]" value="{if isset($smarty_creator_form_education_endYear[$row_count])}{$smarty_creator_form_education_endYear[$row_count]}{/if}" /> </td>
            <td> <label for="creator_form_education_degree"> Stupeň: </label> {if isset($smarty_creator_form_education_degree[$row_count])}{$smarty_creator_form_education_degree[$row_count]}{/if} </td>
          </p>
        </tr>
        {/for}
      </tbody>
    </table>

    <h2> Praxe </h2>
    <table id="dataTable" class="form" border="1">
      <tbody>
        {for $row_count=0 to $smarty_work_row_count - 1}
        <tr>
          <p>
            <td> <label for="creator_form_work_employer"> Zaměstnavatel: </label> <input disabled required="required" type="text" name="creator_form_work_employer[]" value="{if isset($smarty_creator_form_work_employer[$row_count])}{$smarty_creator_form_work_employer[$row_count]}{/if}" />  </td>
            <td> <label for="creator_form_work_position"> Pozice: </label> <input disabled required="required" type="text" name="creator_form_work_position[]" value="{if isset($smarty_creator_form_work_position[$row_count])}{$smarty_creator_form_work_position[$row_count]}{/if}" /> </td>
            <td> <label for="creator_form_work_startYear"> Od: </label> <input disabled required="required" type="text" name="creator_form_work_startYear[]" value="{if isset($smarty_creator_form_work_startYear[$row_count])}{$smarty_creator_form_work_startYear[$row_count]}{/if}" /> </td>
            <td> <label for="creator_form_work_endYear"> Do: </label> <input disabled required="required" type="text" name="creator_form_work_endYear[]" value="{if isset($smarty_creator_form_work_endYear[$row_count])}{$smarty_creator_form_work_endYear[$row_count]}{/if}" /> </td>
            <td> <label for="creator_form_work_description"> Popis: </label> <textarea disabled name="creator_form_work_description[]">{if isset($smarty_creator_form_work_description[$row_count])}{$smarty_creator_form_work_description[$row_count]}{/if}</textarea>
          </p>
        </tr>
        {/for}
      </tbody>
    </table>

    <h2> Dovednosti </h2>
    <table id="dataTable" class="form" border="1">
      <tbody>
        {for $row_count=0 to $smarty_skills_row_count - 1}
        <tr>
          <p>
            <td> <label for="creator_form_skills_name"> Název: </label> <input disabled required="required" type="text" name="creator_form_skills_name[]" value="{if isset($smarty_creator_form_skills_name[$row_count])}{$smarty_creator_form_skills_name[$row_count]}{/if}" />  </td>
            <td> <label for="creator_form_skills_level"> Stupeň: </label> {if isset($smarty_creator_form_skills_level[$row_count])}{$smarty_creator_form_skills_level[$row_count]}{/if}
            </td>

          </p>
        </tr>
        {/for}
      </tbody>
    </table>

    <h2> Jazyky </h2>
    <table id="dataTable" class="form" border="1">
      <tbody>
        {for $row_count=0 to $smarty_languages_row_count - 1}
        <tr>
          <p>
            <td> <label for="creator_form_languages_name"> Název: </label> <input disabled required="required" type="text" name="creator_form_languages_name[]" value="{if isset($smarty_creator_form_languages_name[$row_count])}{$smarty_creator_form_languages_name[$row_count]}{/if}" />  </td>
            <td> <label for="creator_form_languages_level"> Stupeň: </label> {if isset($smarty_creator_form_languages_level[$row_count])} {$smarty_creator_form_languages_level[$row_count]} {/if}  </td>
          </p>
        </tr>
        {/for}
      </tbody>
    </table>

    <h2> Certifikace </h2>
    <table id="dataTable" class="form" border="1">
      <tbody>
        {for $row_count=0 to $smarty_certification_row_count - 1}
        <tr>
          <p>
            <td> <label for="creator_form_certification_name"> Název certifikátu: </label> <input disabled required="required" type="text" name="creator_form_certification_name[]" value="{if isset($smarty_creator_form_certification_name[$row_count])}{$smarty_creator_form_certification_name[$row_count]}{/if}" />  </td>
            <td> <label for="creator_form_certification_year"> Rok získání: </label> <input disabled required="required" type="text" name="creator_form_certification_year[]" value="{if isset($smarty_creator_form_certification_year[$row_count])}{$smarty_creator_form_certification_year[$row_count]}{/if}" />  </td>
            <td> <label for="creator_form_certification_school"> Školící středisko: </label> <input disabled required="required" type="text" name="creator_form_certification_school[]" value="{if isset($smarty_creator_form_certification_school[$row_count])}{$smarty_creator_form_certification_school[$row_count]}{/if}" />  </td>
          </p>
        </tr>
        {/for}
      </tbody>
    </table>

    <form name="creator_form_final" action="" method="POST">
      <input type="submit" value="< Zpět" name="creator_form_final_back" class="btn" />
      <input type="submit" value="Pokračovat >" name="creator_form_final_submit" class="btn" />
    </form>

{include file='default_footer.tpl'}
