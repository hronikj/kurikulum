{include file='default_header.tpl'}
  {include file='default_messages.tpl'}
  {if $display_content}
    {if isset($smarty_subscription) && $smarty_subscription == TRUE}

    <script type="text/javascript">
      /**
        * Basic jQuery Validation Form Demo Code
        * Copyright Sam Deering 2012
        * Licence: http://www.jquery4u.com/license/
        */
      (function($,W,D) {
          var JQUERY4U = {};

          JQUERY4U.UTIL = {
              setupFormValidation: function() {
                  // form validation rules
                  $("#creator_form").validate({
                      rules: {
                          creator_form_name: "required",

                      },
                      messages: {
                          creator_form_name: "Please enter your firstname",

                      },

                      submitHandler: function(form) {
                          form.submit();
                      }
                  });
              }
          }

          // when the dom has loaded setup form validation rules
          $(D).ready(function($) {
              JQUERY4U.UTIL.setupFormValidation();
          });

      })(jQuery, window, document);
    </script>



      <h1> Průvodce vytvořením CV </h1>
      <form name="creator_form" action="" method="POST" id="creator_form">
        <table>
          <tr>
              <td> Název konfigurace: </td>
              <td> <input type="text" size="73" name="creator_form_name" value="{if isset($smarty_name)}{$smarty_name}{/if}" /> </td>
          </tr>
        </table>

        <p> Vyberte grafickou podobu životopisu: </p>
        <table>
          <tr>
            <td>
              <div style="width: 110px; height: 120px; padding: 5px; background-color: green; color: #fff; text-align: center; font-size: 110px;"> 1 </div>
            </td>
            <td>
              <div style="width: 110px; height: 120px; padding: 5px; background-color: blue; color: #fff; text-align: center; font-size: 121px;"> 2 </div>
            </td>
            <td>
              <div style="width: 110px; height: 120px; padding: 5px; background-color: brown; color: #fff; text-align: center; font-size: 110px;"> 3 </div>
            </td>
            <td>
              <div style="width: 110px; height: 120px; padding: 5px; background-color: magenta; color: #fff; text-align: center; font-size: 110px;"> 4 </div>
            </td>
            <td>
              <div style="width: 110px; height: 120px; padding: 5px; background-color: yellow; color: #fff; text-align: center; font-size: 110px;"> 5 </div>
            </td>
          </tr>
          <tr>
            <td align="center"> <input type="radio" name="creator_form_graphics" value="1" {if (isset($smarty_graphics) && $smarty_graphics == '1')}checked{/if} /> </td>
            <td align="center"> <input type="radio" name="creator_form_graphics" value="2" {if (isset($smarty_graphics) && $smarty_graphics == '2')}checked{/if} /> </td>
            <td align="center"> <input type="radio" name="creator_form_graphics" value="3" {if (isset($smarty_graphics) && $smarty_graphics == '3')}checked{/if} /> </td>
            <td align="center"> <input type="radio" name="creator_form_graphics" value="4" {if (isset($smarty_graphics) && $smarty_graphics == '4')}checked{/if} /> </td>
            <td align="center"> <input type="radio" name="creator_form_graphics" value="5" {if (isset($smarty_graphics) && $smarty_graphics == '5')}checked{/if} /> </td>
          </tr>
        </table>

        <input type="submit" value="Pokračovat" name="creator_form_submit" class="btn" />
      </form>
    {else}
      <p> Je nám líto, ale nemáte aktivní předplatné! </p>
    {/if}
  {else}
    <p> Tato část webu je dostupná pouze přihlášeným uživatelům. <a href="/login"> Přihlásit se </a> </p>
  {/if}

{include file='default_footer.tpl'}
