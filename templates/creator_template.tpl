{include file='default_header.tpl'}
  {include file='default_messages.tpl'}

    <h1> Průvodce vytvořením CV - grafická šablona </h1>

    <form name="creator_form_template" id="creator_form_template" action="" method="POST">
      {$row_count = 1}
      <input type="text" name="id[]" value="{$row_count}" hidden="true" />
      {foreach from=$smarty_templates_array key=k item=i}
        <p>
          {$i.name}
          <input type="radio" name="templateid[]" value="{$i.id}" {if $k == 0}checked{/if} />
        </p>
      {/foreach}

      {if isset($smarty_validation_templateid[0])}
        {$smarty_validation_templateid[0]}
        <br />
      {/if}

      <input type="submit" value="< Zpět" name="creator_form_back" class="btn" />
      <input type="submit" value="Pokračovat >" name="creator_form_submit" class="btn" />
    </form>

{include file='default_footer.tpl'}
