<html>
  <head>
    <title> {$smarty_header_title} </title>
    <link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css" />
    <style>
      body {
        font-family: Helvetica, sans-serif;
        size: 12px;
        margin: 0px;
        padding: 0px;
      }

      /*.error {
        position: absolute;
        z-index: 101;
        top: 0;
        left: 0;
        right: 0;
        background: #d03434;
        text-align: left;
        padding-left: 20px;
        line-height: 2.5;
        overflow: hidden;
        -webkit-box-shadow: 0 0 5px black;
        -moz-box-shadow:    0 0 5px black;
        box-shadow:         0 0 5px black;
        color: #fff;
        font-weight: bold;
        padding-top: 5px;
        padding-bottom: 5px;
      }*/

      .ok {
        position: absolute;
        z-index: 101;
        top: 0;
        left: 0;
        right: 0;
        background: #4de06d;
        text-align: left;
        padding-left: 20px;
        line-height: 2.5;
        overflow: hidden;
        -webkit-box-shadow: 0 0 5px black;
        -moz-box-shadow:    0 0 5px black;
        box-shadow:         0 0 5px black;
        color: #fff;
        font-weight: bold;
        padding-top: 5px;
        padding-bottom: 5px;
      }

      #close {
        color: #fff;
        float: right;
        margin-right: 20px;
      }


      .header {
        width: 100%;
        height: 40px;

        background: #7abcff; /* Old browsers */
        background: -moz-linear-gradient(top, #7abcff 0%, #60abf8 44%, #4096ee 100%); /* FF3.6+ */
        background: -webkit-gradient(linear, left top, left bottom, color-stop(0%,#7abcff), color-stop(44%,#60abf8), color-stop(100%,#4096ee)); /* Chrome,Safari4+ */
        background: -webkit-linear-gradient(top, #7abcff 0%,#60abf8 44%,#4096ee 100%); /* Chrome10+,Safari5.1+ */
        background: -o-linear-gradient(top, #7abcff 0%,#60abf8 44%,#4096ee 100%); /* Opera 11.10+ */
        background: -ms-linear-gradient(top, #7abcff 0%,#60abf8 44%,#4096ee 100%); /* IE10+ */
        background: linear-gradient(to bottom, #7abcff 0%,#60abf8 44%,#4096ee 100%); /* W3C */      }

      .header a {
        color: #fff;
        text-decoration: none;
      }

      .header a:hover {
        text-decoration: underline;
      }

      .header span {
        color: #fff;
        font-weight: bold;
        position: absolute;
        top: 10px;
        right: 10px;
      }

      .profile-main {
        width: 300px;
        height: 100%;
        float: left;
      }

      #logo {
        left: 10px;
      }

      #content {
        padding-left: 20px;
        padding-top: 10px;
        font-size: 12px;
      }

      #content a {
        font-size: 10px;
      }

      #content table * {
        font-size: 12px;
      }

      table {
        font-size: 11px;
        border-width: 1px;
        border-color: #ddd;
        border-collapse: collapse;
      }

      table td {
	      border-width: 1px;
	      padding: 8px;
        border-style: solid;
        border-color: #ddd;
        background-color: #ffffff;
        min-width: 10px;
        vertical-align: top;
      }

      .table-checkbox {
        margin-top: 8px;
      }

      #dataTable, #languageTable {
        border: 0px;
      }

      #dataTable td, #languageTable td {
        border: 0px;
      }

      input {
        padding: 5px;
        margin: 0px;
      }

      .btn {
        margin-top: 10px;
        -moz-box-shadow:inset 0px -3px 7px 0px #29bbff;
      	-webkit-box-shadow:inset 0px -3px 7px 0px #29bbff;
      	box-shadow:inset 0px -3px 7px 0px #29bbff;
        background: #7abcff; /* Old browsers */
        background: -moz-linear-gradient(top, #7abcff 0%, #60abf8 44%, #4096ee 100%); /* FF3.6+ */
        background: -webkit-gradient(linear, left top, left bottom, color-stop(0%,#7abcff), color-stop(44%,#60abf8), color-stop(100%,#4096ee)); /* Chrome,Safari4+ */
        background: -webkit-linear-gradient(top, #7abcff 0%,#60abf8 44%,#4096ee 100%); /* Chrome10+,Safari5.1+ */
        background: -o-linear-gradient(top, #7abcff 0%,#60abf8 44%,#4096ee 100%); /* Opera 11.10+ */
        background: -ms-linear-gradient(top, #7abcff 0%,#60abf8 44%,#4096ee 100%); /* IE10+ */
        background: linear-gradient(to bottom, #7abcff 0%,#60abf8 44%,#4096ee 100%); /* W3C */
      	filter:progid:DXImageTransform.Microsoft.gradient(startColorstr='#2dabf9', endColorstr='#0688fa',GradientType=0);
      	background-color:#2dabf9;
      	-moz-border-radius:3px;
      	-webkit-border-radius:3px;
      	border-radius:3px;
      	border:1px solid #fff;
      	display:inline-block;
      	cursor:pointer;
      	color:#ffffff;
      	font-family:Arial;
      	font-size:15px;
      	padding:9px 23px;
      	text-decoration:none;
      	text-shadow:0px 1px 0px #263666;
      }

      .btn:hover {
        background:-webkit-gradient(linear, left top, left bottom, color-stop(0.05, #0688fa), color-stop(1, #2dabf9));
      	background:-moz-linear-gradient(top, #0688fa 5%, #2dabf9 100%);
      	background:-webkit-linear-gradient(top, #0688fa 5%, #2dabf9 100%);
      	background:-o-linear-gradient(top, #0688fa 5%, #2dabf9 100%);
      	background:-ms-linear-gradient(top, #0688fa 5%, #2dabf9 100%);
      	background:linear-gradient(to bottom, #0688fa 5%, #2dabf9 100%);
      	filter:progid:DXImageTransform.Microsoft.gradient(startColorstr='#0688fa', endColorstr='#2dabf9',GradientType=0);
      	background-color:#0688fa;
      }

      .btn:active {
      	position:relative;
      	top:1px;
      }

      h1, h2, h3 {
        color: #7abcff;
      }

      h1 {
        font-size: 18px;
      }

      h2 {
        font-size: 16px;
      }

      hr {
        border: 0;
        height: 0;
        border-top: 1px solid rgba(0, 0, 0, 0.1);
        border-bottom: 1px solid rgba(255, 255, 255, 0.3);
      }

    </style>

    <!-- hosted by Google API -->
    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1/jquery.min.js"></script>

    <!-- hosted by Microsoft Ajax CDN -->
     <!-- <script src="http://ajax.aspnetcdn.com/ajax/jquery.validate/1.9/jquery.validate.min.js"></script> -->

    <script src="../jquery.validate.js"></script>
    <script src="../education-validation.js"></script>



  </head>
  <body>

    {if $smarty_loggedin == 1}
      <div class="header"> <span id="logo"> <a href="/index">CareerWay</a> <small style="font-size: 10px;"> beta </small> </span> <span> <i class="fa fa-user"></i> <a href="/profile"> {$smarty_login_user} </a> &nbsp;  <i class="fa fa-rocket"></i> <a href="/login/logout"> Odhlásit se </a> </span> </div>
    {else}
      <div class="header"> <span id="logo"> <a href="/index">CareerWay</a> <small style="font-size: 10px;"> beta </small> </span> <span> <i class="fa fa-user"></i> Nepřihlášený uživatel &nbsp; <i class="fa fa-arrow-down"></i>
 <a href="/login"> Přihlásit se</a> &nbsp; <i class="fa fa-caret-square-o-down"></i>
 <a href="/register"> Registrovat </a> </span> </div>
    {/if}

    <div id="content">
