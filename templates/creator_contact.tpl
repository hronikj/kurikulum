{include file='default_header.tpl'}
  {include file='default_messages.tpl'}

  <h1> Kontaktní údaje </h1>

  <form name="creator_form_contact" id="creator_form_contact" action="" method="POST">
    <table>
      <tr>
        <td> <label for="firstname"> Jméno </label> </td>
        <td> <input type="text" name="firstname" value="{if isset($smarty_firstname)}{$smarty_firstname}{/if}" /> </td>
      </tr>

      <tr>
        <td> <label for="lastname"> Příjmení </label> </td>
        <td> <input type="text" name="lastname" value="{if isset($smarty_lastname)}{$smarty_lastname}{/if}" /> </td>
      </tr>

      <tr>
        <td> <label for="permanent_address"> Adresa trvalého bydliště </label> </td>
        <td> <input type="text" name="permanent_address" value="{if isset($smarty_permanent_address)}{$smarty_permanent_address}{/if}" /> </td>
      </tr>

      <tr>
        <td> <label for="permanent_zipcode"> PSČ trvalého bydliště </label> </td>
        <td> <input type="text" name="permanent_zipcode" value="{if isset($smarty_permanent_zipcode)}{$smarty_permanent_zipcode}{/if}" /> </td>
      </tr>

      <tr>
        <td> <label for="permanent_city"> Město trvalého bydliště </label> </td>
        <td> <input type="text" name="permanent_city" value="{if isset($smarty_permanent_city)}{$smarty_permanent_city}{/if}" /> </td>
      </tr>

      <tr>
        <td> <label for="contact_address"> Adresa přechodného bydliště </label> </td>
        <td> <input type="text" name="contact_address" value="{if isset($smarty_contact_address)}{$smarty_contact_address}{/if}" /> </td>
      </tr>

      <tr>
        <td> <label for="contact_zipcode"> PSČ přechodného bydliště </label> </td>
        <td> <input type="text" name="contact_zipcode" value="{if isset($smarty_contact_zipcode)}{$smarty_contact_zipcode}{/if}" /> </td>
      </tr>

      <tr>
        <td> <label for="_city"> Město přechodného bydliště </label> </td>
        <td> <input type="text" name="contact_city" value="{if isset($smarty_contact_city)}{$smarty_contact_city}{/if}" /> </td>
      </tr>

      <tr>
        <td> <label for="telephone"> Telefon </label> </td>
        <td> <input type="text" name="telephone" value="{if isset($smarty_telephone)}{$smarty_telephone}{/if}" /> </td>
      </tr>

      <tr>
        <td> <label for="nationality"> Národnost </label> </td>
        <td> <input type="text" name="nationality" value="{if isset($smarty_nationality)}{$smarty_nationality}{/if}"  /> </td>
      </tr>

      <tr>
        <td> <label for="birthdate"> Datum narození </label> </td>
        <td>
          <select name="birthday">
            {for $day = 1 to 31}
              <option value="{$day}" {if (isset($smarty_birthday) && $smarty_birthday == $day)}selected{/if}> {$day}. </option>
            {/for}
          </select>
          <select name="birthmonth">
            {for $month = 1 to 12}
              <option value="{$month}" {if (isset($smarty_birthmonth) && $smarty_birthmonth == $month)}selected{/if}> {$month}. </option>
            {/for}
          </select>
          <select name="birthyear">
            {for $year = 1950 to 2015}
              <option value="{$year}" {if (isset($smarty_birthyear) && $smarty_birthyear == $year)}selected{/if}> {$year} </option>
            {/for}
          </select>
        </td>
      </tr>
  </table>

    <input type="submit" value="< Zpět" name="creator_form_back" class="btn" />
    <input type="submit" value="Pokračovat >" name="creator_form_submit" class="btn" />
  </form>



{include file='default_footer.tpl'}
