{include file='default_header.tpl'}
  {include file='default_messages.tpl'}
  {if $display_content}
    {if $smarty_configurations_display}
      {foreach from=$smarty_configurations_data key=myId item=i}
        Název: {$i.name} <br />
        Grafika: {$i.graphics} <br />
        <a href="/creator/edit/{$i.id}/"> upravit</a> &nbsp
        <a href="configurations/delete/{$i.id}/" onclick="return confirm('Delete?')"> smazat</a>  &nbsp
        <a href="configurations/generate/{$i.id}/"> vygenerovat PDF </a>
        <hr />
      {/foreach}

    {else}
      <p> Nemáte zatím vytvořen žádný životopis! <a href="creator"> Vytvořit nyní! </a> </p>
    {/if}

  {else}
    <p> Tato část webu je dostupná pouze přihlášeným uživatelům. <a href="/login"> Přihlásit se </a> </p>
  {/if}

{include file='default_footer.tpl'}
