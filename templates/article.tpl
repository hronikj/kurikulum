<html>
  <head>
    <title> {$smarty_header_title} </title>
    <style>
      body {
        font-family: Helvetica, sans-serif;
        size: 12px;
      }
      .error {
        color: red;
      }
      .ok {
        color: green;
      }
    </style>
  </head>
  <body>
    <h1> {$smarty_article_headline} </h1>
    <i> {$smarty_article_category} </i>
    <small> {$smarty_article_perex} </small>
    <p> {$smarty_article_text} </p>
  </body>
</html>
