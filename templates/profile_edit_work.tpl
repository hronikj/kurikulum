{include file='default_header.tpl'}
  {include file='default_messages.tpl'}
  <form name="profile_edit_work_form" method="POST" action="">
    <table>
      <tr>
        <td> Rok zahájení </td> <td> <input type="text" name="profile_edit_work_startYear" value="{$smarty_startYear}" /> </td>
      </tr>

      <tr>
        <td> Rok ukončení: </td> <td> <input type="text" name="profile_edit_work_endYear" value="{$smarty_endYear}" /> </td>
      </tr>

      <tr>
        <td> Zaměstnavatel </td> <td> <input type="text" name="profile_edit_work_employer" value="{$smarty_employer}" /> </td>
      </tr>

      <tr>
        <td> Pozice </td> <td> <input type="text" name="profile_edit_work_position" value="{$smarty_position}" /> </td>
      </tr>

      <tr>
        <td> Popis </td> <td> <textarea type="text" name="profile_edit_work_description">{$smarty_description}</textarea> </td>
      </tr>

    </table>
    <input type="submit" value="Odeslat" name="profile_edit_work_submit" class="btn" />
    <a href="/profile"> <span type="submit" value="Zpět na profil" class="btn"> Zpět na profil </span> </a>
  </form>

{include file='default_footer.tpl'}
