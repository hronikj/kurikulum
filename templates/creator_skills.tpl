{include file='default_header.tpl'}
  {include file='default_messages.tpl'}

  <script type="text/javascript">
    function addRow(tableID) {
      var table = document.getElementById(tableID);
      var rowCount = table.rows.length;
      if(rowCount < 5){  // limit the user from creating fields more than your limits
        var row = table.insertRow(rowCount);
        var colCount = table.rows[0].cells.length;
        for (var i = 0; i < colCount; i++) {
          var newcell = row.insertCell(i);
          newcell.innerHTML = table.rows[0].cells[i].innerHTML;
        }
      } else {
        alert("Maximum Passenger per ticket is 5");
      }
    }

    function deleteRow(tableID) {
      var table = document.getElementById(tableID);
      var rowCount = table.rows.length;
      for(var i=0; i<rowCount; i++) {
        var row = table.rows[i];
        var chkbox = row.cells[0].childNodes[0];
        if(null != chkbox && true == chkbox.checked) {
          if(rowCount <= 1) { // limit the user from removing all the fields
            alert("Cannot Remove all the Passenger.");
            break;
          }
          table.deleteRow(i);
          rowCount--;
          i--;
        }
      }
    }
  </script>

    <h1> Průvodce vytvořením CV - dovednosti </h1>

    {include file='creator_controls.tpl'}

    <form name="creator_form_skills" action="" method="POST">

      <table id="dataTable" class="form" border="1">
        <tbody>
          {for $row_count=0 to $smarty_row_count - 1}
          <tr>
            <p>
              <td style="min-width: 15px;"><input type="checkbox" required="required" name="chk[]" checked="checked" /></td>
              <td>
                <label for="creator_form_skills_name"> Název: </label>
                <input required="required" type="text" name="creator_form_skills_name[]" value="{if isset($smarty_creator_form_skills_name[$row_count])}{$smarty_creator_form_skills_name[$row_count]}{/if}" />
                {if isset($smarty_validation_name[$row_count])}
                  <ul>
                    {$smarty_validation_name[$row_count]}
                  <ul>
                {/if}
              </td>
              <td> <label for="creator_form_skills_level"> Stupeň: </label>
                <select name="creator_form_skills_level[]">
                  <option value="Základní znalost" {if (isset($smarty_creator_form_skills_level[$row_count]) && $smarty_creator_form_skills_level[$row_count] == 'Základní znalost')}selected{/if}> Základní znalost </option>
                  <option value="Mírně pokročilá znalost" {if (isset($smarty_creator_form_skills_level[$row_count]) && $smarty_creator_form_skills_level[$row_count] == 'Mírně pokročilá znalost')}selected{/if}> Mírně pokročilá znalost </option>
                  <option value="Pokročilá znalost" {if (isset($smarty_creator_form_skills_level[$row_count]) && $smarty_creator_form_skills_level[$row_count] == 'Pokročilá znalost')}selected{/if}> Pokročilá znalost </option>
                  <option value="Expertní" {if (isset($smarty_creator_form_skills_level[$row_count]) && $smarty_creator_form_skills_level[$row_count] == 'Expertní')}selected{/if}> Expertní </option>
                </select>
              </td>

            </p>
          </tr>
          {/for}
        </tbody>
      </table>

      <input type="submit" value="< Zpět" name="creator_form_skills_back" class="btn" />
      <input type="submit" value="Pokračovat >" name="creator_form_skills_submit" class="btn" />
    </form>

{include file='default_footer.tpl'}
