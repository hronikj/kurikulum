{include file='default_header.tpl'}
  {include file='default_messages.tpl'}
  <form name="profile_add_language_form" method="POST" action="">
    <table>
      <tr>
        <td> Jazyk </td> <td> <input type="text" name="profile_add_language_name" value="{if isset($smarty_profile_add_language_name)}{$smarty_profile_add_language_name}{/if}" /> </td>
      </tr>

      <tr>
        <td> Úroveň: </td>
        <td>
          <select name="profile_add_language_level">
            <option value="A1"> A1 </option>
            <option value="A2"> A2 </option>
            <option value="B1"> B1 </option>
            <option value="B2"> B2 </option>
            <option value="C1"> C1 </option>
            <option value="C2"> C2 </option>
            <option value="Rodilý mluvčí"> Rodilý mluvčí </option>
          </select>
        </td>
      </tr>

    </table>
    <input type="submit" value="Odeslat" name="profile_add_language_submit" class="btn" />

  </form>

{include file='default_footer.tpl'}
