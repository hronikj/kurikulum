{include file='default_header.tpl'}
  {include file='default_messages.tpl'}

  {if $display_content}

  <form name="profile_edit_form" action="" method="POST">
    <table>
      <tr>
        <td> Jméno </td> <td> <input type="text" name="profile_edit_form_firstname" value="{if isset($smarty_profile_edit_form_firstname)}{$smarty_profile_edit_form_firstname}{/if}" /> </td>
      </tr>

      <tr>
        <td> Příjmení </td> <td> <input type="text" name="profile_edit_form_lastname" value="{if isset($smarty_profile_edit_form_lastname)}{$smarty_profile_edit_form_lastname}{/if}" /> </td>
      </tr>

      <tr>
        <td> Adresa </td> <td> <input type="text" name="profile_edit_form_address" value="{if isset($smarty_profile_edit_form_address)}{$smarty_profile_edit_form_address}{/if}" /> </td>
      </tr>

      <tr>
        <td> Adresa 2 </td> <td> <input type="text" name="profile_edit_form_address2" value="{if isset($smarty_profile_edit_form_address2)}{$smarty_profile_edit_form_address2}{/if}" />  </td>
      </tr>

      <tr>
        <td> Město </td> <td> <input type="text" name="profile_edit_form_city" value="{if isset($smarty_profile_edit_form_city)}{$smarty_profile_edit_form_city}{/if}" />  </td>
      </tr>

      <tr>
        <td> PSČ </td> <td> <input type="text" name="profile_edit_form_zipcode" value="{if isset($smarty_profile_edit_form_zipcode)}{$smarty_profile_edit_form_zipcode}{/if}" /> </td>
      </tr>

      <tr>
        <td> Telefon </td> <td> <input type="text" name="profile_edit_form_telephone" value="{if isset($smarty_profile_edit_form_telephone)}{$smarty_profile_edit_form_telephone}{/if}" /> </td>
      </tr>

      <tr>
        <td> Email </td> <td> <input type="text" name="profile_edit_form_email" value="{if isset($smarty_profile_edit_form_email)}{$smarty_profile_edit_form_email}{/if}" /> </td>
      </tr>

    </table>
    <input type="submit" name="profile_edit_form_submit" class="btn" value="Odeslat" />
  </form>

  {else}
    <p> Tato část webu je dostupná pouze přihlášeným uživatelům. <a href="/login"> Přihlásit se </a> </p>
  {/if}

{include file='default_footer.tpl'}
