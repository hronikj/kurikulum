{include file='default_header.tpl'}
  {include file='default_messages.tpl'}
    <style>
        .fieldgroup label.error {
        color: #FB3A3A;
        display: inline-block;
        margin: 4px 0 5px 125px;
        padding: 0;
        text-align: left;
        width: 220px;
      }

      .error {
        color: red;
      }

    </style>

    <script type="text/javascript">
      function addRow(tableID) {
        var table = document.getElementById(tableID);
        var rowCount = table.rows.length;
        if(rowCount < 5){  // limit the user from creating fields more than your limits
          var row = table.insertRow(rowCount);
          var colCount = table.rows[0].cells.length;
          for (var i = 0; i < colCount; i++) {
            var newcell = row.insertCell(i);
            newcell.innerHTML = table.rows[0].cells[i].innerHTML;
          }
        } else {
          alert("Maximum Passenger per ticket is 5");
        }
      }

      function deleteRow(tableID) {
        var table = document.getElementById(tableID);
        var rowCount = table.rows.length;
        for(var i=0; i<rowCount; i++) {
          var row = table.rows[i];
          var chkbox = row.cells[0].childNodes[0];
          if(null != chkbox && true == chkbox.checked) {
            if(rowCount <= 1) { // limit the user from removing all the fields
              alert("Cannot Remove all the Passenger.");
              break;
            }
            table.deleteRow(i);
            rowCount--;
            i--;
          }
        }
      }
    </script>

    <h1> Průvodce vytvořením CV - PC dovednosti </h1>

    <form name="creator_form_pcskills" id="creator_form_pcskills" action="" method="POST">
        <table id="dataTable" class="form" border="1">
          <tbody>
            {$row_count = 0}
            <tr> <td> <input type="text" name="id[]" value="{$row_count}" hidden="true" /> </td> </tr>
            <tr>
              <p>
                <td> <label for="word[]"> MS Word: </label>
                  <select name="word[]">
                    <option value="Základní znalost" {if (isset($smarty_word[$row_count]) && $smarty_word[$row_count] == 'Základní znalost')}selected{/if}> Základní znalost </option>
                    <option value="Mírně pokročilá znalost" {if (isset($smarty_word[$row_count]) && $smarty_word[$row_count] == 'Mírně pokročilá znalost')}selected{/if}> Mírně pokročilá znalost </option>
                    <option value="Pokročilá znalost" {if (isset($smarty_word[$row_count]) && $smarty_word[$row_count] == 'Pokročilá znalost')}selected{/if}> Pokročilá znalost </option>
                    <option value="Expertní" {if (isset($smarty_word[$row_count]) && $smarty_word[$row_count] == 'Expertní')}selected{/if}> Expertní </option>
                  </select>
                </td>
              </p>
            </tr>
            <tr>
              <p>
                <td> <label for="excel[]"> MS Excel: </label>
                  <select name="excel[]">
                    <option value="Základní znalost" {if (isset($smarty_excel[$row_count]) && $smarty_excel[$row_count] == 'Základní znalost')}selected{/if}> Základní znalost </option>
                    <option value="Mírně pokročilá znalost" {if (isset($smarty_excel[$row_count]) && $smarty_excel[$row_count] == 'Mírně pokročilá znalost')}selected{/if}> Mírně pokročilá znalost </option>
                    <option value="Pokročilá znalost" {if (isset($smarty_excel[$row_count]) && $smarty_excel[$row_count] == 'Pokročilá znalost')}selected{/if}> Pokročilá znalost </option>
                    <option value="Expertní" {if (isset($smarty_excel[$row_count]) && $smarty_excel[$row_count] == 'Expertní')}selected{/if}> Expertní </option>
                  </select>
                </td>
              </p>
            </tr>
            <tr>
              <p>
                <td> <label for="powerpoint[]"> MS Powerpoint: </label>
                  <select name="powerpoint[]">
                    <option value="Základní znalost" {if (isset($smarty_powerpoint[$row_count]) && $smarty_powerpoint[$row_count] == 'Základní znalost')}selected{/if}> Základní znalost </option>
                    <option value="Mírně pokročilá znalost" {if (isset($smarty_powerpoint[$row_count]) && $smarty_powerpoint[$row_count] == 'Mírně pokročilá znalost')}selected{/if}> Mírně pokročilá znalost </option>
                    <option value="Pokročilá znalost" {if (isset($smarty_powerpoint[$row_count]) && $smarty_powerpoint[$row_count] == 'Pokročilá znalost')}selected{/if}> Pokročilá znalost </option>
                    <option value="Expertní" {if (isset($smarty_powerpoint[$row_count]) && $smarty_powerpoint[$row_count] == 'Expertní')}selected{/if}> Expertní </option>
                  </select>
                </td>
              </p>
            </tr>
            <tr>
              <p>
                <td> <label for="outlook[]"> MS Outlook: </label>
                  <select name="outlook[]">
                    <option value="Základní znalost" {if (isset($smarty_outlook[$row_count]) && $smarty_outlook[$row_count] == 'Základní znalost')}selected{/if}> Základní znalost </option>
                    <option value="Mírně pokročilá znalost" {if (isset($smarty_outlook[$row_count]) && $smarty_outlook[$row_count] == 'Mírně pokročilá znalost')}selected{/if}> Mírně pokročilá znalost </option>
                    <option value="Pokročilá znalost" {if (isset($smarty_outlook[$row_count]) && $smarty_outlook[$row_count] == 'Pokročilá znalost')}selected{/if}> Pokročilá znalost </option>
                    <option value="Expertní" {if (isset($smarty_outlook[$row_count]) && $smarty_outlook[$row_count] == 'Expertní')}selected{/if}> Expertní </option>
                  </select>
                </td>
              </p>
            </tr>
            <tr>
              <p>
                <td> <label for="keyboard[]"> Psaní na klávesnici: </label>
                  <select name="keyboard[]">
                    <option value="Základní znalost" {if (isset($smarty_keyboard[$row_count]) && $smarty_keyboard[$row_count] == 'Základní znalost')}selected{/if}> Základní znalost </option>
                    <option value="Mírně pokročilá znalost" {if (isset($smarty_keyboard[$row_count]) && $smarty_keyboard[$row_count] == 'Mírně pokročilá znalost')}selected{/if}> Mírně pokročilá znalost </option>
                    <option value="Pokročilá znalost" {if (isset($smarty_keyboard[$row_count]) && $smarty_keyboard[$row_count] == 'Pokročilá znalost')}selected{/if}> Pokročilá znalost </option>
                    <option value="Expertní" {if (isset($smarty_keyboard[$row_count]) && $smarty_keyboard[$row_count] == 'Expertní')}selected{/if}> Expertní </option>
                  </select>
                </td>
              </p>
            </tr>

          </tbody>
        </table>

        <input type="submit" value="< Zpět" name="creator_form_back" class="btn" />
        <input type="submit" value="Pokračovat >" name="creator_form_submit" class="btn" />
      </form>

{include file='default_footer.tpl'}
