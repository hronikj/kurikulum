{include file='default_header.tpl'}
  {include file='default_messages.tpl'}
  <form name="profile_add_skills_form" method="POST" action="">
    <table>
      <tr>
        <td> Název </td> <td> <input type="text" name="profile_add_skills_name" value="{if isset($smarty_profile_add_skills_name)}{$smarty_profile_add_skills_name}{/if}" /> </td>
      </tr>

      <tr>
        <td> Úroveň: </td>
        <td>
          <select name="profile_add_skills_level">
            <option value="Základní"> Základní znalost </option>
            <option value="Mírně pokročilá"> Mírně pokročilá znalost </option>
            <option value="Pokročilá"> Pokročilá znalost </option>
            <option value="Expertní"> Expertní </option>
          </select>
        </td>
      </tr>

    </table>
    <input type="submit" value="Odeslat" name="profile_add_skills_submit" class="btn" />

  </form>

{include file='default_footer.tpl'}
