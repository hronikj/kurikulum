{include file='default_header.tpl'}
  {include file='default_messages.tpl'}
  {if $display_content}
  <script type="text/javascript">
    function confirmDelete(type, id) {
      if (confirm("Smazat?")) location.href='/profile/delete/' + type + '/' + id;
    }
  </script>

    <div class="profile-main">
      <h1> Profil </h1>
      <p> <a href="/profile/edit"> Upravit údaje </a> </p>
      <table>
        <tr>
          <td> Jméno a příjmení </td> <td> {$smarty_profile_firstname} {$smarty_profile_lastname} </td>
        </tr>
        <tr>
          <td> Adresa </td> <td> {$smarty_profile_address} {$smarty_profile_address2} </td>
        </tr>
        <tr>
          <td> PSČ </td> <td> {$smarty_profile_zipcode} </td>
        </tr>
        <tr>
          <td> Město </td> <td> {$smarty_profile_city} </td>
        </tr>
        <tr>
          <td> Telefon </td> <td> {$smarty_profile_telephone} </td>
        </tr>
        <tr>
          <td> Email </td> <td> {$smarty_profile_email} </td>
        </tr>
        <tr>
          <td> Registrován(a) od </td> <td> {$smarty_profile_registeredsince} </td>
        </tr>
        <tr>
          <td> Zaplacené členství: </td> <td> {$smarty_profile_payment_status} </td>
        </tr>
      </table>
    </div>

    <h2> Vzdělání </h2>
    <p> <a href="profile/add/education">Přidat údaje o vzdělání</a> </p>
    {if $smarty_education_display}
      {foreach from=$smarty_education_data key=myId item=i}
        Instituce: {$i.school} ({$i.degree}) <br />
        {$i.startYear} - {$i.endYear} <br />
        Titul: {$i.title}
        <a href="profile/edit/education/{$i.id}"> upravit</a> &nbsp <a href="profile/delete/education/{$i.id}/" onclick="return confirm('Delete?')"> smazat </a> <hr />
      {/foreach}
    {/if}

    <h2> Předchozí praxe </h2>
    <p> <a href="profile/add/work"> Přidat údaje o předchozí praxi </a> </p>
    {if $smarty_work_display}
      {foreach from=$smarty_work_data key=myId item=i}
        Zaměstnavatel: {$i.employer} <br />
        Pozice: {$i.position}
        {$i.startYear} - {$i.endYear} <br />
        Popis: {$i.description}
        <a href="profile/edit/work/{$i.id}"> upravit</a> &nbsp <a href="profile/delete/work/{$i.id}/" onclick="return confirm('Delete?')"> smazat </a> <hr />
      {/foreach}
    {/if}

    <h2> Certifikace </h2>
    <p> <a href="profile/add/certification"> Přidat údaje o certifikaci </a> </p>
    {if $smarty_certification_display}
      {foreach from=$smarty_certification_data key=myId item=i}
        Název: {$i.name} <br />
        Rok: {$i.year} <br />
        Instituce: {$i.school}
        <a href="profile/edit/certification/{$i.id}"> upravit</a> &nbsp <a href="profile/delete/certification/{$i.id}/" onclick="return confirm('Delete?')"> smazat </a> <hr />
      {/foreach}
    {/if}

    <h2> Dovednosti </h2>
    <p> <a href="profile/add/skills"> Přidat údaje o dovednostech </a> </p>
    {if $smarty_skills_display}
      {foreach from=$smarty_skills_data key=myId item=i}
        Dovednost: {$i.name} <br />
        Úroveň: {$i.level} <br />
        <a href="profile/edit/skills/{$i.id}"> upravit</a> &nbsp <a href="profile/delete/skills/{$i.id}/" onclick="return confirm('Delete?')"> smazat </a> <hr />
      {/foreach}
    {/if}

    <h2> Jazyky </h2>
    <p> <a href="profile/add/language"> Přidat údaje o jazyce </a> </p>
    {if $smarty_language_display}
      {foreach from=$smarty_language_data key=myId item=i}
        Jazyk: {$i.name} <br />
        Úroveň: {$i.level} <br />
        <a href="profile/edit/language/{$i.id}"> upravit</a> &nbsp <a href="profile/delete/language/{$i.id}/" onclick="return confirm('Delete?')"> smazat </a> <hr />
      {/foreach}
    {/if}

  {else}
    <p> Tato část webu je dostupná pouze přihlášeným uživatelům. <a href="/login"> Přihlásit se </a> </p>
  {/if}

{include file='default_footer.tpl'}
