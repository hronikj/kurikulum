{include file='default_header.tpl'}
  {include file='default_messages.tpl'}
    <h1> Průvodce vytvořením CV - sumarizace </h1>

    <form name="creator_form_sumarization" id="creator_form_sumarization" action="" method="POST">
        <input type="text" name="id[]" value="{$row_count}" hidden="true" />
        <table id="dataTable" class="form" border="1">
          <tbody>
            <tr> <td> <strong> Vzdělání: </strong> </td> </tr>
            {foreach from=$smarty_education_array key=k item=i}
              <tr> <td> Instituce: </td> <td> {$i.school} </td> </tr>
              <tr> <td> Stupeň vzdělání: </td> <td> {$i.degree} </td> </tr>
              <tr> <td> Od: </td> <td> {$i.start_month}/{$i.start_year} </td> </tr>
              <tr> <td> Do: </td> <td> {$i.end_month}/{$i.end_year} </td> </tr>
              <tr> <td> Ukončení: </td> <td> {$i.finished} </td> </tr>
              <tr> <td> Titul: </td> <td> {$i.title} </td> </tr>
              <tr> <td> </td> </tr>
            {/foreach}
            <tr> <td> <strong> Práce: </strong> </td> </tr>
            {foreach from=$smarty_work_array key=k item=i}
              <tr> <td> Zaměstnavatel: </td> <td> {$i.employer} </td> </tr>
              <tr> <td> Pozice: </td> <td> {$i.position} </td> </tr>
              <tr> <td> Od: </td> <td> {$i.start_month}/{$i.start_year} </td> </tr>
              <tr> <td> Do: </td> <td> {$i.end_month}/{$i.end_year} </td> </tr>
              <tr> <td> Reference: </td> <td> {$i.reference_name} ({$i.reference_contact}) </td> </tr>
              <tr> <td> Popis: </td> <td> {$i.description} </td> </tr>
              <tr> <td> Kategorie: </td> <td> {$i.field} </td> </tr>
              <tr> <td> </td> </tr>
            {/foreach}
            <tr> <td> <strong> Jazyky: </strong> </td> </tr>
            {foreach from=$smarty_language_array key=k item=i}
              <tr> <td> {$i.name} </td> <td> {$i.level} </td> </tr>
            {/foreach}
            <tr> <td> <strong> Dovednosti: </strong> </td> </tr>
            {foreach from=$smarty_skills_array key=k item=i}
              <tr> <td> {$i.name} </td> <td> {$i.level} </td> </tr>
              <tr> <td> </td> </tr>
            {/foreach}
            <tr> <td> <strong> Certifikace: </strong> </td> </tr>
            {foreach from=$smarty_certification_array key=k item=i}
              <tr> <td> Instituce: </td> <td> {$i.organization} </td> </tr>
              <tr> <td> Název: </td> <td> {$i.name} </td> </tr>
              <tr> <td> Rok: </td> <td> {$i.year} </td> </tr>
              <tr> <td> </td> </tr>
            {/foreach}
            <tr> <td> <strong> Řidičské oprávnění: </strong> </td> </tr>
            <tr> <td> Skupina: </td> <td> {$smarty_driving} </td> </tr>
            <tr> <td> <strong> Volnočasové aktivity: </strong> </td> </tr>
            <tr> <td> {$smarty_hobbies} </td> </tr>
            <tr> <td> <strong> Hlavička: </strong> </td> </tr>
            <tr> <td> Hlavička: </td> <td> {$smarty_header_header} </td> </tr>
            <tr> <td> Motto: </td> <td> {$smarty_header_motto} </td> </tr>
          </tbody>
        </table>

        <input type="submit" value="< Zpět" name="creator_form_back" class="btn" />
        <input type="submit" value="Pokračovat >" name="creator_form_submit" class="btn" />
      </form>

{include file='default_footer.tpl'}
