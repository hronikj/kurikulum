{include file='default_header.tpl'}
  {include file='default_messages.tpl'}

  <form name="profile_edit_education_form" method="POST" action="">
    <table>
      <tr>
        <td> Rok zahájení: </td> <td> <input type="text" name="profile_edit_education_startYear" value="{$smarty_startYear}" /> </td>
      </tr>

      <tr>
        <td> Rok ukončení: </td> <td> <input type="text" name="profile_edit_education_endYear" value="{$smarty_endYear}" /> </td>
      </tr>

      <tr>
        <td> Instituce: </td> <td> <input type="text" name="profile_edit_education_school" value="{$smarty_school}" /> </td>
      </tr>

      <tr>
        <td> Udělený titul: </td> <td> <input type="text" name="profile_edit_education_title" value="{$smarty_title}" /> </td>
      </tr>

      <tr>
        <td> Typ vzdělání: </td>
        <td>
          <select name="profile_edit_education_degree">
            <option value="Základní"> Základní </option>
            <option value="Střední (výuční list)"> Střední (výuční list) </option>
            <option value="Střední (maturita)"> Střední (maturita) </option>
            <option value="Vyšší odborné"> Vyšší odborné </option>
            <option value="Vysoká škola"> Vysoká škola </option>
            <option value="Jiné"> Jiné </option>
          </select>
        </td>
      </tr>


    </table>
    <input type="submit" value="Odeslat" class="btn" name="profile_edit_education_submit" />
      <a href="/profile"> <span type="submit" value="Zpět na profil" class="btn"> Zpět na profil </span> </a>
  </form>

{include file='default_footer.tpl'}
