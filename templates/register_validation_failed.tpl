{include file='default_header.tpl'}

    <h1> Registrace neúspěšná! </h1>
    <p> Registrace nemohla být dokončena, důvody mohou být následující: </p>
    <ul>
      <li> Neplatný nebo chybějící registrační token </li>
      <li> Platnost registrace již vypršela </li>
      <li> Interní chyba systému (kontaktujte administrátora) </li>
    </ul>

{include file='default_footer.tpl'}
