{include file='default_header.tpl'}
  {include file='default_messages.tpl'}
    <style>
        .fieldgroup label.error {
        color: #FB3A3A;
        display: inline-block;
        margin: 4px 0 5px 125px;
        padding: 0;
        text-align: left;
        width: 220px;
      }

      .error {
        color: red;
      }

    </style>

    <script type="text/javascript">
      function addRow(tableID) {
        var table = document.getElementById(tableID);
        var rowCount = table.rows.length;
        if(rowCount < 5){  // limit the user from creating fields more than your limits
          var row = table.insertRow(rowCount);
          var colCount = table.rows[0].cells.length;
          for (var i = 0; i < colCount; i++) {
            var newcell = row.insertCell(i);
            newcell.innerHTML = table.rows[0].cells[i].innerHTML;
          }
        } else {
          alert("Maximum Passenger per ticket is 5");
        }
      }

      function deleteRow(tableID) {
        var table = document.getElementById(tableID);
        var rowCount = table.rows.length;
        for(var i=0; i<rowCount; i++) {
          var row = table.rows[i];
          var chkbox = row.cells[0].childNodes[0];
          if(null != chkbox && true == chkbox.checked) {
            if(rowCount <= 1) { // limit the user from removing all the fields
              alert("Cannot Remove all the Passenger.");
              break;
            }
            table.deleteRow(i);
            rowCount--;
            i--;
          }
        }
      }
    </script>

    <h1> Průvodce vytvořením CV - Řidičské oprávnění </h1>

    <form name="creator_form_pcskills" id="creator_form_pcskills" action="" method="POST">
        <table id="dataTable" class="form" border="1">
          <tbody>
            {$row_count = 0}
            <tr> <td> <input type="text" name="id[]" value="{$row_count}" hidden="true" /> </td> </tr>
            <tr>
              <p>
                <td> <label for="level[]"> Řidičské oprávnění: </label>
                  <select name="level[]">
                    <option value="Bez řidičského oprávnění" {if (isset($smarty_level[$row_count]) && $smarty_level[$row_count] == 'Bez řidičského oprávnění')}selected{/if}> Bez řidičského oprávnění </option>
                    <option value="B1" {if (isset($smarty_level[$row_count]) && $smarty_level[$row_count] == 'B1')}selected{/if}> B1 </option>
                    <option value="B" {if (isset($smarty_level[$row_count]) && $smarty_level[$row_count] == 'B')}selected{/if}> B </option>
                    <option value="B+E" {if (isset($smarty_level[$row_count]) && $smarty_level[$row_count] == 'B+E')}selected{/if}> B+E </option>
                    <option value="C1" {if (isset($smarty_level[$row_count]) && $smarty_level[$row_count] == 'C1')}selected{/if}> C1 </option>
                    <option value="C1+E" {if (isset($smarty_level[$row_count]) && $smarty_level[$row_count] == 'C1+E')}selected{/if}> C1+E </option>
                    <option value="C" {if (isset($smarty_level[$row_count]) && $smarty_level[$row_count] == 'C')}selected{/if}> C </option>
                    <option value="C+E" {if (isset($smarty_level[$row_count]) && $smarty_level[$row_count] == 'C+E')}selected{/if}> C+E </option>
                    <option value="D1" {if (isset($smarty_level[$row_count]) && $smarty_level[$row_count] == 'D1')}selected{/if}> D1 </option>
                    <option value="D1+E" {if (isset($smarty_level[$row_count]) && $smarty_level[$row_count] == 'D1+E')}selected{/if}> D1+E </option>
                    <option value="D" {if (isset($smarty_level[$row_count]) && $smarty_level[$row_count] == 'D')}selected{/if}> D </option>
                    <option value="D+E" {if (isset($smarty_level[$row_count]) && $smarty_level[$row_count] == 'D+E')}selected{/if}> D+E </option>
                    <option value="T" {if (isset($smarty_level[$row_count]) && $smarty_level[$row_count] == 'T')}selected{/if}> T </option>
                  </select>
                </td>
              </p>
            </tr>

          </tbody>
        </table>

        <input type="submit" value="< Zpět" name="creator_form_back" class="btn" />
        <input type="submit" value="Pokračovat >" name="creator_form_submit" class="btn" />
      </form>

{include file='default_footer.tpl'}
