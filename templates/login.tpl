{include file='default_header.tpl'}
  {include file='default_messages.tpl'}

    {if $smarty_display_login_form}
      <form action="" method="POST" name="login_form">
        <label for="login_form_login"> Přihlašovací jméno </label>
        <input type="text" name="login_form_login" />

        <label for="login_form_password"> Heslo </label>
        <input type="password" name="login_form_password" />

        <input type="submit" name="login_form_submit" value="Přihlásit" />
      </form>

      {else}
      <p> Přihlášení proběhlo v pořádku! </p>
    {/if}

{include file='default_footer.tpl'}
