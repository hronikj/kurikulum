{if $smarty_display_messages == 1}
  <script>
    // close = document.getElementById("close");
    // close.addEventListener('click', function() {
    //  note = document.getElementById("note");
    //  note.style.display = 'none';
    // }, false);
  </script>

  {foreach from=$smarty_msg key=myId item=i}
    <div class="{$i.class}"> {if $i.type == 'OK' || $i.type == 'ok'}<i class="fa fa-check-circle"></i>{else}<i class="fa fa-exclamation-triangle"></i>{/if} &nbsp; {$i.message} <a id="close" href=""><i class="fa fa-times" style="font-size: 14px; padding-top: 7px; position: absolute; top: 5px; right: 20px;"></i></a> </div>
  {/foreach}
{/if}
