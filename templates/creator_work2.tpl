{include file='default_header.tpl'}
  {include file='default_messages.tpl'}

  <script type="text/javascript">
    function addRow(tableID) {
      var table = document.getElementById(tableID);
      var rowCount = table.rows.length;
      if(rowCount < 5){  // limit the user from creating fields more than your limits
        var row = table.insertRow(rowCount);
        var colCount = table.rows[0].cells.length;
        for (var i = 0; i < colCount; i++) {
          var newcell = row.insertCell(i);
          newcell.innerHTML = table.rows[0].cells[i].innerHTML;
        }
      } else {
        alert("Maximum položek pro tento formulář je: 5");
      }
    }

    function deleteRow(tableID) {
      var table = document.getElementById(tableID);
      var rowCount = table.rows.length;
      for(var i=0; i<rowCount; i++) {
        var row = table.rows[i];
        var chkbox = row.cells[0].childNodes[0];
        if(null != chkbox && true == chkbox.checked) {
          if(rowCount <= 1) { // limit the user from removing all the fields
            alert("Nelze odebrat všechny řádky!");
            break;
          }
          table.deleteRow(i);
          rowCount--;
          i--;
        }
      }
    }
  </script>

  <style>
    .fieldgroup label.error {
      color: #FB3A3A;
      display: inline-block;
      margin: 4px 0 5px 125px;
      padding: 0;
      text-align: left;
      width: 220px;
    }

    .error {
      color: red;
    }
  </style>


    <h1> Průvodce vytvořením CV - práce </h1>

    {include file='creator_controls.tpl'}

    <form name="creator_form_work" id="creator_form_work" action="" method="POST">
      <table id="dataTable" class="form" border="0">
        <tbody>
          {for $row_count=0 to $smarty_row_count - 1}
          <tr>
              <td><input type="checkbox" required="required" name="chk[]" checked="checked" class="table-checkbox" /><input type="text" name="id[]" value="{$row_count}" hidden="true" /></td>
              <td>
                <table>
                  <tr>
                    <td> <label for="employer[]"> Zaměstnavatel:  </label> </td>
                    <td>
                      <input type="text" name="employer[]" value="{if isset($smarty_employer[$row_count])}{$smarty_employer[$row_count]}{/if}" />
                      {if isset($smarty_validation_employer[$row_count])}
                        {$smarty_validation_employer[$row_count]}
                      {/if}
                    </td>
                  </tr>
                  <tr>
                    <td> <label for="position[]"> Pozice: </label> </td>
                    <td>
                      <input type="text" name="position[]" value="{if isset($smarty_position[$row_count])}{$smarty_position[$row_count]}{/if}" />
                      {if isset($smarty_validation_position[$row_count])}
                        {$smarty_validation_position[$row_count]}
                      {/if}
                    </td>
                  </tr>
                  <tr>
                    <td> <label for="start_month[]"> Zaměstnán(a) od (měsíc, rok): </label> </td>
                    <td>
                      <select name="start_month[]">
                        <option value="1" {if (isset($smarty_start_month[$row_count]) && $smarty_start_month[$row_count] == 1)}selected{/if}> leden </option>
                        <option value="2" {if (isset($smarty_start_month[$row_count]) && $smarty_start_month[$row_count] == 2)}selected{/if}> únor </option>
                        <option value="3" {if (isset($smarty_start_month[$row_count]) && $smarty_start_month[$row_count] == 3)}selected{/if}> březen </option>
                        <option value="4" {if (isset($smarty_start_month[$row_count]) && $smarty_start_month[$row_count] == 4)}selected{/if}> duben </option>
                        <option value="5" {if (isset($smarty_start_month[$row_count]) && $smarty_start_month[$row_count] == 5)}selected{/if}> květen </option>
                        <option value="6" {if (isset($smarty_start_month[$row_count]) && $smarty_start_month[$row_count] == 6)}selected{/if}> červen </option>
                        <option value="7" {if (isset($smarty_start_month[$row_count]) && $smarty_start_month[$row_count] == 7)}selected{/if}> červenec </option>
                        <option value="8" {if (isset($smarty_start_month[$row_count]) && $smarty_start_month[$row_count] == 8)}selected{/if}> srpen </option>
                        <option value="9" {if (isset($smarty_start_month[$row_count]) && $smarty_start_month[$row_count] == 9)}selected{/if}> září </option>
                        <option value="10" {if (isset($smarty_start_month[$row_count]) && $smarty_start_month[$row_count] == 10)}selected{/if}> říjen </option>
                        <option value="11" {if (isset($smarty_start_month[$row_count]) && $smarty_start_month[$row_count] == 11)}selected{/if}> listopad </option>
                        <option value="12" {if (isset($smarty_start_month[$row_count]) && $smarty_start_month[$row_count] == 12)}selected{/if}> prosinec </option>
                      </select>
                      <select name="start_year[]">
                        {for $year = 1950 to 2015}
                          <option value="{$year}" {if (isset($smarty_start_year[$row_count]) && $smarty_start_year[$row_count] == $year)}selected{/if}> {$year} </option>
                        {/for}
                      </select>
                    </td>
                  </tr>
                  <tr>
                    <td> Zaměstnán(a) do (měsíc, rok): </td>
                    <td>
                      <select name="end_month[]">
                        <option value="1" {if (isset($smarty_end_month[$row_count]) && $smarty_end_month[$row_count] == 1)}selected{/if}> leden </option>
                        <option value="2" {if (isset($smarty_end_month[$row_count]) && $smarty_end_month[$row_count] == 2)}selected{/if}> únor </option>
                        <option value="3" {if (isset($smarty_end_month[$row_count]) && $smarty_end_month[$row_count] == 3)}selected{/if}> březen </option>
                        <option value="4" {if (isset($smarty_end_month[$row_count]) && $smarty_end_month[$row_count] == 4)}selected{/if}> duben </option>
                        <option value="5" {if (isset($smarty_end_month[$row_count]) && $smarty_end_month[$row_count] == 5)}selected{/if}> květen </option>
                        <option value="6" {if (isset($smarty_end_month[$row_count]) && $smarty_end_month[$row_count] == 6)}selected{/if}> červen </option>
                        <option value="7" {if (isset($smarty_end_month[$row_count]) && $smarty_end_month[$row_count] == 7)}selected{/if}> červenec </option>
                        <option value="8" {if (isset($smarty_end_month[$row_count]) && $smarty_end_month[$row_count] == 8)}selected{/if}> srpen </option>
                        <option value="9" {if (isset($smarty_end_month[$row_count]) && $smarty_end_month[$row_count] == 9)}selected{/if}> září </option>
                        <option value="10" {if (isset($smarty_end_month[$row_count]) && $smarty_end_month[$row_count] == 10)}selected{/if}> říjen </option>
                        <option value="11" {if (isset($smarty_end_month[$row_count]) && $smarty_end_month[$row_count] == 11)}selected{/if}> listopad </option>
                        <option value="12" {if (isset($smarty_end_month[$row_count]) && $smarty_end_month[$row_count] == 12)}selected{/if}> prosinec </option>
                      </select>
                      <select name="end_year[]">
                        {for $year = 1950 to 2015}
                          <option value="{$year}" {if (isset($smarty_end_year[$row_count]) && $smarty_end_year[$row_count] == $year)}selected{/if}> {$year} </option>
                        {/for}
                      </select>
                     </td>
                  </tr>
                  <tr>
                    <td> Obor činnosti: </td>
                    <td>
                      <select name="field[]">
                        <option value="Administrativa" {if (isset($smarty_field[$row_count]) && $smarty_field[$row_count] == 'Administrativa')}selected{/if}> Administrativa </option>
                        <option value="Auto - moto" {if (isset($smarty_field[$row_count]) && $smarty_field[$row_count] == 'Auto - moto')}selected{/if}> Auto - moto </option>
                        <option value="Bankovnictví a finanční služby" {if (isset($smarty_field[$row_count]) && $smarty_field[$row_count] == 'Bankovnictví a finanční služby')}selected{/if}> Bankovnictví a finanční služby </option>
                        <option value="Cestovní ruch a ubytování" {if (isset($smarty_field[$row_count]) && $smarty_field[$row_count] == 'Cestovní ruch a ubytování')}selected{/if}> Cestovní ruch a ubytování </option>
                        <option value="Doprava, logistika a zásobování" {if (isset($smarty_field[$row_count]) && $smarty_field[$row_count] == 'Doprava, logistika a zásobování')}selected{/if}> Doprava, logistika a zásobování </option>
                        <option value="Ekonomika a podnikové finance" {if (isset($smarty_field[$row_count]) && $smarty_field[$row_count] == 'Ekonomika a podnikové finance')}selected{/if}> Ekonomika a podnikové finance </option>
                        <option value="Elektrotechnika a energetika" {if (isset($smarty_field[$row_count]) && $smarty_field[$row_count] == 'Elektrotechnika a energetika')}selected{/if}> Elektrotechnika a energetika </option>
                        <option value="Farmacie" {if (isset($smarty_field[$row_count]) && $smarty_field[$row_count] == 'Farmacie')}selected{/if}> Farmacie </option>
                        <option value="Gastronomie a pohostinství" {if (isset($smarty_field[$row_count]) && $smarty_field[$row_count] == 'Gastronomie a pohostinství')}selected{/if}> Gastronomie a pohostinství </option>
                        <option value="Chemický průmysl" {if (isset($smarty_field[$row_count]) && $smarty_field[$row_count] == 'Chemický průmysl')}selected{/if}> Chemický průmysl </option>
                        <option value="IS/IT: Konzultace, analýzy a projektové řízení" {if (isset($smarty_field[$row_count]) && $smarty_field[$row_count] == 'IS/IT: Konzultace, analýzy a projektové řízení')}selected{/if}> IS/IT: Konzultace, analýzy a projektové řízení </option>
                        <option value="IS/IT: Správa systémů a HW" {if (isset($smarty_field[$row_count]) && $smarty_field[$row_count] == 'IS/IT: Správa systémů a HW')}selected{/if}> IS/IT: Správa systémů a HW </option>
                        <option value="IS/IT: Vývoj aplikací a systémů" {if (isset($smarty_field[$row_count]) && $smarty_field[$row_count] == 'IS/IT: Vývoj aplikací a systémů')}selected{/if}> IS/IT: Vývoj aplikací a systémů </option>
                        <option value="Kultura, umění a tvůrčí práce" {if (isset($smarty_field[$row_count]) && $smarty_field[$row_count] == 'Kultura, umění a tvůrčí práce')}selected{/if}> Kultura, umění a tvůrčí práce </option>
                        <option value="Kvalita a kontrola jakosti" {if (isset($smarty_field[$row_count]) && $smarty_field[$row_count] == 'Kvalita a kontrola jakosti')}selected{/if}> Kvalita a kontrola jakosti </option>
                        <option value="Marketing" {if (isset($smarty_field[$row_count]) && $smarty_field[$row_count] == 'Marketing')}selected{/if}> Marketing </option>
                        <option value="Média, reklama a PR" {if (isset($smarty_field[$row_count]) && $smarty_field[$row_count] == 'Média, reklama a PR')}selected{/if}> Média, reklama a PR </option>
                        <option value="Nákup" {if (isset($smarty_field[$row_count]) && $smarty_field[$row_count] == 'Nákup')}selected{/if}> Nákup </option>
                        <option value="Ostraha a bezpečnost" {if (isset($smarty_field[$row_count]) && $smarty_field[$row_count] == 'Ostraha a bezpečnost')}selected{/if}> Ostraha a bezpečnost </option>
                        <option value="Personalistika a HR" {if (isset($smarty_field[$row_count]) && $smarty_field[$row_count] == 'Personalistika a HR')}selected{/if}> Personalistika a HR </option>
                        <option value="Pojišťovnictví" {if (isset($smarty_field[$row_count]) && $smarty_field[$row_count] == 'Pojišťovnictví')}selected{/if}> Pojišťovnictví </option>
                        <option value="Potravinářství" {if (isset($smarty_field[$row_count]) && $smarty_field[$row_count] == 'Potravinářství')}selected{/if}> Potravinářství </option>
                        <option value="Právní služby" {if (isset($smarty_field[$row_count]) && $smarty_field[$row_count] == 'Právní služby')}selected{/if}> Právní služby </option>
                        <option value="Prodej a obchod" {if (isset($smarty_field[$row_count]) && $smarty_field[$row_count] == 'Prodej a obchod')}selected{/if}> Prodej a obchod </option>
                        <option value="Řemeslné a manuální práce" {if (isset($smarty_field[$row_count]) && $smarty_field[$row_count] == 'Řemeslné a manuální práce')}selected{/if}> Řemeslné a manuální práce </option>
                        <option value="Služby" {if (isset($smarty_field[$row_count]) && $smarty_field[$row_count] == 'Služby')}selected{/if}> Služby </option>
                        <option value="Státní a veřejná správa" {if (isset($smarty_field[$row_count]) && $smarty_field[$row_count] == 'Státní a veřejná správa')}selected{/if}> Státní a veřejná správa </option>
                        <option value="Stavebnictví a reality" {if (isset($smarty_field[$row_count]) && $smarty_field[$row_count] == 'Stavebnictví a reality')}selected{/if}> Stavebnictví a reality </option>
                        <option value="Strojírenství" {if (isset($smarty_field[$row_count]) && $smarty_field[$row_count] == 'Strojírenství')}selected{/if}> Strojírenství </option>
                        <option value="Technika a vývoj" {if (isset($smarty_field[$row_count]) && $smarty_field[$row_count] == 'Technika a vývoj')}selected{/if}> Technika a vývoj </option>
                        <option value="Telekomunikace" {if (isset($smarty_field[$row_count]) && $smarty_field[$row_count] == 'Telekomunikace')}selected{/if}> Telekomunikace </option>
                        <option value="Věda a výzkum" {if (isset($smarty_field[$row_count]) && $smarty_field[$row_count] == 'Věda a výzkum')}selected{/if}> Věda a výzkum </option>
                        <option value="Vrcholový management" {if (isset($smarty_field[$row_count]) && $smarty_field[$row_count] == 'Vrcholový management')}selected{/if}> Vrcholový management </option>
                        <option value="Vydavatelství, tisk a polygrafie" {if (isset($smarty_field[$row_count]) && $smarty_field[$row_count] == 'Vydavatelství, tisk a polygrafie')}selected{/if}> Vydavatelství, tisk a polygrafie </option>
                        <option value="Výroba a průmysl" {if (isset($smarty_field[$row_count]) && $smarty_field[$row_count] == 'Výroba a průmysl')}selected{/if}> Výroba a průmysl </option>
                        <option value="Vzdělávání a školství" {if (isset($smarty_field[$row_count]) && $smarty_field[$row_count] == 'Vzdělávání a školství')}selected{/if}> Vzdělávání a školství </option>
                        <option value="Zákaznický servis" {if (isset($smarty_field[$row_count]) && $smarty_field[$row_count] == 'Zákaznický servis')}selected{/if}> Zákaznický servis </option>
                        <option value="Zdravotnictví a sociální péče" {if (isset($smarty_field[$row_count]) && $smarty_field[$row_count] == 'Zdravotnictví a sociální péče')}selected{/if}> Zdravotnictví a sociální péče </option>
                        <option value="Zemědělství, lesnictví a ekologie" {if (isset($smarty_field[$row_count]) && $smarty_field[$row_count] == 'Zemědělství, lesnictví a ekologie')}selected{/if}> Zemědělství, lesnictví a ekologie </option>
                      </select>
                     </td>
                  </tr>
                  <tr>
                    <td> <label for="description[]"> Popis: </label> </td>
                    <td>
                      <textarea name="description[]">{if isset($smarty_description[$row_count])}{$smarty_description[$row_count]}{/if}</textarea>
                      {if isset($smarty_validation_description[$row_count])}
                        <br />
                        {$smarty_validation_description[$row_count]}
                      {/if}
                    </td>
                  </tr>
                  <tr>
                    <td> <label for="reference_name[]"> Referenční osoba:  </label> </td>
                    <td>
                      <input type="text" name="reference_name[]" value="{if isset($smarty_reference_name[$row_count])}{$smarty_reference_name[$row_count]}{/if}" />
                      {if isset($smarty_validation_reference_name[$row_count])}
                        {$smarty_validation_reference_name[$row_count]}
                      {/if}
                    </td>
                  </tr>
                  <tr>
                    <td> <label for="reference_contact[]"> Kontakt:  </label> </td>
                    <td>
                      <input type="text" name="reference_contact[]" value="{if isset($smarty_reference_contact[$row_count])}{$smarty_reference_contact[$row_count]}{/if}" />
                      {if isset($smarty_validation_reference_contact[$row_count])}
                        {$smarty_validation_reference_contact[$row_count]}
                      {/if}
                    </td>
                  </tr>
                </table>
              </td>
            </tr>
          {/for}
        </tbody>
      </table>

      <input type="submit" value="< Zpět" name="creator_form_back" class="btn" />
      <input type="submit" value="Pokračovat >" name="creator_form_submit" class="btn" />

    </form>

    <!-- <form name="creator_form_work" id="creator_form_work" action="" method="POST">

      <table id="dataTable" class="form" border="1">
        <tbody>
          {for $row_count=0 to $smarty_row_count - 1}
          <tr>
            <p>
              <td style="min-width: 15px;"><input type="checkbox" required="required" name="chk[]" checked="checked" /></td>
              <td>
                <label for="creator_form_work_school[]"> Instituce: </label>
                <input type="text" name="creator_form_work_school[]" value="{if isset($smarty_creator_form_work_school[$row_count])}{$smarty_creator_form_work_school[$row_count]}{/if}" />
                {if isset($smarty_validation_school[$row_count])}
                  <ul>
                    {$smarty_validation_school[$row_count]}
                  <ul>
                {/if}
              </td>
              <td>
                <label for="creator_form_work_title[]"> Titul: </label>
                <input type="text" name="creator_form_work_title[]" value="{if isset($smarty_creator_form_work_title[$row_count])}{$smarty_creator_form_work_title[$row_count]}{/if}" />
                {if isset($smarty_validation_title[$row_count])}
                  <ul>
                    {$smarty_validation_title[$row_count]}
                  <ul>
                {/if}
              </td>
              <td>
                <label for="creator_form_work_startYear[]"> Od: </label>
                <input type="text" name="creator_form_work_startYear[]" value="{if isset($smarty_creator_form_work_startYear[$row_count])}{$smarty_creator_form_work_startYear[$row_count]}{/if}" />
                {if isset($smarty_validation_startYear[$row_count])}
                  <ul>
                    {$smarty_validation_startYear[$row_count]}
                  <ul>
                {/if}
              </td>

              <td>
                <label for="creator_form_work_endYear[]"> Do: </label>
                <input type="text" name="creator_form_work_endYear[]" value="{if isset($smarty_creator_form_work_endYear[$row_count])}{$smarty_creator_form_work_endYear[$row_count]}{/if}" />
                {if isset($smarty_validation_endYear[$row_count])}
                  <ul>
                    {$smarty_validation_endYear[$row_count]}
                  <ul>
                {/if}
              </td>
              <td> <label for="creator_form_work_degree[]"> Stupeň: </label>
                <select name="creator_form_work_degree[]">
                  <option value="Základní" {if (isset($smarty_creator_form_work_degree[$row_count]) && $smarty_creator_form_work_degree[$row_count] == 'Základní')}selected{/if}> Základní </option>
                  <option value="Střední (výuční list)" {if (isset($smarty_creator_form_work_degree[$row_count]) && $smarty_creator_form_work_degree[$row_count] == 'Střední (výuční list)')}selected{/if}> Střední (výuční list) </option>
                  <option value="Střední (maturita)" {if (isset($smarty_creator_form_work_degree[$row_count]) && $smarty_creator_form_work_degree[$row_count] == 'Střední (maturita)')}selected{/if}> Střední (maturita) </option>
                  <option value="Vyšší odborné" {if (isset($smarty_creator_form_work_degree[$row_count]) && $smarty_creator_form_work_degree[$row_count] == 'Vyšší odborné')}selected{/if}> Vyšší odborné </option>
                  <option value="Vysoká škola" {if (isset($smarty_creator_form_work_degree[$row_count]) && $smarty_creator_form_work_degree[$row_count] == 'Vysoká škola')}selected{/if}> Vysoká škola </option>
                  <option value="Jiné" {if (isset($smarty_creator_form_work_degree[$row_count]) && $smarty_creator_form_work_degree[$row_count] == 'Jiné')}selected{/if}> Jiné </option>
                </select>
              </td>
            </p>
          </tr>
          {/for}
        </tbody>
      </table>

      <input type="submit" value="< Zpět" name="creator_form_work_back" class="btn" />
      <input type="submit" value="Pokračovat >" name="creator_form_work_submit" class="btn" />
    </form> -->

{include file='default_footer.tpl'}
