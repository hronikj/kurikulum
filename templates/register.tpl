{include file='default_header.tpl'}
  {include file='default_messages.tpl'}

  {if isset($smarty_access)}
    <h2> Povinná pole </h2>
    <form name="register_form" action="" method="POST">
      <table>
        <tr>
          <td> <label for="login"> Email (login) </label> </td>
          <td> <input type="text" name="login" value="{if isset($smarty_login)}{$smarty_login}{/if}" /> </td>
        </tr>
        <tr>
          <td> <label for="password"> Heslo </label> </td>
          <td> <input type="password" name="password" /> </td>
        </tr>

        <tr>
          <td> <label for="password2"> Heslo podruhé </label> </td>
          <td> <input type="password" name="password2" /> </td>
        </tr>
      </table>

      <h2> Nepovinná pole </h2>
      <table>
        <tr>
          <td> <label for="firstname"> Jméno </label> </td>
          <td> <input type="text" name="firstname" value="{if isset($smarty_firstname)}{$smarty_firstname}{/if}" /> </td>
        </tr>

        <tr>
          <td> <label for="lastname"> Příjmení </label> </td>
          <td> <input type="text" name="lastname" value="{if isset($smarty_lastname)}{$smarty_lastname}{/if}" /> </td>
        </tr>

        <tr>
          <td> <label for="permanent_address"> Adresa trvalého bydliště </label> </td>
          <td> <input type="text" name="permanent_address" value="{if isset($smarty_permanent_address)}{$smarty_permanent_address}{/if}" /> </td>
        </tr>

        <tr>
          <td> <label for="permanent_zipcode"> PSČ trvalého bydliště </label> </td>
          <td> <input type="text" name="permanent_zipcode" value="{if isset($smarty_permanent_zipcode)}{$smarty_permanent_zipcode}{/if}" /> </td>
        </tr>

        <tr>
          <td> <label for="permanent_city"> Město trvalého bydliště </label> </td>
          <td> <input type="text" name="permanent_city" value="{if isset($smarty_permanent_city)}{$smarty_permanent_city}{/if}" /> </td>
        </tr>

        <tr>
          <td> <label for="contact_address"> Adresa přechodného bydliště </label> </td>
          <td> <input type="text" name="contact_address" value="{if isset($smarty_contact_address)}{$smarty_contact_address}{/if}" /> </td>
        </tr>

        <tr>
          <td> <label for="contact_zipcode"> PSČ přechodného bydliště </label> </td>
          <td> <input type="text" name="contact_zipcode" value="{if isset($smarty_contact_zipcode)}{$smarty_contact_zipcode}{/if}" /> </td>
        </tr>

        <tr>
          <td> <label for="_city"> Město přechodného bydliště </label> </td>
          <td> <input type="text" name="contact_city" value="{if isset($smarty_contact_city)}{$smarty_contact_city}{/if}" /> </td>
        </tr>

        <tr>
          <td> <label for="telephone"> Telefon </label> </td>
          <td> <input type="text" name="telephone" value="{if isset($smarty_telephone)}{$smarty_telephone}{/if}" /> </td>
        </tr>

        <tr>
          <td> <label for="nationality"> Národnost </label> </td>
          <td> <input type="text" name="nationality" value="{if isset($smarty_nationality)}{$smarty_nationality}{/if}"  /> </td>
        </tr>

        <tr>
          <td> <label for="birthdate"> Datum narození </label> </td>
          <td> <input type="text" name="birthdate" value="{if isset($smarty_birthdate)}{$smarty_birthdate}{/if}"  /> </td>
        </tr>
    </table>


    <input name="submit" type="submit" class="btn" value="Zaregistrovat" />
    <a href="/index"> <span type="submit" value="Zpět na profil" class="btn"> Zpět na hlavní stránku </span> </a>
  </form>
  {else if isset($smarty_access_paid)}
    Sorry, access is restricted to those who paid!
  {else if isset($smarty_access_registered)}
    Sorry, access is restricted to registered users!
  {/if}


{include file='default_footer.tpl'}
