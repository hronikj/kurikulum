{include file='default_header.tpl'}
  {include file='default_messages.tpl'}
  <form name="profile_edit_skills_form" method="POST" action="">
    <table>
      <tr>
        <td> Rok zahájení </td> <td> <input type="text" name="profile_edit_skills_name" value="{$smarty_name}" /> </td>
      </tr>

      <tr>
        <td> Úroveň: </td>
        <td>
          <select name="profile_edit_skills_level">
            <option value="Základní"> Základní znalost </option>
            <option value="Mírně pokročilá"> Mírně pokročilá znalost </option>
            <option value="Pokročilá"> Pokročilá znalost </option>
            <option value="Expertní"> Expertní </option>
          </select>
        </td>
      </tr>

    </table>
    <input type="submit" value="Odeslat" name="profile_edit_skills_submit" class="btn" />
    <a href="/profile"> <span type="submit" value="Zpět na profil" class="btn"> Zpět na profil </span> </a>
  </form>

{include file='default_footer.tpl'}
