{include file='default_header.tpl'}
  {include file='default_messages.tpl'}

    {if $smarty_logged_out}
      <p> Uživatel úspěšně odhlášen! :) </p>
      <ul>
        <li> <a href="/index"> Pokračovat na úvodní stránku </a> </li>
        <li> <a href="/login"> Přihlásit se </a> </li>
      </ul>

      {else}
      <p> Uživatel není přihlášen. Není možné provést odhlášení! </p>
      <ul>
        <li> <a href="/index"> Pokračovat na úvodní stránku </a> </li>
        <li> <a href="/login"> Přihlásit se </a> </li>
      </ul>
    {/if}

{include file='default_footer.tpl'}
