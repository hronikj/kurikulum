{include file='default_header.tpl'}
  {include file='default_messages.tpl'}
  <form name="profile_add_education_form" method="POST" action="">
    <table>
      <tr>
        <td> Rok zahájení </td> <td> <input type="text" name="profile_add_education_startYear" value="{if isset($smarty_profile_add_education_startYear)}{$smarty_profile_add_education_startYear}{/if}" /> </td>
      </tr>

      <tr>
        <td> Rok ukončení: </td> <td> <input type="text" name="profile_add_education_endYear" value="{if isset($smarty_profile_add_education_endYear)}{$smarty_profile_add_education_endYear}{/if}" /> </td>
      </tr>

      <tr>
        <td> Instituce </td> <td> <input type="text" name="profile_add_education_school" value="{if isset($smarty_profile_add_education_school)}{$smarty_profile_add_education_school}{/if}" /> </td>
      </tr>

      <tr>
        <td> Udělený titul </td> <td> <input type="text" name="profile_add_education_title" value="{if isset($smarty_profile_add_education_title)}{$smarty_profile_add_education_title}{/if}" /> </td>
      </tr>

      <tr>
        <td> Typ vzdělání </td>
        <td>
          <select name="profile_add_education_degree" value="{if isset($smarty_profile_add_education_degree)}{$smarty_profile_add_education_degree}{/if}">
            <option value="Základní"> Základní </option>
            <option value="Střední (výuční list)"> Střední (výuční list) </option>
            <option value="Střední (maturita)"> Střední (maturita) </option>
            <option value="Vyšší odborné"> Vyšší odborné </option>
            <option value="Vysoká škola"> Vysoká škola </option>
            <option value="Jiné"> Jiné </option>
          </select>
        </td>
      </tr>
    </table>
    <input type="submit" value="Odeslat" name="profile_add_education_submit" class="btn" />
  </form>

{include file='default_footer.tpl'}
