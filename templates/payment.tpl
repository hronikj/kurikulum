{include file='default_header.tpl'}
  {include file='default_messages.tpl'}
  {if $smarty_display}
    <p> Kliknutím na tlačítko odeslat budete přesměrování k platební bráně. Následně Vám bude aktivováno předplatné služby. </p>
    <form name="payment_create_form" method="POST" action="">
      <input type="submit" value="Zaplatit a aktivovat předplatné" name="payment_create_form_submit" class="btn" />
    </form>
  {else}
    <p style="margin-top: 70px;">  <i class="fa fa-thumbs-o-up" style="font-size: 20px; padding-right: 10px;"></i> Vaše předplatné je momentálně aktivní a vyprší <i> {$smarty_subscription_to}. </i> </p>
    <p> Děkujeme za váš zájem. </p>
  {/if}

{include file='default_footer.tpl'}
