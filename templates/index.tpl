{include file='default_header.tpl'}
  {include file='default_messages.tpl'}
  <div style="width: 70%; float: left">
    <h1> {$smarty_header} </h1>
    <p> {$smarty_sub} </p>

    <h2> {$smarty_header2} </h2>
    <p> {$smarty_sub2} </p>

    <p> {$smarty_main_text} </p>
  </div>

  <div style="float: right; padding-top: 30px; padding-right: 30px;">
     <a href="creator" style="font-size: 20px; text-decoration: none;" class="btn"> Založte si nový životopis! </a> <br />
     <a href="configurations" style="font-size: 20px; text-decoration: none;" class="btn"> Moje životopisy </a>
   </div>

{include file='default_footer.tpl'}
