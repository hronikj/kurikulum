{include file='default_header.tpl'}
  {include file='default_messages.tpl'}

  <script type="text/javascript">
    function addRow(tableID) {
      var table = document.getElementById(tableID);
      var rowCount = table.rows.length;
      if(rowCount < 5){  // limit the user from creating fields more than your limits
        var row = table.insertRow(rowCount);
        var colCount = table.rows[0].cells.length;
        for (var i = 0; i < colCount; i++) {
          var newcell = row.insertCell(i);
          newcell.innerHTML = table.rows[0].cells[i].innerHTML;
        }
      } else {
        alert("Maximum položek pro tento formulář je: 5");
      }
    }

    function deleteRow(tableID) {
      var table = document.getElementById(tableID);
      var rowCount = table.rows.length;
      for(var i=0; i<rowCount; i++) {
        var row = table.rows[i];
        var chkbox = row.cells[0].childNodes[0];
        if(null != chkbox && true == chkbox.checked) {
          if(rowCount <= 1) { // limit the user from removing all the fields
            alert("Nelze odebrat všechny řádky!");
            break;
          }
          table.deleteRow(i);
          rowCount--;
          i--;
        }
      }
    }
  </script>

  <style>
      .fieldgroup label.error {
      color: #FB3A3A;
      display: inline-block;
      margin: 4px 0 5px 125px;
      padding: 0;
      text-align: left;
      width: 220px;
    }

    .error {
    color: red;
}

  </style>


    <h1> Průvodce vytvořením CV - vzdělání </h1>

    {include file='creator_controls.tpl'}

    <form name="creator_form_education" id="creator_form_education" action="" method="POST">
      <table id="dataTable" class="form" border="0">
        <tbody>
          {for $row_count=0 to $smarty_row_count - 1}
          <tr>
              <td><input type="checkbox" required="required" name="chk[]" checked="checked" class="table-checkbox" /><input type="text" name="id[]" value="{$row_count}" hidden="true" /></td>
              <td>
                <table>
                  <tr>
                    <td> <label for="school[]"> Škola:  </label> </td>
                    <td>
                      <input type="text" name="school[]" value="{if isset($smarty_school[$row_count])}{$smarty_school[$row_count]}{/if}" />
                      {if isset($smarty_validation_school[$row_count])}
                        {$smarty_validation_school[$row_count]}
                      {/if}
                    </td>
                  </tr>
                  <tr>
                    <td> <label for="title[]"> Titul: </label> </td>
                    <td>
                      <input type="text" name="title[]" value="{if isset($smarty_title[$row_count])}{$smarty_title[$row_count]}{/if}" />
                      {if isset($smarty_validation_title[$row_count])}
                        {$smarty_validation_title[$row_count]}
                      {/if}
                    </td>
                  </tr>
                  <tr>
                    <td> <label for="start_month[]"> Studium od (měsíc, rok): </label> </td>
                    <td>
                      <select name="start_month[]">
                        <option value="1" {if (isset($smarty_start_month[$row_count]) && $smarty_start_month[$row_count] == 1)}selected{/if}> leden </option>
                        <option value="2" {if (isset($smarty_start_month[$row_count]) && $smarty_start_month[$row_count] == 2)}selected{/if}> únor </option>
                        <option value="3" {if (isset($smarty_start_month[$row_count]) && $smarty_start_month[$row_count] == 3)}selected{/if}> březen </option>
                        <option value="4" {if (isset($smarty_start_month[$row_count]) && $smarty_start_month[$row_count] == 4)}selected{/if}> duben </option>
                        <option value="5" {if (isset($smarty_start_month[$row_count]) && $smarty_start_month[$row_count] == 5)}selected{/if}> květen </option>
                        <option value="6" {if (isset($smarty_start_month[$row_count]) && $smarty_start_month[$row_count] == 6)}selected{/if}> červen </option>
                        <option value="7" {if (isset($smarty_start_month[$row_count]) && $smarty_start_month[$row_count] == 7)}selected{/if}> červenec </option>
                        <option value="8" {if (isset($smarty_start_month[$row_count]) && $smarty_start_month[$row_count] == 8)}selected{/if}> srpen </option>
                        <option value="9" {if (isset($smarty_start_month[$row_count]) && $smarty_start_month[$row_count] == 9)}selected{/if}> září </option>
                        <option value="10" {if (isset($smarty_start_month[$row_count]) && $smarty_start_month[$row_count] == 10)}selected{/if}> říjen </option>
                        <option value="11" {if (isset($smarty_start_month[$row_count]) && $smarty_start_month[$row_count] == 11)}selected{/if}> listopad </option>
                        <option value="12" {if (isset($smarty_start_month[$row_count]) && $smarty_start_month[$row_count] == 12)}selected{/if}> prosinec </option>
                      </select>
                      <select name="start_year[]">
                        {for $year = 1950 to 2015}
                          <option value="{$year}" {if (isset($smarty_start_year[$row_count]) && $smarty_start_year[$row_count] == $year)}selected{/if}> {$year} </option>
                        {/for}
                      </select>
                    </td>
                  </tr>
                  <tr>
                    <td> Studium ukončeno (měsíc, rok): </td>
                    <td>
                      <select name="end_month[]">
                        <option value="1" {if (isset($smarty_end_month[$row_count]) && $smarty_end_month[$row_count] == 1)}selected{/if}> leden </option>
                        <option value="2" {if (isset($smarty_end_month[$row_count]) && $smarty_end_month[$row_count] == 2)}selected{/if}> únor </option>
                        <option value="3" {if (isset($smarty_end_month[$row_count]) && $smarty_end_month[$row_count] == 3)}selected{/if}> březen </option>
                        <option value="4" {if (isset($smarty_end_month[$row_count]) && $smarty_end_month[$row_count] == 4)}selected{/if}> duben </option>
                        <option value="5" {if (isset($smarty_end_month[$row_count]) && $smarty_end_month[$row_count] == 5)}selected{/if}> květen </option>
                        <option value="6" {if (isset($smarty_end_month[$row_count]) && $smarty_end_month[$row_count] == 6)}selected{/if}> červen </option>
                        <option value="7" {if (isset($smarty_end_month[$row_count]) && $smarty_end_month[$row_count] == 7)}selected{/if}> červenec </option>
                        <option value="8" {if (isset($smarty_end_month[$row_count]) && $smarty_end_month[$row_count] == 8)}selected{/if}> srpen </option>
                        <option value="9" {if (isset($smarty_end_month[$row_count]) && $smarty_end_month[$row_count] == 9)}selected{/if}> září </option>
                        <option value="10" {if (isset($smarty_end_month[$row_count]) && $smarty_end_month[$row_count] == 10)}selected{/if}> říjen </option>
                        <option value="11" {if (isset($smarty_end_month[$row_count]) && $smarty_end_month[$row_count] == 11)}selected{/if}> listopad </option>
                        <option value="12" {if (isset($smarty_end_month[$row_count]) && $smarty_end_month[$row_count] == 12)}selected{/if}> prosinec </option>
                      </select>
                      <select name="end_year[]">
                        {for $year = 1950 to 2015}
                          <option value="{$year}" {if (isset($smarty_end_year[$row_count]) && $smarty_end_year[$row_count] == $year)}selected{/if}> {$year} </option>
                        {/for}
                      </select>
                     </td>
                  </tr>
                  <tr>
                    <td> <label for="degree[]"> Typ studia: </label> </td>
                    <td>
                      <select name="degree[]">
                        <option value="Základní" {if (isset($smarty_degree[$row_count]) && $smarty_degree[$row_count] == 'Základní')}selected{/if}> Základní </option>
                        <option value="Střední (výuční list)" {if (isset($smarty_degree[$row_count]) && $smarty_degree[$row_count] == 'Střední (výuční list)')}selected{/if}> Střední (výuční list) </option>
                        <option value="Střední (maturita)" {if (isset($smarty_degree[$row_count]) && $smarty_degree[$row_count] == 'Střední (maturita)')}selected{/if}> Střední (maturita) </option>
                        <option value="Vyšší odborné" {if (isset($smarty_degree[$row_count]) && $smarty_degree[$row_count] == 'Vyšší odborné')}selected{/if}> Vyšší odborné </option>
                        <option value="Vysoká škola" {if (isset($smarty_degree[$row_count]) && $smarty_degree[$row_count] == 'Vysoká škola')}selected{/if}> Vysoká škola </option>
                        <option value="Jiné" {if (isset($smarty_degree[$row_count]) && $smarty_degree[$row_count] == 'Jiné')}selected{/if}> Jiné </option>
                      </select>
                    </td>
                  </tr>
                  <tr>
                    <td> <label for="finished[]"> Ukončeno: </label> </td>
                    <td>
                      <select name="finished[]">
                        <option value="Ukončeno" {if (isset($smarty_finished[$row_count]) && $smarty_finished[$row_count] == 'Ukončeno')}selected{/if}> Ukončeno </option>
                        <option value="Neukončeno" {if (isset($smarty_finished[$row_count]) && $smarty_finished[$row_count] == 'Neukončeno')}selected{/if}> Neukončeno </option>
                      </select>
                    </td>
                  </tr>
                </table>
              </td>
            </tr>
          {/for}
        </tbody>
      </table>

      <input type="submit" value="< Zpět" name="creator_form_back" class="btn" />
      <input type="submit" value="Pokračovat >" name="creator_form_submit" class="btn" />

    </form>

    <!-- <form name="creator_form_education" id="creator_form_education" action="" method="POST">

      <table id="dataTable" class="form" border="1">
        <tbody>
          {for $row_count=0 to $smarty_row_count - 1}
          <tr>
            <p>
              <td style="min-width: 15px;"><input type="checkbox" required="required" name="chk[]" checked="checked" /></td>
              <td>
                <label for="creator_form_education_school[]"> Instituce: </label>
                <input type="text" name="creator_form_education_school[]" value="{if isset($smarty_creator_form_education_school[$row_count])}{$smarty_creator_form_education_school[$row_count]}{/if}" />
                {if isset($smarty_validation_school[$row_count])}
                  <ul>
                    {$smarty_validation_school[$row_count]}
                  <ul>
                {/if}
              </td>
              <td>
                <label for="creator_form_education_title[]"> Titul: </label>
                <input type="text" name="creator_form_education_title[]" value="{if isset($smarty_creator_form_education_title[$row_count])}{$smarty_creator_form_education_title[$row_count]}{/if}" />
                {if isset($smarty_validation_title[$row_count])}
                  <ul>
                    {$smarty_validation_title[$row_count]}
                  <ul>
                {/if}
              </td>
              <td>
                <label for="creator_form_education_startYear[]"> Od: </label>
                <input type="text" name="creator_form_education_startYear[]" value="{if isset($smarty_creator_form_education_startYear[$row_count])}{$smarty_creator_form_education_startYear[$row_count]}{/if}" />
                {if isset($smarty_validation_startYear[$row_count])}
                  <ul>
                    {$smarty_validation_startYear[$row_count]}
                  <ul>
                {/if}
              </td>

              <td>
                <label for="creator_form_education_endYear[]"> Do: </label>
                <input type="text" name="creator_form_education_endYear[]" value="{if isset($smarty_creator_form_education_endYear[$row_count])}{$smarty_creator_form_education_endYear[$row_count]}{/if}" />
                {if isset($smarty_validation_endYear[$row_count])}
                  <ul>
                    {$smarty_validation_endYear[$row_count]}
                  <ul>
                {/if}
              </td>
              <td> <label for="creator_form_education_degree[]"> Stupeň: </label>
                <select name="creator_form_education_degree[]">
                  <option value="Základní" {if (isset($smarty_creator_form_education_degree[$row_count]) && $smarty_creator_form_education_degree[$row_count] == 'Základní')}selected{/if}> Základní </option>
                  <option value="Střední (výuční list)" {if (isset($smarty_creator_form_education_degree[$row_count]) && $smarty_creator_form_education_degree[$row_count] == 'Střední (výuční list)')}selected{/if}> Střední (výuční list) </option>
                  <option value="Střední (maturita)" {if (isset($smarty_creator_form_education_degree[$row_count]) && $smarty_creator_form_education_degree[$row_count] == 'Střední (maturita)')}selected{/if}> Střední (maturita) </option>
                  <option value="Vyšší odborné" {if (isset($smarty_creator_form_education_degree[$row_count]) && $smarty_creator_form_education_degree[$row_count] == 'Vyšší odborné')}selected{/if}> Vyšší odborné </option>
                  <option value="Vysoká škola" {if (isset($smarty_creator_form_education_degree[$row_count]) && $smarty_creator_form_education_degree[$row_count] == 'Vysoká škola')}selected{/if}> Vysoká škola </option>
                  <option value="Jiné" {if (isset($smarty_creator_form_education_degree[$row_count]) && $smarty_creator_form_education_degree[$row_count] == 'Jiné')}selected{/if}> Jiné </option>
                </select>
              </td>
            </p>
          </tr>
          {/for}
        </tbody>
      </table>

      <input type="submit" value="< Zpět" name="creator_form_education_back" class="btn" />
      <input type="submit" value="Pokračovat >" name="creator_form_education_submit" class="btn" />
    </form> -->

{include file='default_footer.tpl'}
