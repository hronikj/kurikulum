{include file='default_header.tpl'}
  {include file='default_messages.tpl'}
  <form name="profile_edit_language_form" method="POST" action="">
    <table>
      <tr>
        <td> Jazyk </td> <td> <input type="text" name="profile_edit_language_name" value="{$smarty_name}" /> </td>
      </tr>

      <tr>
        <td> Úroveň: </td>
        <td>
          <select name="profile_add_language_level">
            <option value="A1"> A1 </option>
            <option value="A2"> A2 </option>
            <option value="B1"> B1 </option>
            <option value="B2"> B2 </option>
            <option value="C1"> C1 </option>
            <option value="C2"> C2 </option>
            <option value="Rodilý mluvčí"> Rodilý mluvčí </option>
          </select>
        </td>
      </tr>

    </table>
    <input type="submit" value="Odeslat" name="profile_edit_language_submit" class="btn" />
    <a href="/profile"> <span type="submit" value="Zpět na profil" class="btn"> Zpět na profil </span> </a>
  </form>

{include file='default_footer.tpl'}
