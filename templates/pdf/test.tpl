<style>
  body {
    font-size: 12px;
  }

  table {
    border: 0px solid #ddd;
    border-collapse: collapse;
  }

  h1 {
    font-family: 'Jomolhari';
    padding-top: 0px;
    padding-bottom: 0px;
    margin-bottom: 0px;
  }

  h3 {
    font-family: 'FreeMono';
    margin-top: 20px;
    width: 100%;
    background-color: #eee;
    padding-left: 5px;
  }

  td {
    border: 0px solid #ddd;
    padding: 5px;
    font-family: 'Uthman.ttf';
  }

  #header {
    width: 100%;
    background-color: #000;
    color: #fff;
    padding-left: 15px;
    padding-top: 0px;
    padding-bottom: 0px;
    margin-bottom: 0px;
  }

  #header-id {
    position: absolute;
    top: 58px;
    left: 230px;
    color: #fff;
    font-size: 12px;
  }

  #subheader {
    width: 100%;
    background-color: #ddd;
    padding-top: 10px;
    padding-left: 10px;
    padding-bottom: 10px;
    font-family: 'Garuda';
  }

  #image {
    width: 128px;
    height: 115px;
    background-image: url(http://38.media.tumblr.com/avatar_fd4afa12595a_128.png);
    position: absolute;
    right: 37px;
  }
</style>
<div id="image"> </div>

<div id="header-id"> <i> resume by CareerWay.cz </i> </div>
<div id="header">
  <h1> {$smarty_firstName} {$smarty_lastName} </h1>
</div>
<div id="subheader">
  Email: {$smarty_email}, Telephone: {$smarty_telephone} <br />
  City: {$smarty_city} (Zip Code: {$smarty_zipCode})
</div>

<h3> Education </h3>
<table>
{foreach from=$smarty_education_data key=myId item=i}
  <tr>
    <td rowspan="3" valign="top" width="100"> {$i.startYear} - {$i.endYear} </td>
    <td> <b> School: </b> </td> <td> {$i.school} </td>
  </tr>
  <tr>
    <td> <b> Degree: </b> </td> <td> {$i.degree} </td>
  </tr>
  <tr>
    <td> <b> Title: </b> </td> <td> {$i.title}  </td>
  </tr>
  <tr> <td colspan="3"> </td> </tr>
{/foreach}
</table>

<h3> Work </h3>
<table>
{foreach from=$smarty_work_data key=myId item=i}
  <tr>
    <td rowspan="3" valign="top" width="100"> {$i.startYear} - {$i.endYear} </td>
    <td> <b> Employer: </b> </td> <td> {$i.employer} </td>
  </tr>
  <tr>
    <td> <b> Position: </b> </td> <td> {$i.position} </td>
  </tr>
  <tr>
    <td> <b> Description: </b> </td> <td> {$i.description} </td>
  </tr>
  <tr> <td colspan="3"> </td> </tr>
{/foreach}
</table>


<h3> Languages </h3>
<table>
  {foreach from=$smarty_languages_data key=myId item=i}
  <tr>
    <td> <b> Language: </b> </td> <td> {$i.name} - {$i.level} </td>
  </tr>
  {/foreach}
</table>

<h3> Skills </h3>
<table>
  {foreach from=$smarty_skills_data key=myId item=i}
  <tr>
    <td> <b> Skill: </b> </td> <td> {$i.name} - {$i.level} </td>
  </tr>
  {/foreach}
</table>

<h3> Certifications </h3>
<table>
{foreach from=$smarty_certification_data key=myId item=i}
  <tr>
    <td rowspan="3" valign="top" width="100"> {$i.year}  </td>
    <td> <b> Certification: </b> </td> <td> {$i.name} </td>
  </tr>
  <tr> <td> <b> School: </b> </td> <td> {$i.school} </td>
  <tr> <td colspan="3"> </td> </tr>
{/foreach}
</table>
