-- MySQL dump 10.13  Distrib 5.6.25, for Linux (x86_64)
--
-- Host: localhost    Database: kurikulum
-- ------------------------------------------------------
-- Server version	5.6.25

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Current Database: `kurikulum`
--

CREATE DATABASE /*!32312 IF NOT EXISTS*/ `kurikulum` /*!40100 DEFAULT CHARACTER SET utf8 */;

USE `kurikulum`;

--
-- Table structure for table `articles`
--

DROP TABLE IF EXISTS `articles`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `articles` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `headline` varchar(255) DEFAULT '',
  `rewrite_url` varchar(255) DEFAULT NULL,
  `perex` text,
  `text` text,
  `keywords` varchar(255) DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  `categoryid` int(11) unsigned NOT NULL,
  `visibility` tinyint(3) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `categoryid->categories.id` (`categoryid`),
  CONSTRAINT `categoryid->categories.id` FOREIGN KEY (`categoryid`) REFERENCES `categories` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `articles`
--

LOCK TABLES `articles` WRITE;
/*!40000 ALTER TABLE `articles` DISABLE KEYS */;
INSERT INTO `articles` VALUES (1,'Toto je titulek clanku','titulek1','Toto je zkraceny uryvek clanku...','Toto jsou samotna data clanku!','klicove slovo 1, klicoveslovo2, klicoveslov3','This is description',1,1),(2,'Toto je titulek clanku','titulek2','Toto je zkraceny uryvek clanku...','Toto jsou samotna data clanku!','klicove slovo 1, klicoveslovo2, klicoveslov3','This is description',1,0),(3,'Toto je titulek clanku','titulek3','Toto je zkraceny uryvek clanku...','Toto jsou samotna data clanku!','klicove slovo 1, klicoveslovo2, klicoveslov3','This is description',1,0),(4,'Toto je titulek clanku',NULL,'Toto je zkraceny uryvek clanku...','Toto jsou samotna data clanku!','klicove slovo 1, klicoveslovo2, klicoveslov3','This is description',1,0),(5,'Toto je titulek clanku',NULL,'Toto je zkraceny uryvek clanku...','Toto jsou samotna data clanku!','klicove slovo 1, klicoveslovo2, klicoveslov3','This is description',1,0),(6,'Toto je titulek clanku',NULL,'Toto je zkraceny uryvek clanku...','Toto jsou samotna data clanku!','klicove slovo 1, klicoveslovo2, klicoveslov3','This is description',1,0),(7,'Toto je titulek clanku',NULL,'Toto je zkraceny uryvek clanku...','Toto jsou samotna data clanku!','klicove slovo 1, klicoveslovo2, klicoveslov3','This is description',1,0),(8,'Toto je titulek clanku',NULL,'Toto je zkraceny uryvek clanku...','Toto jsou samotna data clanku!','klicove slovo 1, klicoveslovo2, klicoveslov3','This is description',1,0),(9,'Toto je titulek clanku',NULL,'Toto je zkraceny uryvek clanku...','Toto jsou samotna data clanku!','klicove slovo 1, klicoveslovo2, klicoveslov3','This is description',1,0);
/*!40000 ALTER TABLE `articles` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `authlogs`
--

DROP TABLE IF EXISTS `authlogs`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `authlogs` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `userid` int(11) unsigned NOT NULL DEFAULT '0',
  `ts` datetime DEFAULT NULL,
  `httpUserAgent` varchar(255) DEFAULT NULL,
  `requestMethod` varchar(10) DEFAULT NULL,
  `username` varchar(255) DEFAULT NULL,
  `success` tinyint(1) unsigned DEFAULT NULL,
  `httpClientHost` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `authlogs.userid->users.id` (`userid`)
) ENGINE=InnoDB AUTO_INCREMENT=55 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `authlogs`
--

LOCK TABLES `authlogs` WRITE;
/*!40000 ALTER TABLE `authlogs` DISABLE KEYS */;
INSERT INTO `authlogs` VALUES (1,16,'0000-00-00 00:00:00','afdasfa','GET','adfasf',1,'fasdfdsads'),(2,0,'2015-06-21 14:41:14','Mozilla/5.0 (Macintosh; Intel Mac OS X 10_10_4) AppleWebKit/600.7.7 (KHTML, like Gecko) Version/8.0.7 Safari/600.7.7','POST','a',0,'192.168.33.1'),(3,0,'2015-06-21 14:41:31','Mozilla/5.0 (Macintosh; Intel Mac OS X 10_10_4) AppleWebKit/600.7.7 (KHTML, like Gecko) Version/8.0.7 Safari/600.7.7','POST','a',0,'192.168.33.1'),(4,0,'2015-06-21 14:42:40','Mozilla/5.0 (Macintosh; Intel Mac OS X 10_10_4) AppleWebKit/600.7.7 (KHTML, like Gecko) Version/8.0.7 Safari/600.7.7','POST','a',0,'192.168.33.1'),(5,0,'2015-06-21 14:43:17','Mozilla/5.0 (Macintosh; Intel Mac OS X 10_10_4) AppleWebKit/600.7.7 (KHTML, like Gecko) Version/8.0.7 Safari/600.7.7','POST','aaa',0,'192.168.33.1'),(6,0,'2015-06-21 14:43:31','Mozilla/5.0 (Macintosh; Intel Mac OS X 10_10_4) AppleWebKit/600.7.7 (KHTML, like Gecko) Version/8.0.7 Safari/600.7.7','POST','aaa',0,'192.168.33.1'),(7,0,'2015-06-21 14:43:43','Mozilla/5.0 (Macintosh; Intel Mac OS X 10_10_4) AppleWebKit/600.7.7 (KHTML, like Gecko) Version/8.0.7 Safari/600.7.7','POST','asfas@sasd.cz',0,'192.168.33.1'),(8,26,'2015-06-21 14:47:56','Mozilla/5.0 (Macintosh; Intel Mac OS X 10_10_4) AppleWebKit/600.7.7 (KHTML, like Gecko) Version/8.0.7 Safari/600.7.7','POST','asfas@sasd.cz',0,'192.168.33.1'),(9,26,'2015-06-21 14:48:06','Mozilla/5.0 (Macintosh; Intel Mac OS X 10_10_4) AppleWebKit/600.7.7 (KHTML, like Gecko) Version/8.0.7 Safari/600.7.7','POST','asfas@sasd.cz',0,'192.168.33.1'),(10,0,'2015-06-21 14:56:56','Mozilla/5.0 (Macintosh; Intel Mac OS X 10_10_4) AppleWebKit/600.7.7 (KHTML, like Gecko) Version/8.0.7 Safari/600.7.7','POST','test@test.cz',1,'192.168.33.1'),(11,0,'2015-06-21 14:57:13','Mozilla/5.0 (Macintosh; Intel Mac OS X 10_10_4) AppleWebKit/600.7.7 (KHTML, like Gecko) Version/8.0.7 Safari/600.7.7','POST','test@test.cz',0,'192.168.33.1'),(12,0,'2015-06-21 14:57:37','Mozilla/5.0 (Macintosh; Intel Mac OS X 10_10_4) AppleWebKit/600.7.7 (KHTML, like Gecko) Version/8.0.7 Safari/600.7.7','POST','test@test.cz',0,'192.168.33.1'),(13,0,'2015-06-21 15:54:47','Mozilla/5.0 (Macintosh; Intel Mac OS X 10_10_4) AppleWebKit/600.7.7 (KHTML, like Gecko) Version/8.0.7 Safari/600.7.7','POST','test@test.cz',1,'192.168.33.1'),(14,0,'2015-06-21 16:01:54','Mozilla/5.0 (Macintosh; Intel Mac OS X 10_10_4) AppleWebKit/600.7.7 (KHTML, like Gecko) Version/8.0.7 Safari/600.7.7','POST','test@test.cz',1,'192.168.33.1'),(15,0,'2015-06-21 17:17:31','Mozilla/5.0 (Macintosh; Intel Mac OS X 10_10_4) AppleWebKit/600.7.7 (KHTML, like Gecko) Version/8.0.7 Safari/600.7.7','POST','test@test.cz',1,'192.168.33.1'),(16,0,'2015-06-30 13:24:21','Mozilla/5.0 (Macintosh; Intel Mac OS X 10_10_4) AppleWebKit/600.7.7 (KHTML, like Gecko) Version/8.0.7 Safari/600.7.7','POST','test@test.cz',1,'192.168.33.1'),(17,0,'2015-06-30 16:07:26','Mozilla/5.0 (Macintosh; Intel Mac OS X 10_10_4) AppleWebKit/600.7.7 (KHTML, like Gecko) Version/8.0.7 Safari/600.7.7','POST','test@test.cz',1,'192.168.33.1'),(18,0,'2015-06-30 16:09:52','Mozilla/5.0 (Macintosh; Intel Mac OS X 10_10_4) AppleWebKit/600.7.7 (KHTML, like Gecko) Version/8.0.7 Safari/600.7.7','POST','test@test.cz',1,'192.168.33.1'),(19,0,'2015-06-30 16:11:09','Mozilla/5.0 (Macintosh; Intel Mac OS X 10_10_4) AppleWebKit/600.7.7 (KHTML, like Gecko) Version/8.0.7 Safari/600.7.7','POST','test@test.cz',1,'192.168.33.1'),(20,0,'2015-06-30 16:16:17','Mozilla/5.0 (Macintosh; Intel Mac OS X 10_10_4) AppleWebKit/600.7.7 (KHTML, like Gecko) Version/8.0.7 Safari/600.7.7','POST','test@test',0,'192.168.33.1'),(21,0,'2015-06-30 16:18:02','Mozilla/5.0 (Macintosh; Intel Mac OS X 10_10_4) AppleWebKit/600.7.7 (KHTML, like Gecko) Version/8.0.7 Safari/600.7.7','POST','adfadsf',0,'192.168.33.1'),(22,0,'2015-06-30 16:18:16','Mozilla/5.0 (Macintosh; Intel Mac OS X 10_10_4) AppleWebKit/600.7.7 (KHTML, like Gecko) Version/8.0.7 Safari/600.7.7','POST','adfadsf',0,'192.168.33.1'),(23,0,'2015-06-30 16:18:18','Mozilla/5.0 (Macintosh; Intel Mac OS X 10_10_4) AppleWebKit/600.7.7 (KHTML, like Gecko) Version/8.0.7 Safari/600.7.7','POST','adfadsf',0,'192.168.33.1'),(24,0,'2015-06-30 16:18:23','Mozilla/5.0 (Macintosh; Intel Mac OS X 10_10_4) AppleWebKit/600.7.7 (KHTML, like Gecko) Version/8.0.7 Safari/600.7.7','POST','adfadsf',0,'192.168.33.1'),(25,0,'2015-06-30 16:18:30','Mozilla/5.0 (Macintosh; Intel Mac OS X 10_10_4) AppleWebKit/600.7.7 (KHTML, like Gecko) Version/8.0.7 Safari/600.7.7','POST','adfadsf',0,'192.168.33.1'),(26,0,'2015-06-30 16:18:38','Mozilla/5.0 (Macintosh; Intel Mac OS X 10_10_4) AppleWebKit/600.7.7 (KHTML, like Gecko) Version/8.0.7 Safari/600.7.7','POST','adfadsf',0,'192.168.33.1'),(27,0,'2015-06-30 16:18:44','Mozilla/5.0 (Macintosh; Intel Mac OS X 10_10_4) AppleWebKit/600.7.7 (KHTML, like Gecko) Version/8.0.7 Safari/600.7.7','POST','adfadsf',0,'192.168.33.1'),(28,0,'2015-06-30 16:18:48','Mozilla/5.0 (Macintosh; Intel Mac OS X 10_10_4) AppleWebKit/600.7.7 (KHTML, like Gecko) Version/8.0.7 Safari/600.7.7','POST','adfadsf',0,'192.168.33.1'),(29,0,'2015-06-30 16:18:55','Mozilla/5.0 (Macintosh; Intel Mac OS X 10_10_4) AppleWebKit/600.7.7 (KHTML, like Gecko) Version/8.0.7 Safari/600.7.7','POST','adfadsf',0,'192.168.33.1'),(30,0,'2015-06-30 16:18:56','Mozilla/5.0 (Macintosh; Intel Mac OS X 10_10_4) AppleWebKit/600.7.7 (KHTML, like Gecko) Version/8.0.7 Safari/600.7.7','POST','adfadsf',0,'192.168.33.1'),(31,0,'2015-06-30 16:19:45','Mozilla/5.0 (Macintosh; Intel Mac OS X 10_10_4) AppleWebKit/600.7.7 (KHTML, like Gecko) Version/8.0.7 Safari/600.7.7','POST','test@test.cz',1,'192.168.33.1'),(32,0,'2015-06-30 18:34:49','Mozilla/5.0 (Macintosh; Intel Mac OS X 10_10_4) AppleWebKit/600.7.7 (KHTML, like Gecko) Version/8.0.7 Safari/600.7.7','POST','test@test.cz',1,'192.168.33.1'),(34,0,'2015-06-30 18:36:57','Mozilla/5.0 (Macintosh; Intel Mac OS X 10_10_4) AppleWebKit/600.7.7 (KHTML, like Gecko) Version/8.0.7 Safari/600.7.7','POST','test@test.cz',1,'192.168.33.1'),(35,0,'2015-07-05 10:08:08','Mozilla/5.0 (Macintosh; Intel Mac OS X 10_10_4) AppleWebKit/600.7.12 (KHTML, like Gecko) Version/8.0.7 Safari/600.7.12','POST','test@test.cz',1,'192.168.33.1'),(36,0,'2015-07-05 12:27:10','Mozilla/5.0 (Macintosh; Intel Mac OS X 10_10_4) AppleWebKit/600.7.12 (KHTML, like Gecko) Version/8.0.7 Safari/600.7.12','POST','test@test.cz',1,'192.168.33.1'),(37,0,'2015-07-09 19:59:36','Mozilla/5.0 (Macintosh; Intel Mac OS X 10_10_4) AppleWebKit/600.7.12 (KHTML, like Gecko) Version/8.0.7 Safari/600.7.12','POST','test@test.cz',1,'192.168.33.1'),(38,0,'2015-07-10 11:51:50','Mozilla/5.0 (Macintosh; Intel Mac OS X 10_10_4) AppleWebKit/600.7.12 (KHTML, like Gecko) Version/8.0.7 Safari/600.7.12','POST','test@test.cz',1,'192.168.33.1'),(39,0,'2015-07-10 15:23:01','Mozilla/5.0 (Macintosh; Intel Mac OS X 10_10_4) AppleWebKit/600.7.12 (KHTML, like Gecko) Version/8.0.7 Safari/600.7.12','POST','test@test.cz',1,'192.168.33.1'),(40,0,'2015-07-10 15:23:54','Mozilla/5.0 (Macintosh; Intel Mac OS X 10_10_4) AppleWebKit/600.7.12 (KHTML, like Gecko) Version/8.0.7 Safari/600.7.12','POST','test@test.cz',1,'192.168.33.1'),(41,0,'2015-07-13 18:38:20','Mozilla/5.0 (Macintosh; Intel Mac OS X 10_10_4) AppleWebKit/600.7.12 (KHTML, like Gecko) Version/8.0.7 Safari/600.7.12','POST','test@test.cz',1,'192.168.33.1'),(42,0,'2015-07-19 13:25:39','Mozilla/5.0 (Macintosh; Intel Mac OS X 10_10_4) AppleWebKit/600.7.12 (KHTML, like Gecko) Version/8.0.7 Safari/600.7.12','POST','test@test.cz',1,'192.168.33.1'),(43,0,'2015-07-19 15:46:26','Mozilla/5.0 (Macintosh; Intel Mac OS X 10_10_4) AppleWebKit/600.7.12 (KHTML, like Gecko) Version/8.0.7 Safari/600.7.12','POST','test@test.cz',1,'192.168.33.1'),(44,0,'2015-07-21 17:27:55','Mozilla/5.0 (Macintosh; Intel Mac OS X 10_10_4) AppleWebKit/600.7.12 (KHTML, like Gecko) Version/8.0.7 Safari/600.7.12','POST','test@test.cz',1,'192.168.33.1'),(45,0,'2015-07-24 12:34:33','Mozilla/5.0 (Macintosh; Intel Mac OS X 10_10_4) AppleWebKit/600.7.12 (KHTML, like Gecko) Version/8.0.7 Safari/600.7.12','POST','test@test.cz',1,'192.168.33.1'),(46,0,'2015-07-24 12:44:47','Mozilla/5.0 (Macintosh; Intel Mac OS X 10_10_4) AppleWebKit/600.7.12 (KHTML, like Gecko) Version/8.0.7 Safari/600.7.12','POST','test@test.cz',1,'192.168.33.1'),(47,0,'2015-07-29 19:08:57','Mozilla/5.0 (Macintosh; Intel Mac OS X 10_10_4) AppleWebKit/600.7.12 (KHTML, like Gecko) Version/8.0.7 Safari/600.7.12','POST','test@test.cz',1,'192.168.33.1'),(48,0,'2015-07-31 20:45:06','Mozilla/5.0 (Macintosh; Intel Mac OS X 10_10_4) AppleWebKit/600.7.12 (KHTML, like Gecko) Version/8.0.7 Safari/600.7.12','POST','test@test.cz',1,'192.168.33.1'),(49,0,'2015-08-01 11:37:24','Mozilla/5.0 (Macintosh; Intel Mac OS X 10_10_4) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/44.0.2403.125 Safari/537.36','POST','test@test.cz',1,'192.168.33.1'),(50,0,'2015-08-02 17:53:10','Mozilla/5.0 (Macintosh; Intel Mac OS X 10_10_4) AppleWebKit/600.7.12 (KHTML, like Gecko) Version/8.0.7 Safari/600.7.12','POST','test@test.cz',1,'192.168.33.1'),(51,0,'2015-08-03 16:23:04','Mozilla/5.0 (Macintosh; Intel Mac OS X 10_10_4) AppleWebKit/600.7.12 (KHTML, like Gecko) Version/8.0.7 Safari/600.7.12','POST','test@test.cz',1,'192.168.33.1'),(52,0,'2015-08-04 14:30:07','Mozilla/5.0 (Macintosh; Intel Mac OS X 10_10_4) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/44.0.2403.125 Safari/537.36','POST','test@test.cz',1,'192.168.33.1'),(53,0,'2015-08-04 15:23:12','Mozilla/5.0 (Macintosh; Intel Mac OS X 10_10_4) AppleWebKit/600.7.12 (KHTML, like Gecko) Version/8.0.7 Safari/600.7.12','POST','test@test.cz',1,'192.168.33.1'),(54,0,'2015-09-06 14:59:28','Mozilla/5.0 (Macintosh; Intel Mac OS X 10_10_5) AppleWebKit/600.8.9 (KHTML, like Gecko) Version/8.0.8 Safari/600.8.9','POST','test@test.cz',1,'192.168.33.1');
/*!40000 ALTER TABLE `authlogs` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `categories`
--

DROP TABLE IF EXISTS `categories`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `categories` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `display_name` varchar(255) DEFAULT '',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `categories`
--

LOCK TABLES `categories` WRITE;
/*!40000 ALTER TABLE `categories` DISABLE KEYS */;
INSERT INTO `categories` VALUES (1,'test','Testovaci kategorie');
/*!40000 ALTER TABLE `categories` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cv_assets_certification`
--

DROP TABLE IF EXISTS `cv_assets_certification`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cv_assets_certification` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `configid` int(11) unsigned NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `school` varchar(255) DEFAULT NULL,
  `year` int(4) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `cv_assets_certification.configid` (`configid`),
  CONSTRAINT `cv_assets_certification.configid` FOREIGN KEY (`configid`) REFERENCES `cv_configurations` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cv_assets_certification`
--

LOCK TABLES `cv_assets_certification` WRITE;
/*!40000 ALTER TABLE `cv_assets_certification` DISABLE KEYS */;
INSERT INTO `cv_assets_certification` VALUES (6,7,'asdfasf','asdfasfd',1234),(7,8,'aaaaa','aaaaa',1111),(8,8,'bbbbbb','bbbbb',2222);
/*!40000 ALTER TABLE `cv_assets_certification` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cv_assets_education`
--

DROP TABLE IF EXISTS `cv_assets_education`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cv_assets_education` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `startYear` int(11) unsigned DEFAULT NULL,
  `endYear` int(11) unsigned DEFAULT NULL,
  `school` varchar(255) DEFAULT NULL,
  `degree` varchar(255) DEFAULT NULL,
  `title` varchar(255) DEFAULT NULL,
  `configid` int(11) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `cv_assets_education.configid` (`configid`),
  CONSTRAINT `cv_assets_education.configid` FOREIGN KEY (`configid`) REFERENCES `cv_configurations` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cv_assets_education`
--

LOCK TABLES `cv_assets_education` WRITE;
/*!40000 ALTER TABLE `cv_assets_education` DISABLE KEYS */;
INSERT INTO `cv_assets_education` VALUES (10,1111,1111,'111111','Základní','111111',7),(11,1111,1111,'111111','Základní','111111',7),(12,1111,1111,'bbbb','Střední (maturita)','bbbb',8),(13,2222,2222,'cccc','Jiné','cccc',8);
/*!40000 ALTER TABLE `cv_assets_education` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cv_assets_language`
--

DROP TABLE IF EXISTS `cv_assets_language`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cv_assets_language` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `configid` int(11) unsigned DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `level` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `cv_assets_language.configid` (`configid`),
  CONSTRAINT `cv_assets_language.configid` FOREIGN KEY (`configid`) REFERENCES `cv_configurations` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cv_assets_language`
--

LOCK TABLES `cv_assets_language` WRITE;
/*!40000 ALTER TABLE `cv_assets_language` DISABLE KEYS */;
INSERT INTO `cv_assets_language` VALUES (6,7,'vvvv','B2'),(7,8,'aaaaa','C2'),(8,8,'bbbbbb','A1');
/*!40000 ALTER TABLE `cv_assets_language` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cv_assets_skills`
--

DROP TABLE IF EXISTS `cv_assets_skills`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cv_assets_skills` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `configid` int(11) unsigned DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `level` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `cv_asset_skills.configid` (`configid`),
  CONSTRAINT `cv_asset_skills.configid` FOREIGN KEY (`configid`) REFERENCES `cv_configurations` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cv_assets_skills`
--

LOCK TABLES `cv_assets_skills` WRITE;
/*!40000 ALTER TABLE `cv_assets_skills` DISABLE KEYS */;
INSERT INTO `cv_assets_skills` VALUES (8,7,'asdfasdf','Expertní'),(9,8,'aaaaaa','Mírně pokročilá znalost'),(10,8,'bbbbb','Pokročilá znalost');
/*!40000 ALTER TABLE `cv_assets_skills` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cv_assets_work`
--

DROP TABLE IF EXISTS `cv_assets_work`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cv_assets_work` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `startYear` int(4) unsigned DEFAULT NULL,
  `endYear` int(4) unsigned DEFAULT NULL,
  `configid` int(11) unsigned NOT NULL,
  `employer` varchar(255) DEFAULT NULL,
  `description` text,
  `position` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `cv_assets_work` (`configid`),
  CONSTRAINT `cv_assets_work` FOREIGN KEY (`configid`) REFERENCES `cv_configurations` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cv_assets_work`
--

LOCK TABLES `cv_assets_work` WRITE;
/*!40000 ALTER TABLE `cv_assets_work` DISABLE KEYS */;
INSERT INTO `cv_assets_work` VALUES (9,1222,1111,7,'asfdffd','','sdddd'),(10,1222,1111,7,'asfdffd','','sdddd'),(11,1111,1111,8,'aaaaa','vvvvv','aaaaa aaaaa'),(12,2222,2222,8,'bbbbbb','bbbbbb','bbbb ');
/*!40000 ALTER TABLE `cv_assets_work` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cv_configurations`
--

DROP TABLE IF EXISTS `cv_configurations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cv_configurations` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `userid` int(11) unsigned DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `graphics` int(4) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `cv_configurations` (`userid`),
  CONSTRAINT `cv_configurations` FOREIGN KEY (`userid`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cv_configurations`
--

LOCK TABLES `cv_configurations` WRITE;
/*!40000 ALTER TABLE `cv_configurations` DISABLE KEYS */;
INSERT INTO `cv_configurations` VALUES (7,16,'asfdfsafdafasdfsaf',5),(8,16,'11111',1);
/*!40000 ALTER TABLE `cv_configurations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `errorMessages`
--

DROP TABLE IF EXISTS `errorMessages`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `errorMessages` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `text` varchar(255) NOT NULL DEFAULT '',
  `type` varchar(255) NOT NULL DEFAULT '',
  `class` varchar(255) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `errorMessages`
--

LOCK TABLES `errorMessages` WRITE;
/*!40000 ALTER TABLE `errorMessages` DISABLE KEYS */;
/*!40000 ALTER TABLE `errorMessages` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `noticeMessages`
--

DROP TABLE IF EXISTS `noticeMessages`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `noticeMessages` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `text` varchar(255) NOT NULL DEFAULT '',
  `type` varchar(255) NOT NULL DEFAULT '',
  `class` varchar(255) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `noticeMessages`
--

LOCK TABLES `noticeMessages` WRITE;
/*!40000 ALTER TABLE `noticeMessages` DISABLE KEYS */;
/*!40000 ALTER TABLE `noticeMessages` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `passwordTokens`
--

DROP TABLE IF EXISTS `passwordTokens`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `passwordTokens` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `userid` int(11) unsigned DEFAULT NULL,
  `token` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `passwordTokens.userid->users.id` (`userid`),
  CONSTRAINT `passwordTokens.userid->users.id` FOREIGN KEY (`userid`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=44 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `passwordTokens`
--

LOCK TABLES `passwordTokens` WRITE;
/*!40000 ALTER TABLE `passwordTokens` DISABLE KEYS */;
INSERT INTO `passwordTokens` VALUES (42,15,'d0fb6bd4319ff0b10661a3aaf6f81abb'),(43,15,'afc7407a7ca35e22b9f1893367fcb919');
/*!40000 ALTER TABLE `passwordTokens` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `payments`
--

DROP TABLE IF EXISTS `payments`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `payments` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `from` datetime DEFAULT NULL,
  `to` datetime DEFAULT NULL,
  `amount` int(11) unsigned DEFAULT NULL,
  `userid` int(11) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `payments.userid` (`userid`),
  CONSTRAINT `payments.userid` FOREIGN KEY (`userid`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=22 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `payments`
--

LOCK TABLES `payments` WRITE;
/*!40000 ALTER TABLE `payments` DISABLE KEYS */;
INSERT INTO `payments` VALUES (21,'2015-07-10 00:00:00','2015-08-09 00:00:00',200,16);
/*!40000 ALTER TABLE `payments` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `profile_certification`
--

DROP TABLE IF EXISTS `profile_certification`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `profile_certification` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `school` varchar(255) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `userid` int(11) unsigned DEFAULT NULL,
  `year` int(4) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `profile_certification.userid` (`userid`),
  CONSTRAINT `profile_certification.userid` FOREIGN KEY (`userid`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `profile_certification`
--

LOCK TABLES `profile_certification` WRITE;
/*!40000 ALTER TABLE `profile_certification` DISABLE KEYS */;
INSERT INTO `profile_certification` VALUES (7,'asfasdf','asdfafds',16,1234);
/*!40000 ALTER TABLE `profile_certification` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `profile_education`
--

DROP TABLE IF EXISTS `profile_education`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `profile_education` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `startYear` int(4) DEFAULT NULL,
  `endYear` int(4) DEFAULT NULL,
  `school` varchar(255) DEFAULT NULL,
  `degree` varchar(30) DEFAULT NULL,
  `title` varchar(10) DEFAULT NULL,
  `userid` int(11) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `profile_education_userid` (`userid`),
  CONSTRAINT `profile_education_userid` FOREIGN KEY (`userid`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `profile_education`
--

LOCK TABLES `profile_education` WRITE;
/*!40000 ALTER TABLE `profile_education` DISABLE KEYS */;
INSERT INTO `profile_education` VALUES (2,1231,1222,'asfaa','Základní','adfsafa',16);
/*!40000 ALTER TABLE `profile_education` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `profile_language`
--

DROP TABLE IF EXISTS `profile_language`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `profile_language` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `level` varchar(20) DEFAULT NULL,
  `userid` int(11) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `profile_language.userid` (`userid`),
  CONSTRAINT `profile_language.userid` FOREIGN KEY (`userid`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `profile_language`
--

LOCK TABLES `profile_language` WRITE;
/*!40000 ALTER TABLE `profile_language` DISABLE KEYS */;
INSERT INTO `profile_language` VALUES (2,'asdfgghgf','A1',16),(3,'afdafsd','A1',16);
/*!40000 ALTER TABLE `profile_language` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `profile_skills`
--

DROP TABLE IF EXISTS `profile_skills`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `profile_skills` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `level` varchar(255) DEFAULT NULL,
  `userid` int(11) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `profile_skills.userid` (`userid`),
  CONSTRAINT `profile_skills.userid` FOREIGN KEY (`userid`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `profile_skills`
--

LOCK TABLES `profile_skills` WRITE;
/*!40000 ALTER TABLE `profile_skills` DISABLE KEYS */;
INSERT INTO `profile_skills` VALUES (1,'jhggfg ghjghgj','Pokročilá',16);
/*!40000 ALTER TABLE `profile_skills` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `profile_work`
--

DROP TABLE IF EXISTS `profile_work`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `profile_work` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `startYear` int(11) unsigned DEFAULT NULL,
  `endYear` int(11) unsigned DEFAULT NULL,
  `employer` varchar(255) DEFAULT NULL,
  `position` varchar(255) DEFAULT NULL,
  `description` text,
  `userid` int(11) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `profile_work.userid` (`userid`),
  CONSTRAINT `profile_work.userid` FOREIGN KEY (`userid`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `profile_work`
--

LOCK TABLES `profile_work` WRITE;
/*!40000 ALTER TABLE `profile_work` DISABLE KEYS */;
INSERT INTO `profile_work` VALUES (1,1234,1234,'adfsdf','asdfasfd','asfaf',16);
/*!40000 ALTER TABLE `profile_work` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `registrations`
--

DROP TABLE IF EXISTS `registrations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `registrations` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `email` varchar(255) DEFAULT '',
  `token` varchar(255) DEFAULT '',
  `firstName` varchar(255) DEFAULT NULL,
  `lastName` varchar(255) DEFAULT NULL,
  `address` varchar(255) DEFAULT NULL,
  `address2` varchar(255) DEFAULT NULL,
  `zipCode` int(6) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  `telephone` varchar(20) DEFAULT NULL,
  `registeredSince` datetime DEFAULT NULL,
  `city` varchar(50) DEFAULT NULL,
  `blocked` tinyint(1) NOT NULL DEFAULT '0',
  `userlevel` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `registrations`
--

LOCK TABLES `registrations` WRITE;
/*!40000 ALTER TABLE `registrations` DISABLE KEYS */;
INSERT INTO `registrations` VALUES (1,'test@test.cc','2d3a3a4597a2c34fbf4378e3f4d78625','Jmeno','Prijmeni','Address 1','Address 2',12345,'test','+420123456789',NULL,'Brno',0,0);
/*!40000 ALTER TABLE `registrations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `static_pages`
--

DROP TABLE IF EXISTS `static_pages`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `static_pages` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT '',
  `access` tinyint(1) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `static_pages`
--

LOCK TABLES `static_pages` WRITE;
/*!40000 ALTER TABLE `static_pages` DISABLE KEYS */;
INSERT INTO `static_pages` VALUES (1,'index',1);
/*!40000 ALTER TABLE `static_pages` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `static_pages_texts`
--

DROP TABLE IF EXISTS `static_pages_texts`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `static_pages_texts` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `pageid` int(11) unsigned NOT NULL,
  `key` varchar(255) DEFAULT NULL,
  `value` text,
  PRIMARY KEY (`id`),
  KEY `static_pages_texts.pageid` (`pageid`),
  CONSTRAINT `static_pages_texts.pageid` FOREIGN KEY (`pageid`) REFERENCES `static_pages` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `static_pages_texts`
--

LOCK TABLES `static_pages_texts` WRITE;
/*!40000 ALTER TABLE `static_pages_texts` DISABLE KEYS */;
INSERT INTO `static_pages_texts` VALUES (1,1,'header','CAREERWAY'),(2,1,'header2','This is header 2'),(3,1,'sub','Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Vivamus luctus egestas leo. Suspendisse nisl. Maecenas fermentum, sem in pharetra pellentesque, velit turpis volutpat ante, in pharetra metus odio a lectus. Cras pede libero, dapibus nec, pretium sit amet, tempor quis. </p>\n\n<p> Morbi scelerisque luctus velit. Pellentesque pretium lectus id turpis. Praesent in mauris eu tortor porttitor accumsan. Et harum quidem rerum facilis est et expedita distinctio. Donec ipsum massa, ullamcorper in, auctor et, scelerisque sed, est. Nullam rhoncus aliquam metus. Vivamus luctus egestas leo. Morbi leo mi, nonummy eget tristique non, rhoncus non leo. Integer lacinia. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Vivamus porttitor turpis ac leo. Cras pede libero, dapibus nec, pretium sit amet, tempor quis.'),(4,1,'sub2','Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Vivamus luctus egestas leo. Suspendisse nisl. Maecenas fermentum, sem in pharetra pellentesque, velit turpis volutpat ante, in pharetra metus odio a lectus. '),(5,1,'main_text','Pellentesque sapien. Duis bibendum, lectus ut viverra rhoncus, dolor nunc faucibus libero, eget facilisis enim ipsum id lacus. Morbi imperdiet, mauris ac auctor dictum, nisl ligula egestas nulla, et sollicitudin sem purus in lacus. Nunc tincidunt ante vitae massa. Praesent vitae arcu tempor neque lacinia pretium. Praesent dapibus. Nam libero tempore, cum soluta nobis est eligendi optio cumque nihil impedit quo minus id quod maxime placeat facere possimus, omnis voluptas assumenda est, omnis dolor repellendus. Etiam neque. Fusce tellus. Donec quis nibh at felis congue commodo. Nunc auctor. </p>\n\n<p> Nam quis nulla. Donec iaculis gravida nulla. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Nulla non arcu lacinia neque faucibus fringilla. Sed convallis magna eu sem. Praesent in mauris eu tortor porttitor accumsan. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Fusce consectetuer risus a nunc. Nullam eget nisl. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos hymenaeos. Cras elementum. Nunc tincidunt ante vitae massa. </p>\n\n<p> Aenean fermentum risus id tortor. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos hymenaeos. Aenean vel massa quis mauris vehicula lacinia. Aenean id metus id velit ullamcorper pulvinar. Integer vulputate sem a nibh rutrum consequat. Nullam lectus justo, vulputate eget mollis sed, tempor sed magna. Nam quis nulla. Aliquam erat volutpat. In convallis. Fusce aliquam vestibulum ipsum. Aliquam erat volutpat. Phasellus rhoncus. Fusce wisi. Nunc dapibus tortor vel mi dapibus sollicitudin. Curabitur ligula sapien, pulvinar a vestibulum quis, facilisis vel sapien. Cras elementum. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Maecenas libero. Maecenas lorem. </p>\n\n<p> Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Fusce nibh. Etiam quis quam. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Proin pede metus, vulputate nec, fermentum fringilla, vehicula vitae, justo. Aenean id metus id velit ullamcorper pulvinar. Phasellus faucibus molestie nisl. Integer imperdiet lectus quis justo. Donec iaculis gravida nulla. Morbi imperdiet, mauris ac auctor dictum, nisl ligula egestas nulla, et sollicitudin sem purus in lacus. </p>');
/*!40000 ALTER TABLE `static_pages_texts` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `successMessages`
--

DROP TABLE IF EXISTS `successMessages`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `successMessages` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `text` varchar(255) NOT NULL DEFAULT '',
  `type` varchar(255) NOT NULL DEFAULT '',
  `class` varchar(255) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `successMessages`
--

LOCK TABLES `successMessages` WRITE;
/*!40000 ALTER TABLE `successMessages` DISABLE KEYS */;
INSERT INTO `successMessages` VALUES (1,'Toto je testovaci zprava zobrazovana pri uspechu','ok','ok');
/*!40000 ALTER TABLE `successMessages` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `firstName` varchar(255) DEFAULT NULL,
  `lastName` varchar(255) DEFAULT NULL,
  `registeredSince` datetime DEFAULT NULL,
  `address` varchar(255) DEFAULT NULL,
  `address2` varchar(255) DEFAULT NULL,
  `zipCode` int(6) unsigned NOT NULL,
  `password` varchar(255) DEFAULT NULL,
  `userlevel` int(1) unsigned NOT NULL DEFAULT '0',
  `email` varchar(255) NOT NULL DEFAULT '',
  `telephone` varchar(20) DEFAULT NULL,
  `blocked` int(1) unsigned NOT NULL DEFAULT '0',
  `city` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=27 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES (15,'atestatsa','testadsafa',NULL,'asdfsafa dfs',NULL,12345,'6655a82577604ddb635c7ee1b86ab4ece0a0f06b',0,'&lt;skfsjsadfsak&gt;///?&gt;',NULL,0,NULL),(16,'Účet','Testovací','2015-05-11 13:33:37','SomeKindOfAddress','',12300,'a94a8fe5ccb19ba61c4c0873d391e987982fbbd3',0,'test@test.cz','+42012345689',0,'SomeCity'),(17,NULL,NULL,NULL,NULL,NULL,0,NULL,0,'',NULL,0,NULL),(18,NULL,NULL,NULL,NULL,NULL,0,NULL,0,'',NULL,0,NULL),(19,'Jmeno','Prijmeni',NULL,'Address 1','Address 2',12345,'test',0,'test@test.ac','+420123456789',0,'Brno'),(20,'Jmeno','Prijmeni',NULL,'Address 1','Address 2',12345,'test',0,'test@test.ac','+420123456789',0,'Brno'),(21,'Jmeno','Prijmeni',NULL,'Address 1','Address 2',12345,'test',0,'test@test.ac','+420123456789',0,'Brno'),(22,'Jmeno','Prijmeni','2015-05-11 13:33:37','Address 1','Address 2',12345,'a94a8fe5ccb19ba61c4c0873d391e987982fbbd3',0,'test@test.aa','+420123456789',0,'Brno'),(23,'Jmeno','Prijmeni','2015-05-11 13:33:37','Address 1','Address 2',12345,'a94a8fe5ccb19ba61c4c0873d391e987982fbbd3',0,'test@test.aa','+420123456789',0,'Brno'),(24,'Jmeno','Prijmeni','2015-05-11 13:33:37','Address 1','Address 2',12345,'a94a8fe5ccb19ba61c4c0873d391e987982fbbd3',0,'test@test.aa','+420123456789',0,'Brno'),(25,'Jmeno','Prijmeni',NULL,'Address 1','Address 2',12345,'test',0,'test@test.ac','+420123456789',0,'Brno'),(26,'aaa','aa','2015-06-16 19:49:05','aaa',NULL,12345,'e0c9035898dd52fc65c41454cec9c4d2611bfb37',0,'asfas@sasd.cz','12345',0,NULL);
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2015-09-06 13:06:18
