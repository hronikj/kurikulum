<?php
	require_once('./vendor/autoload.php'); // composer autoload
	$init = new Init();

	/* Routing */
	$router = new Router();
	$requested_uri = $_SERVER['REQUEST_URI'];

	$router -> parseParameters(array($requested_uri));

	/* autoloader
	   searches in CLASS_PATH and if not found then in CONTROLLER_PATH */
	/*
	function __autoload($class) {
		if (file_exists(CLASS_PATH . $class . '.php'))
			require_once(CLASS_PATH . $class . '.php');
		elseif (file_exists(CONTROLLER_PATH . $class . '.php'))
			require_once(CONTROLLER_PATH . $class . '.php');
		else die('<p class="error"> Soubor třídy s názvem: <u>' . $class .  '</u> neexistuje! </p>');
	} */

	/* SMARTY autoload registration
	define('SMARTY_SPL_AUTOLOAD', 1);
	require_once(SMARTY_PATH .  'Smarty.class.php');
	spl_autoload_register('__autoload'); */


	/*  this would grab the second location from the root
	$requested_uri = substr(
		preg_replace('~/{2,}~', '/', $_SERVER['REQUEST_URI']),
		strlen(dirname($_SERVER['PHP_SELF']))
	); */

?>
