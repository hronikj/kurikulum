<?php
  class Work extends CreatorAsset {
    public $start_year;
    public $end_year;
    public $start_month;
    public $end_month;
    public $employer;
    public $position;
    public $description;
    public $reference_name;
    public $reference_contact;
    public $field;

    public static $fieldset = array('start_year',
                                    'end_year',
                                    'employer',
                                    'position',
                                    'description',
                                    'start_month',
                                    'end_month',
                                    'reference_name',
                                    'reference_contact',
                                    'field');

    public $validated_start_year  = FALSE;
    public $validated_end_year    = FALSE;
    public $validated_start_month = FALSE;
    public $validated_end_month   = FALSE;
    public $validated_employer    = FALSE;
    public $validated_position    = FALSE;
    public $validated_description = FALSE;
    public $validated_reference_name = FALSE;
    public $validated_reference_contact = FALSE;
    public $validated_field = FALSE;

    public $rules_start_year  = array('start_year'  => 'required|numeric|max_len,4|min_len,4');
    public $rules_end_year    = array('end_year'    => 'required|numeric|max_len,4|min_len,4');
    public $rules_start_month = array('start_month' => 'required|numeric');
    public $rules_end_month   = array('end_month'   => 'required|numeric');
    public $rules_employer    = array('employer'    => 'required|alpha_numeric');
    public $rules_position    = array('position'    => 'required|alpha_numeric');
    public $rules_description = array('description' => 'required|alpha_numeric');
    public $rules_reference_name = array('reference_name' => 'required');
    public $rules_reference_contact = array('reference_contact' => 'required');
    public $rules_field = array('field' => 'required');

  }
?>
