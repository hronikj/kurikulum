<?php
  class Driving extends CreatorAsset {
    public $level;

    public static $fieldset = array('level');

    public $validated_level = FALSE;

    public $rules_level = array('level' => 'required');
  }
?>
