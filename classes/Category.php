<?php
  class Category {
    public function exists($category) {
      $test = (is_numeric($category)) ? dibi::query('SELECT [id] FROM [categories] WHERE [id] = %i', $category) -> fetchSingle() :
        dibi::query('SELECT [id] FROM [categories] WHERE [name] = %s', $category) -> fetchSingle();

      return ($test) ? TRUE : FALSE;
    }

    public function get($category) {
      $categoryData = (is_numeric($category)) ? dibi::fetch('SELECT * FROM [categories] WHERE [id] = %i', $category) :
        dibi::fetch('SELECT * FROM [categories] WHERE [name] = %s', $category);

      return ($categoryData) ? $categoryData : FALSE;
    }

    public function getId($category) {
      if (is_numeric($category)) {
        $categoryId = dibi::query('SELECT [id] FROM [categories] WHERE [id] = %i', $category) -> fetchSingle();
        if ($categoryId == $category) return $category;
        else return FALSE;
      } else {
        $categoryId = dibi::query('SELECT [id] FROM [categories] WHERE [name] = %s', $category) -> fetchSingle();
        return ($categoryId) ? $categoryId : FALSE;
      }
    }

    public function create($categoryData) {
      return (dibi::query('INSERT INTO [categories]', $categoryData)) ? TRUE : FALSE;
    }

    public function delete($category) {
      if ($this -> exists($category)) {
        if (is_numeric($category)) {
          $test = dibi::query('DELETE FROM [categories] WHERE [id] = %i', $category);
          return ($test) ? TRUE : FALSE;
        } else {
          $test = dibi::query('DELETE FROM [categories] WHERE [name] = %s', $category);
          return ($test) ? TRUE : FALSE;
        }
      } else return FALSE;
    }
  }
?>
