<?php
  class Duplicity {
    public static function evaluateEditForm($originalData, $submittedData) {
      $object_vars = get_object_vars($originalData[0]);
      unset($object_vars['id']);
      unset($object_vars['userid']);

      return (empty(array_diff($object_vars, $submittedData))) ? TRUE : FALSE;
    }
  }
?>
