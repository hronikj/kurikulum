<?php
  class Payment {
    private $userid;
    private $months = 1;   // default amount of months for new subscriptions
    private $amount = 200; // default amount of money for new subscriptions

    function __construct($userid) {
      if (User::exists($userid)) $this -> userid = $userid;
      else return FALSE;
    }

    public function create($from, $to, $amount) {
      if (!empty($this -> userid)) {

        if ($from == NULL)   $from = date("Y-m-d"); // set todays date
        if ($amount == NULL) $amount = $this -> amount;
        if ($to == NULL)     $to = MonthCycles::endCycle($from, $this -> months);

        $transactionData = array(
          'from'    => $from,
          'to'      => $to,
          'amount'  => $amount,
          'userid'  => $this -> userid
        );

        return (dibi::query('INSERT INTO [payments]', $transactionData)) ? TRUE : FALSE;
      } else return FALSE;
    }

    public function isActive() {
      if (!empty($this -> userid)) {
        $today = date('Y-m-d H:i:s', time());
        $data = dibi::query('SELECT * FROM [payments] WHERE [userid] = %i ORDER BY [id] DESC LIMIT 1',
          $this -> userid) -> fetchAll();

        if (isset($data[0] -> id)) {
          $data = $data[0];
          if ($this -> checkDateInRange($data -> from, $data -> to, $today)) {
            return $data -> to;
          } else return FALSE;
        } else return FALSE; // no payment found in database
      } else return FALSE;
    }

    public function delete($paymentid) {
      // check if this paymentid exists
      $paymentid = dibi::query('SELECT [id] FROM [payments] WHERE [id] = %i', $paymentid) -> fetchSingle();
      if ($paymentid) {
        dibi::query('DELETE FROM [payments] WHERE [id] = %i', $paymentid);
        return TRUE;
      } else return FALSE; // payment with that ID does not exist
    }

    private function checkDateInRange($startDate, $endDate, $searchDate) {
      $startTS  = strtotime($startDate);
      $endTS    = strtotime($endDate);
      $searchTS = strtotime($searchDate);

      return (($searchTS >= $startTS) && ($searchTS <= $endTS));
    }

  }
?>
