<?php
  class CreatorCertification {
    public function validate($data) {
      $return_string = '';
      $name   = $data['creator_form_certification_name'];
      $year   = $data['creator_form_certification_year'];
      $school = $data['creator_form_certification_school'];

      foreach ($name as $key => $value) {
        $test['creator_form_certification_name'] = $value;
        $validation = Validation::is_valid($test, array(
            'creator_form_certification_name' => 'required|max_len,20',
          )
        );

        $_SESSION['validation_name'][$key] = $validation;
        if ($validation !== TRUE) {
          foreach ($validation as $key => $value) {  $return_string = $return_string .  $value . ' <br /> ' ; }
        }
      }

      foreach ($year as $key => $value) {
        $test['creator_form_certification_year'] = $value;
        $validation = Validation::is_valid($test, array(
            'creator_form_certification_year' => 'required|max_len,4|min_len,4|numeric',
          )
        );

        $_SESSION['validation_year'][$key] = $validation;
        if ($validation !== TRUE) {
          foreach ($validation as $key => $value) {  $return_string = $return_string .  $value . ' <br /> ' ; }
        }
      }

      foreach ($school as $key => $value) {
        $test['creator_form_certification_school'] = $value;
        $validation = Validation::is_valid($test, array(
            'creator_form_certification_school' => 'required',
          )
        );

        $_SESSION['validation_school'][$key] = $validation;
        if ($validation !== TRUE) {
          foreach ($validation as $key => $value) {  $return_string = $return_string .  $value . ' <br /> ' ; }
        }
      }

      return (empty($return_string)) ? TRUE : $return_string;

    }

  }
?>
