<?php
  class Password {
    protected $password;
    protected $userid;

    /* receives user ID and it's password (optional) */
    public function __construct($userid, $password) {
      if (User::exists($userid)) $this -> userid = $userid;
      if ($password != '') {
        $this -> password = $password;
        $this -> hashPassword($password);
      }
    }

    /* generates token and saves it to the database */
    public function generateToken() {
      if (empty($this -> userid)) return FALSE;

      $db_data['token'] = md5(uniqid(rand())); //generate token
      $db_data['userid'] = $this -> userid;

      return (dibi::query('INSERT INTO [passwordTokens]', $db_data))
        ? $db_data['token'] : FALSE;
    }

    /* validates token */
    public function validateToken($userid, $token) {
      $test = dibi::query('SELECT * FROM [passwordTokens]
        WHERE [userid] = %i AND [token] = %s', $userid, $token)
        -> fetchSingle();

      return ($test) ? TRUE : FALSE;
    }

    /* inserts password to database */
    public function setPassword() {
      if (empty($this -> userid) || empty($this -> password)) return FALSE;
      $this -> hashPassword();

      return (dibi::query('UPDATE `users` SET `password` = %s WHERE `id` = %i',
        $this -> password, $this -> userid))
        ? TRUE : FALSE;
    }

    public function getPassword() {
      return $this -> password;
    }

    /* hashes the password using SHA1 (+ salt maybe) */
    public function hashPassword() {
      $this -> password = sha1($this -> password);
    }

  }
?>
