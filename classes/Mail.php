<?php
  /* Wrapper for PHPMailer, sending general mail, loading templates */
  class Mail {
    private static $instance; // instance of PHPMailer

    public function __construct() {
      ini_set('SMTP', 'ssl://smtp.gmail.com');
      ini_set('smtp_port', MAIL_PORT);

      require SERVER_PATH . 'vendor/phpmailer/phpmailer/PHPMailerAutoload.php';

      $this -> instance = new PHPMailer();

      $this -> instance -> SMTPAuth   = MAIL_SMTPAUTH;
      $this -> instance -> Host       = MAIL_HOST;
      $this -> instance -> SMTPSecure = MAIL_SMTPSECURE;
      $this -> instance -> Username   = MAIL_USERNAME;
      $this -> instance -> Password   = MAIL_PASSWORD;
      $this -> instance -> Port       = MAIL_PORT;
      $this -> instance -> IsSMTP();
      $this -> instance -> WordWrap = 500;
    }

    public static function getInstance() {
      if (static::$instance == NULL) {
        static::$instance = new static();
      }

      return static::$instance;
    }

    public function send($to, $subject, $body, $template, $template_variables) {
      $this -> instance -> AddAddress($to);
      $this -> instance -> Subject = $subject;
      if (!$template) $this -> instance -> Body = $body;
      else $this -> instance -> Body = $this -> loadTemplate($template, $template_variables);

      return ($this -> instance -> Send()) ? TRUE : FALSE;
    }

    private function loadTemplate($template, $template_variables) {
      $templatepath = MAIL_TEMPLATES_PATH . $template . '.tpl';

      if (file_exists($templatepath)) {
        $smarty = new Smarty();

        if (!empty($template_variables)) {
          foreach ($template_variables as $smarty_var => $actual_data) {
              $smarty -> assign($smarty_var, $actual_data);
          }
        }

        return $smarty -> fetch($templatepath);
      } else return FALSE; // file with template was not found
    }
  }
?>
