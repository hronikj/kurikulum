<?php
  abstract class Log {
    protected $logtype;
    protected $logdata = array();

    abstract function addLogEntry();

    public function getLogs($offset, $limit) {
      if (empty($this -> logtype) || !isset($this -> logtype)) return FALSE;

      $logtype = $this -> logtype;
      $query = "SELECT * FROM [$logtype] %lmt %ofs";

      return dibi::query($query, $limit, $offset) -> fetchAll();
    }

    protected function getUserIP() {
      $ipaddress = '';

      if (isset($_SERVER['HTTP_CLIENT_IP']) && $_SERVER['HTTP_CLIENT_IP'])
        $ipaddress = $_SERVER['HTTP_CLIENT_IP'];
      else if (isset($_SERVER['HTTP_X_FORWARDED_FOR']) && $_SERVER['HTTP_X_FORWARDED_FOR'])
        $ipaddress = $_SERVER['HTTP_X_FORWARDED_FOR'];
      else if (isset($_SERVER['HTTP_X_FORWARDED']) && $_SERVER['HTTP_X_FORWARDED'])
        $ipaddress = $_SERVER['HTTP_X_FORWARDED'];
      else if (isset($_SERVER['HTTP_FORWARDED_FOR']) && $_SERVER['HTTP_FORWARDED_FOR'])
        $ipaddress = $_SERVER['HTTP_FORWARDED_FOR'];
      else if (isset($_SERVER['HTTP_FORWARDED']) && $_SERVER['HTTP_FORWARDED'])
        $ipaddress = $_SERVER['HTTP_FORWARDED'];
      else if (isset($_SERVER['REMOTE_ADDR']) && $_SERVER['REMOTE_ADDR'])
        $ipaddress = $_SERVER['REMOTE_ADDR'];
      else
        $ipaddress = 'UNKNOWN';

      $this -> logdata['httpClientHost'] = $ipaddress;
    }

  }
?>
