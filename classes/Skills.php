<?php
  class Skills extends CreatorAsset {
    public $name;
    public $level;

    public static $fieldset = array('name', 'level');

    public $validated_name  = FALSE;
    public $validated_level = FALSE;

    public $rules_name  = array('name'  => 'required');
    public $rules_level = array('level' => 'required');
  }
?>
