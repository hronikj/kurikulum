<?php
  class Education extends CreatorAsset {
    public $start_year;
    public $end_year;
    public $start_month;
    public $end_month;
    public $school;
    public $title;
    public $degree;
    public $finished;

    public static $fieldset = array('start_year',
                                    'end_year',
                                    'school',
                                    'title',
                                    'degree',
                                    'start_month',
                                    'end_month',
                                    'finished');

    public $validated_start_year  = FALSE;
    public $validated_end_year    = FALSE;
    public $validated_start_month = FALSE;
    public $validated_end_month   = FALSE;
    public $validated_school      = FALSE;
    public $validated_title       = FALSE;
    public $validated_degree      = FALSE;
    public $validated_finished    = FALSE;

    public $rules_start_year  = array('start_year'  => 'required|numeric|max_len,4|min_len,4');
    public $rules_end_year    = array('end_year'    => 'required|numeric|max_len,4|min_len,4');
    public $rules_start_month = array('start_month' => 'required|numeric');
    public $rules_end_month   = array('end_month'   => 'required|numeric');
    public $rules_school      = array('school'      => 'required');
    public $rules_title       = array('title'       => 'required');
    public $rules_degree      = array('degree'      => 'required');
    public $rules_finished    = array('finished'    => 'required');
  }
?>
