<?php
  class AuthLog extends Log {
    public function __construct($logdata, $success) {
      $this -> logtype = 'authlogs';
      $this -> logdata = $logdata;
      $this -> logdata['success'] = $success;

      $this ->  getUserIp();
      $this -> logdata['ts']            = date('Y-m-d H:i:s', time());
      $this -> logdata['httpUserAgent'] = $_SERVER['HTTP_USER_AGENT'];
      $this -> logdata['requestMethod'] = $_SERVER['REQUEST_METHOD'];

    }

    public function addLogEntry() {
      $test = dibi::query('INSERT INTO [authlogs]', $this -> logdata);

      return ($test) ? TRUE : FALSE;
    }
  }
?>
