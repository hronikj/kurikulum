<?php
  class Template extends CreatorAsset {
    public $templateid;

    public static $fieldset = array('templateid');

    public $validated_templateid = FALSE;

    public $rules_templateid  = array('templateid' => 'required');
  }
?>
