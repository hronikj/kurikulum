<?php
  class CreatorWork {
    public function validate($data) {
      $return_string = '';
      $employer    = $data['creator_form_work_employer'];
      $position    = $data['creator_form_work_position'];
      $startYear   = $data['creator_form_work_startYear'];
      $endYear     = $data['creator_form_work_endYear'];
      $description = $data['creator_form_work_description'];

      foreach ($startYear as $key => $value) {
        $test['creator_form_work_startYear'] = $value;
        $validation = Validation::is_valid($test, array(
            'creator_form_work_startYear' => 'required|max_len,4|min_len,4|numeric',
          )
        );

        $_SESSION['validation_startYear'][$key] = $validation;
        if ($validation !== TRUE) {
          foreach ($validation as $key => $value) {  $return_string = $return_string .  $value . ' <br /> ' ; }
        }
      }

      foreach ($endYear as $key => $value) {
        $test['creator_form_work_endYear'] = $value;
        $validation = Validation::is_valid($test, array(
            'creator_form_work_endYear' => 'required|max_len,4|min_len,4|numeric',
          )
        );

        $_SESSION['validation_endYear'][$key] = $validation;
        if ($validation !== TRUE) {
          foreach ($validation as $key => $value) {  $return_string = $return_string .  $value . ' <br /> ' ; }
        }
      }

      foreach ($employer as $key => $value) {
        $test['creator_form_work_employer'] = $value;
        $validation = Validation::is_valid($test, array(
            'creator_form_work_employer' => 'required|max_len,20',
          )
        );

        $_SESSION['validation_employer'][$key] = $validation;
        if ($validation !== TRUE) {
          foreach ($validation as $key => $value) {  $return_string = $return_string .  $value . ' <br /> ' ; }
        }
      }

      foreach ($position as $key => $value) {
        $test['creator_form_work_position'] = $value;
        $validation = Validation::is_valid($test, array(
            'creator_form_work_position' => 'required|max_len,20',
          )
        );

        $_SESSION['validation_position'][$key] = $validation;
        if ($validation !== TRUE) {
          foreach ($validation as $key => $value) {  $return_string = $return_string .  $value . ' <br /> ' ; }
        }
      }


      return (empty($return_string)) ? TRUE : $return_string;

    }

  }
?>
