<?php
  class Hobbies extends CreatorAsset {
    public $hobbies;

    public static $fieldset = array('hobbies');

    public $validated_hobbies  = FALSE;

    public $rules_hobbies = array('hobbies'  => 'required');
  }
?>
