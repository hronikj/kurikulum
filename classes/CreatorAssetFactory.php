<?php
  class CreatorAssetFactory {
    public static function create($assetType) {
      if (class_exists($assetType)) {
        return new $assetType();
      } else {
        throw new Exception("Invalid assetType in CreatorAssetFactory!");
      }
    }
  }
 ?>
