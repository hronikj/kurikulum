<?php
  class User {
    /* test if user exists in database */
    // protected function exists($id) {
    //   $test = dibi::query('SELECT [firstName] FROM [users] WHERE [id] = %i', $id);
    //   return (count($test) == 1) ? TRUE : FALSE;
    // }

    public static function exists($id) {
      $test = dibi::query('SELECT [firstName] FROM [users] WHERE [id] = %i', $id);
      return (count($test) == 1) ? TRUE : FALSE;
    }

    public function getById($id) {
      if ($this -> exists($id)) {
        $userdata = dibi::fetch('SELECT * FROM [users] WHERE [id] = %i', $id);
        return $userdata; // returned as Dibi object
      } else return FALSE;
    }

    public function loginExists($login) {
      $test = dibi::query('SELECT [email] FROM [users] WHERE [email] = %s', $login);
      return (count($test) >= 1 ) ? TRUE : FALSE;
    }

    public function search($needle) {
      $modifier = '%';
      $needle = "$modifier$needle$modifier";

      $searchdata = dibi::query('SELECT * FROM [users] WHERE
        [firstName] LIKE %s
        OR [lastName] LIKE %s
        OR [email] LIKE %s', $needle, $needle, $needle) -> fetchAll();

      return $searchdata;
      /*
        foreach ($usersearch as $rowId => $rowObject) {
          echo $rowObject -> id;
          echo $rowObject -> firstName;
          ...
        }
      */
    }

    public function create($userdata) {
      return (dibi::query('INSERT INTO [users]', $userdata)) ? TRUE : FALSE;
    }

    public function getAll() {
      $result = dibi::query('SELECT * FROM [users]') -> fetchAll();

      return ($result) ? $result : FALSE;
      /*
        foreach ($usersearch as $rowId => $rowObject) {
          echo $rowObject -> id;
          echo $rowObject -> firstName;
          ...
        }
      */
    }

    public function getAuthorization($userid) {
      if ($this -> exists($userid)) {
        $userlevel = dibi::query('SELECT [userlevel] FROM [users] WHERE [id] = %i', $userid)
          -> fetchSingle();
        return $userlevel;
      } else return FALSE; // user with that ID does not exist
    }

    public function remove($userid) {
      if ($this -> exists($userid)) {
        dibi::query('DELETE FROM [users] WHERE [id] = %id', $userid);
        return TRUE;
      } else return FALSE; // user with this ID does not exist
    }

    /* userdata reflects columns in the table users */
    public function edit($userid, $userdata) {
      if ($this -> exists($userid)) {
        // clean the input
        foreach ($userdata as $key => $value) { $userdata[$key] = htmlspecialchars(trim($value)); }
        return (dibi::query('UPDATE `users` SET', $userdata, 'WHERE `id` = %i', $userid))
          ? TRUE : FALSE;
      } else return FALSE; // user does not exist
    }

    public function isBlocked($userid) {
      $blocked = dibi::query('SELECT [blocked] FROM [users] WHERE [id] = %i', $userid) -> fetchSingle();
      return ($blocked) ? TRUE : FALSE;
    }

  }
?>
