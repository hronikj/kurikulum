<?php
  abstract class CreatorAsset {
    public static $fieldset;

    protected function validate() {
      $validator = new Validation();

      # late static binding: http://php.net/manual/en/language.oop5.late-static-bindings.php
      foreach (static::$fieldset as $field) {
        $validation_array = array("$field" => $this -> $field);
        $validation_rule_name = "rules_$field";
        $validated_var_name = "validated_$field";
        $validator -> validate($validation_array, $this -> $validation_rule_name);
        $this -> $validated_var_name = $validator -> get_readable_errors(true);
        // var_dump($this -> $validated_var_name);
      }
    }

    public function isValid() {
      foreach (static::$fieldset as $field) {
        $validated_var_name = "validated_$field";

        if ($this -> $validated_var_name == FALSE) {
          $this -> validate();
        }
        if ($this -> $validated_var_name != NULL) {
          return FALSE;
        }
      }

      return TRUE;
    }

    public static function getFieldset() {
      return static::$fieldset;
      # late static binding (returns fieldset of the calling class, not this one)
    }
  }
?>
