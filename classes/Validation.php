<?php
  class Validation extends GUMP {

  	public function get_readable_errors($convert_to_string = false, $field_class="gump-field", $error_class="gump-error-message") {
  		if(empty($this->errors)) {
  			return ($convert_to_string)? null : array();
  		}

  		$resp = array();

  		foreach($this->errors as $e) {

  			$field = ucwords(str_replace($this->fieldCharsToRemove, chr(32), $e['field']));
  			$param = $e['param'];

  			// Let's fetch explicit field names if they exist
  			if(array_key_exists($e['field'], self::$fields)) {
  				$field = self::$fields[$e['field']];
  			}

  			switch($e['rule']) {
  				case 'mismatch' :
  					$resp[] = "There is no validation rule for <span class=\"$field_class\">$field</span>";
  					break;
  				case 'validate_required':
  					$resp[] = "Pole <span class=\"$field_class\">$field</span> je povinné";
  					break;
  				case 'validate_valid_email':
  					$resp[] = "Pole <span class=\"$field_class\">$field</span> musí být platná emailová adresa";
  					break;
  				case 'validate_max_len':
  					if($param == 1) {
  						$resp[] = "The <span class=\"$field_class\">$field</span> field needs to be shorter than $param character";
  					} else {
  						$resp[] = "The <span class=\"$field_class\">$field</span> field needs to be shorter than $param characters";
  					}
  					break;
  				case 'validate_min_len':
  					if($param == 1) {
  						$resp[] = "The <span class=\"$field_class\">$field</span> field needs to be longer than $param character";
  					} else {
  						$resp[] = "The <span class=\"$field_class\">$field</span> field needs to be longer than $param characters";
  					}
  					break;
  				case 'validate_exact_len':
  					if($param == 1) {
  						$resp[] = "The <span class=\"$field_class\">$field</span> field needs to be exactly $param character in length";
  					} else {
  						$resp[] = "The <span class=\"$field_class\">$field</span> field needs to be exactly $param characters in length";
  					}
  					break;
  				case 'validate_alpha':
  					$resp[] = "The <span class=\"$field_class\">$field</span> field may only contain alpha characters(a-z)";
  					break;
  				case 'validate_alpha_numeric':
  					$resp[] = "The <span class=\"$field_class\">$field</span> field may only contain alpha-numeric characters";
  					break;
  				case 'validate_alpha_dash':
  					$resp[] = "The <span class=\"$field_class\">$field</span> field may only contain alpha characters &amp; dashes";
  					break;
  				case 'validate_numeric':
  					$resp[] = "The <span class=\"$field_class\">$field</span> field may only contain numeric characters";
  					break;
  				case 'validate_integer':
  					$resp[] = "The <span class=\"$field_class\">$field</span> field may only contain a numeric value";
  					break;
  				case 'validate_boolean':
  					$resp[] = "The <span class=\"$field_class\">$field</span> field may only contain a true or false value";
  					break;
  				case 'validate_float':
  					$resp[] = "The <span class=\"$field_class\">$field</span> field may only contain a float value";
  					break;
  				case 'validate_valid_url':
  					$resp[] = "The <span class=\"$field_class\">$field</span> field is required to be a valid URL";
  					break;
  				case 'validate_url_exists':
  					$resp[] = "The <span class=\"$field_class\">$field</span> URL does not exist";
  					break;
  				case 'validate_valid_ip':
  					$resp[] = "The <span class=\"$field_class\">$field</span> field needs to contain a valid IP address";
  					break;
  				case 'validate_valid_cc':
  					$resp[] = "The <span class=\"$field_class\">$field</span> field needs to contain a valid credit card number";
  					break;
  				case 'validate_valid_name':
  					$resp[] = "The <span class=\"$field_class\">$field</span> field needs to contain a valid human name";
  					break;
  				case 'validate_contains':
  					$resp[] = "The <span class=\"$field_class\">$field</span> field needs to contain one of these values: ".implode(', ', $param);
  					break;
  				case 'validate_containsList':
  					$resp[] = "The <span class=\"$field_class\">$field</span> field needs contain a value from its drop down list";
  					break;
  				case 'validate_doesNotContainList':
  					$resp[] = "The <span class=\"$field_class\">$field</span> field contains a value that is not accepted";
  					break;
  				case 'validate_street_address':
  					$resp[] = "The <span class=\"$field_class\">$field</span> field needs to be a valid street address";
  					break;
  				case 'validate_date':
  					$resp[] = "The <span class=\"$field_class\">$field</span> field needs to be a valid date";
  					break;
  				case 'validate_min_numeric':
  					$resp[] = "The <span class=\"$field_class\">$field</span> field needs to be a numeric value, equal to, or higher than $param";
  					break;
  				case 'validate_max_numeric':
  					$resp[] = "The <span class=\"$field_class\">$field</span> field needs to be a numeric value, equal to, or lower than $param";
  					break;
  				case 'validate_starts':
  					$resp[] = "The <span class=\"$field_class\">$field</span> field needs to start with $param";
  					break;

          case 'validate_extension':
            $resp[] = "The <span class\"$field_class\">$field</span> field can have the following extensions $param";
            break;
          case 'validate_required_file':
            $resp[] = "Pole <span class\"$field_class\">$field</span> je povinné";
            break;

          default:
  					$resp[] = "The <span class=\"$field_class\">$field</span> field is invalid";
  			}
  		}

  		if(!$convert_to_string) {
  			return $resp;
  		} else {
  			$buffer = '';
  			foreach($resp as $s) {
  				$buffer .= "<span class=\"$error_class\">$s</span>";
  			}
  			return $buffer;
  		}
  	}

    public static function is_valid(array $data, array $validators) {
  		$gump = new Validation();

  		$gump->validation_rules($validators);

  		if($gump->run($data) === false) {
  			return $gump->get_readable_errors(false);
  		} else {
  			return true;
  		}
  	}
  }
?>
