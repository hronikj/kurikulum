<?php
  class Session {
    function __construct($userObject) {
      $_SESSION['user'] = $userObject;
    }

    public function login($login, $password) {
      $password = new Password(NULL, $password);
      $logdata['username'] = $login;

      $success = dibi::query('SELECT [id] FROM [users]
        WHERE [email] = %s AND [password] = %s',
        $login, $password -> getPassword()) -> fetchSingle();

      $userid = dibi::query('SELECT [id] FROM [users] WHERE [email] = %s', $login) -> fetchSingle();

      if ($success) {

        // check if user is blocked
        $user = new User();
        if ($user -> isBlocked($userid)) {
          $authLog = new AuthLog($logdata, FALSE);
          $authLog -> addLogEntry();
          return FALSE;
        }

        $authLog = new AuthLog($logdata, TRUE);
        $authLog -> addLogEntry();

        // set session variables
        $_SESSION['logged']   = 1;
        $_SESSION['username'] = $login;
        $_SESSION['id']       = $userid;

        return TRUE;
      } else {
        // log as unsuccessfull
        if ($userid) $logdata['userid'] = $userid;
        $authLog = new AuthLog($logdata, FALSE);
        $authLog -> addLogEntry();
        return FALSE;
      }
    }

    public function logout() {
      if ($this -> isLoggedIn()) {
        $_SESSION = array();
        session_destroy();

        return TRUE;
      } else return FALSE; // user is not logged in
    }

  }
?>
