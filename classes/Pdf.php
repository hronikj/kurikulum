<?php
  class Pdf {
    public function __construct($template) {
      #                                       right  bottom
      # https://github.com/mpdf/mpdf            |       |
      $mpdf = new mPDF('utf-8','A4','','', 10, 10, 10, 10, 10, 10);
      #                                     |       |       |   |
      #                                     left    top     ??? ???

      // $html = '
      //   <html>
      //     <head>
      //       <style>
      //         h1 {
      //           color: white;
      //           font-size: 16px;
      //           font-family: arial;
      //           background-color: black;
      //           padding: 5px;
      //         }
      //         table {
      //           font-size: 10px;
      //           margin-top: 200px;
      //         }
      //       </style>
      //     </head>
      //     <body>
      //       <h1> Test </h1>
      //       <p style="font-family: trebuchetms"> Some testing data... </p>
      //       <table border="1">
      //         <tr> <td> test </td> <td> test 2 </td> </tr>
      //         <tr> <td> something </td> <td> something else as value </td> </tr>
      //       </table>
      //     </body>
      //   </html>
      // ';

      $html = $this -> loadTemplate($template);

      $mpdf -> WriteHtml($html);
      $mpdf -> Output('cv.pdf', 'D');
    }

    private function loadTemplate($template) {
      $templatepath = PDF_TEMPLATES_PATH . $template . '.tpl';
      if (file_exists($templatepath)) {
        $smarty = new Smarty();

        # parse basic information

        $user = new User();
        $basic_data = $user -> getById($_SESSION['id']);
        var_dump($basic_data);

        echo($basic_data -> firstName);

        $smarty -> assign('smarty_firstName', $basic_data -> firstName);
        $smarty -> assign('smarty_lastName', $basic_data -> lastName);
        $smarty -> assign('smarty_city', $basic_data -> city);
        $smarty -> assign('smarty_telephone', $basic_data -> telephone);
        $smarty -> assign('smarty_email', $basic_data -> email);
        $smarty -> assign('smarty_zipCode', $basic_data -> zipCode);

        # parse education
        $fieldset = array('degree', 'school', 'startYear', 'endYear', 'title');
        foreach ($fieldset as $fieldname) {
          $data = $_SESSION['creator_form_education']["$fieldname"];
          foreach ($data as $key => $value) {
            $education_data[$key]["$fieldname"] = $value;
          }
        }

        # parse work
        $fieldset = array('employer', 'startYear', 'endYear', 'position', 'description');
        foreach ($fieldset as $fieldname) {
          $data = $_SESSION['creator_form_work']["$fieldname"];
          foreach ($data as $key => $value) {
            $work_data[$key]["$fieldname"] = $value;
          }
        }

        # parse languages
        $fieldset = array('name', 'level');
        foreach ($fieldset as $fieldname) {
          $data = $_SESSION['creator_form_languages']["$fieldname"];
          foreach ($data as $key => $value) {
            $languages_data[$key]["$fieldname"] = $value;
          }
        }

        # parse skills
        $fieldset = array('name', 'level');
        foreach ($fieldset as $fieldname) {
          $data = $_SESSION['creator_form_skills']["$fieldname"];
          foreach ($data as $key => $value) {
            $skills_data[$key]["$fieldname"] = $value;
          }
        }

        # parse certification
        $fieldset = array('name', 'year', 'school');
        foreach ($fieldset as $fieldname) {
          $data = $_SESSION['creator_form_certification']["$fieldname"];
          foreach ($data as $key => $value) {
            $certification_data[$key]["$fieldname"] = $value;
          }
        }

        $smarty -> assign('smarty_education_data', $education_data);
        $smarty -> assign('smarty_work_data', $work_data);
        $smarty -> assign('smarty_languages_data', $languages_data);
        $smarty -> assign('smarty_certification_data', $certification_data);
        $smarty -> assign('smarty_skills_data', $skills_data  );

        return $smarty -> fetch($templatepath);

      } else return FALSE; // template does not exist
    }
  }
?>
