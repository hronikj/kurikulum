<?php
  class Certification extends CreatorAsset {
    public $name;
    public $organization;
    public $year;

    public static $fieldset = array('name', 'organization', 'year');

    public $validated_name         = FALSE;
    public $validated_organization = FALSE;
    public $validated_year         = FALSE;

    public $rules_name  = array('name'  => 'required');
    public $rules_organization = array('organization' => 'required');
    public $rules_year = array('year' => 'required|numeric');
  }
?>
