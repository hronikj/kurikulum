<?php
  class Profile {
    protected $profileDetails = array();
    protected $userid;

    public function __construct($userid) {
      $this -> userid = $userid;
    }

    // public function add($userid, $type, $profileDetails) {
    //   $user = new User();
    //   if ($user -> exists($userid)) {
    //     $profileDetails['userid'] = $userid;
    //     return (dibi::query("INSERT INTO [$type]", $profileDetails)) ? TRUE : FALSE;
    //   } else return FALSE;
    // }

    public function edit($id, $type, $data) {
      unset($data['id']);
      return (dibi::query("UPDATE [profile_$type] SET", $data, 'WHERE [id] = %i', $id)) ? TRUE : FALSE;
    }

    public function update($id, $data) {
      dibi::query('UPDATE [user_details] SET', $data, 'WHERE [userid] = %i', $id);
      return TRUE;

      # TODO distinction when we're saving the same data
    }

    // public static function getData($type, $userid) {
    //   $user = new User();
    //   if ($user -> exists($userid)) {
    //     $userdata = dibi::query("SELECT * FROM [$type] WHERE [userid] = %i", $userid);
    //     return ($userdata) ? $userdata -> fetchAll() : FALSE;
    //   } else return FALSE;
    // }

    public static function getDetail() {
      if ($this -> userid) {

      } else return FALSE;

      // if ($type == 'education' || $type == 'work' || $type == 'certification' || $type == 'skills' || $type == 'language') {
      //   $test = dibi::query("SELECT * FROM [profile_$type] WHERE [id] = %i AND [userid] = %i", $id, $userid) -> fetchAll();
      //   if (count($test) > 0) {
      //     return $test;
      //   } else return FALSE;
      // } else return FALSE;
    }

    public function getProfileDetails() {
      if ($this -> userid) {
        $test = dibi::query('SELECT * FROM [user_details] WHERE [userid] = %i', $this -> userid) -> fetchAll();
        return $test;
      } else return FALSE;
    }

    public static function deleteDetail($id, $userid, $type) {
      # test if this is user's detail
      if ($type == 'education' || $type == 'work' || $type == 'certification' || $type == 'skills' || $type == 'language') {
        $test = dibi::query("SELECT [id] FROM [profile_$type] WHERE [id] = %i AND [userid] = %i", $id, $userid) -> fetchSingle();
        if (count($test) > 0) {
          # deletion
          return (dibi::query("DELETE FROM [profile_$type] WHERE [id] = %i", $id)) ? TRUE : FALSE;
        } else return FALSE;
      } else return FALSE;
    }
  }
?>
