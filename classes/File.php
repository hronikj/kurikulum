<?php
  class File {
    protected $file_name;
    protected $file_type;
    protected $file_sizeLimit;
    protected $file_typeLimit = array();
    protected $file_id;
    protected $location;
    protected $target_dir;

    public function __construct($filename, $location, $file_id, $file_sizeLimit, $file_typeLimit) {
      $this -> file_name = $filename;
      $this -> location = $location;
      $this -> file_id = $file_id;
      $this -> file_sizeLimit = $file_sizeLimit;
      $this -> file_typeLimit = $file_typeLimit;
      $this -> target_dir = '/var/run/php-uploads/';
    }

    public function put() {
      $file_type = pathinfo($_FILES[$this -> file_id]['name'], PATHINFO_EXTENSION);
      $userid = $_SESSION['user'] -> getId();
      move_uploaded_file($_FILES[$this -> file_id]['tmp_name'], $this -> target_dir . $userid . '.' . $file_type);


      // $location = move_uploaded_file($_FILES[$this -> file_id]['tmp_name'], $this -> location);
      // var_dump($location);

      // $this -> file_type = pathinfo($_FILES[$this -> file_id]['name'], PATHINFO_EXTENSION);
      // $target_file = $this -> location . $this -> file_name . '.' . $this -> file_type;
      //
      // # if ($this -> exists($target_file))
      // #  return FALSE;
      // # else
      // #  return (move_uploaded_file($_FILES[$this -> file_id]['tmp_name'],  $target_file))
      // #    ? TRUE : FALSE;
      // $image_data['image'] = fopen($_FILES[$this -> file_id]['tmp_name'], 'rb');
      // $image_data['image'] = addslashes(fread($image_data['image'], $_FILES[$this -> file_id]['size']));
      // $image_data['image_type'] = $_FILES[$this -> file_id]['type'];
      // $image_data['userid'] = $_SESSION['user'] -> getId();
      //
      // $image_check = dibi::query('SELECT [id] from [photos] WHERE [userid] = %i', $_SESSION['user'] -> getId()) -> fetchSingle();
      // if ($image_check) {
      //   if (dibi::query('UPDATE [photos] SET ', $image_data, 'WHERE [id] = %i', $image_check)) return TRUE;
      //   else return FALSE;
      // } else {
      //   if (dibi::query('INSERT INTO [photos]', $image_data)) return TRUE;
      //   else return FALSE;
      // }

    }

    public function setFilename($filename) {
      $this -> file_name = $filename;
    }

    public function generateFileName() {
      $this -> file_name = uniqid();
    }

    private function exists($target_file) {
      if (file_exists($target_file)) return TRUE;
      else return FALSE;
    }

    public function checkSize() {
      if ($_FILES[$this -> file_id]['size'] > $this -> file_sizeLimit) return FALSE;
      else return TRUE;
    }

    public function checkFileType() {
      # TODO - test and review
      if (in_array($this -> file_type, $this -> file_typeLimit)) return TRUE;
      else return FALSE;

    }
  }
?>
