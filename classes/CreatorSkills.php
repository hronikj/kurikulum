<?php
  class CreatorSkills {
    public function validate($data) {
      $return_string = '';
      $name  = $data['creator_form_skills_name'];
      $level = $data['creator_form_skills_level'];

      foreach ($name as $key => $value) {
        $test['creator_form_skills_name'] = $value;
        $validation = Validation::is_valid($test, array(
            'creator_form_skills_name' => 'required|max_len,20',
          )
        );

        $_SESSION['validation_name'][$key] = $validation;
        if ($validation !== TRUE) {
          foreach ($validation as $key => $value) {  $return_string = $return_string .  $value . ' <br /> ' ; }
        }
      }

      foreach ($level as $key => $value) {
        $test['creator_form_skills_level'] = $value;
        $validation = Validation::is_valid($test, array(
            'creator_form_skills_level' => 'required',
          )
        );
        
        $_SESSION['validation_level'][$key] = $validation;
        if ($validation !== TRUE) {
          foreach ($validation as $key => $value) {  $return_string = $return_string .  $value . ' <br /> ' ; }
        }
      }

      return (empty($return_string)) ? TRUE : $return_string;

    }

  }
?>
