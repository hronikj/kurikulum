<?php
  class Registration {
    public function create($registrationData, $userDetails) {
      $login = strtolower($registrationData['login']);
      $login = htmlspecialchars(trim($login));

      // check if login is not already in use in users table
      $test = dibi::query('SELECT [id] FROM [users] WHERE [login] = %s', $login) -> fetchSingle();
      if (!$test) {
        // check if login is not already in user in registrations table
        $test = dibi::query('SELECT [id] FROM [registrations] WHERE [login] = %s', $login) -> fetchSingle();
        if (!$test) {
          // osetreni vstupu
          foreach ($registrationData as $key => $value) {
            $registrationData[$key] = htmlspecialchars(trim($value));
          }

          foreach ($userDetails as $key => $value) {
            $userDetails[$key] = htmlspecialchars(trim($value));
          }

          $registrationData['login']              = $login;
          $registrationData['token']              = $this -> generateToken();
          $registrationData['registeredSince']    = date('Y-m-d H:i:s', time());

          $password = new Password(NULL, $registrationData['password']);
          $registrationData['password'] = $password -> getPassword();

          if (dibi::query('INSERT INTO [registrations]', $registrationData)) {
            $lastId = dibi::getInsertId();
            $userDetails['registrationid'] = $lastId;
            if (dibi::query('INSERT INTO [user_details]', $userDetails)) {
              # send registration email
              // $mail = new Mail();
              $recipient = $registrationData['login'];
              $subject = 'Registrace do CareerWay';
              $template_variables['token'] = $registrationData['token'];
              Mail::getInstance() -> send($recipient, $subject, NULL, 'registration', $template_variables);
              // $mail -> send($recipient, $subject, NULL, 'registration', $template_variables);
              return TRUE;
            } else return FALSE;
          } else return FALSE;

        } else return FALSE; // login is already in use
      } else return FALSE; // login is already in use
    }

    public function finish($token) {
      // osetreni vstupu
      $token = htmlspecialchars(trim($token));

      if ($this -> validateToken($token)) {
        $registrationData = dibi::query('SELECT * FROM [registrations] WHERE [token] = %s', $token)
          -> fetchAssoc('token');

        $registrationId = $registrationData[$token]['id'];
        unset($registrationData[$token]['id']);
        unset($registrationData[$token]['token']);

        // start database transaction (insert to users table & delete from registrations table)
        dibi::begin();
          if (!dibi::query('INSERT INTO [users]', $registrationData[$token])) return FALSE; // insert unsuccessfull

          $lastId = dibi::getInsertId();
          if (!dibi::query('UPDATE [user_details] SET [userid] = %i WHERE [registrationid] = %i', $lastId, $registrationId)) return FALSE;
          if (!dibi::query('DELETE FROM [registrations] WHERE [token] = %s', $token)) return FALSE; // delete unsuccessfull
        dibi::commit(); // end of the transaction

        return TRUE;

      } else return FALSE; // invalid token
    }

    private function validateToken($token) {
      $test = dibi::query('SELECT [id] FROM [registrations] WHERE [token] = %s', $token) -> fetchSingle();
      return ($test) ? TRUE : FALSE;
    }

    private function generateToken() {
      return md5(uniqid(rand()));
    }
  }
?>
