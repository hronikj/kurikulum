<?php
  /* Flash message mechanism  */
  class Message {
    private static $instance;

    public static function getInstance() {
      if (static::$instance == NULL) {
        static::$instance = new static();
      }

      return static::$instance;
    }

    protected function __construct() {
    }

    public function add($msgText, $msgType, $msgClass) {
      $msg_id = uniqid();
      while (isset($_SESSION['messages'][$msg_id]) == $msg_id) {
        $msg_id = uniqid();
      }

      $_SESSION['messages'][$msg_id]['type']    = $msgType;
      $_SESSION['messages'][$msg_id]['class']   = $msgClass;
      $_SESSION['messages'][$msg_id]['message'] = $msgText;

      return TRUE;
    }

    public function addCanned($type, $id) {
      switch ($type) {
        case 'error' :  $tablename = 'errorMessages';
                        break;

        case 'notice':  $tablename = 'noticeMessages';
                        break;

        case 'ok':      $tablename = 'successMessages';
                        break;

        default:        return FALSE; // type not implemented
      }

      $query = "SELECT * FROM [$tablename] WHERE id = %i";
      $messageData = dibi::fetch($query, $id);

      if ($messageData) {
        $msg_id = uniqid();
        while (isset($_SESSION['messages'][$msg_id]) == $msg_id) {
          $msg_id = uniqid();
        }

        $_SESSION['messages'][$msg_id]['type']    = $messageData -> type;
        $_SESSION['messages'][$msg_id]['class']   = $messageData -> class;
        $_SESSION['messages'][$msg_id]['message'] = $messageData -> text;

        return TRUE;
      } else return FALSE;
    }


    /* usage:
      $messages = $messages -> get();

      foreach ($messages as $msg_id => $msg_index) {
        echo $msg_index['type'] . ' - ';
        echo $msg_index['class'] . ' - <i> ';
        echo $msg_index['message'] . ' </i>';
      }
    */
    public function get() {
      $return = array();
      $count = FALSE;

      if (!empty($_SESSION['messages'])) {
        foreach ($_SESSION['messages'] as $msg_id => $msg_index) {
          $return[$msg_id]['type']    = $msg_index['type'];
          $return[$msg_id]['class']   = $msg_index['class'];
          $return[$msg_id]['message'] = $msg_index['message'];

          unset($_SESSION['messages'][$msg_id]);

          $count = TRUE;
        }
      }

      return ($count) ? $return : FALSE; // if there are no messages, returns FALSE
    }
  }
?>
