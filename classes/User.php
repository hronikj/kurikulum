<?php
  class User {
    private $username;
    private $id;

    # 0 = blocked user
    # 1 = registered user
    # 2 = registered user with paid membership
    # 3 = administrator
    private $accessLevel;

    function __construct($username) {
      # check if user exists in the database
      $this -> id = self::exists($username);
      $this -> username = $username;
    }

    public function login($password) {
      $password = new Password(NULL, $password);
      $test = dibi::query('SELECT [id] FROM [users] WHERE [login] = %s AND [password] = %s',
                            $this -> username, $password -> getPassword()) -> fetchSingle();

      if ($test) {
        if ($this -> getAccessLevel()) {
          $_SESSION['user'] = $this;
          return TRUE;
        } else return FALSE; // user is blocked
      }
    }

    public static function logout() {
      if (isset($_SESSION['user'])) {
        unset($_SESSION['user']);
      }
    }

    public static function exists($username) {
      $userid = dibi::query('SELECT [id] FROM [users] WHERE [login] = %s', $username) -> fetchSingle();
      return ($userid) ? $userid : FALSE;
    }

    public function getUsername() {
      return $this -> username;
    }

    public function getId() {
      return $this -> id;
    }

    public function getAccessLevel() {
      if (isset($this -> id)) {
        $accessLevel = dibi::query('SELECT [access_level] FROM [users] WHERE [id] = %i', $this -> id);
        $this -> accessLevel = $accessLevel;
        return $this -> accessLevel;
      } else {
        return FALSE;
      }
    }

    public function getUserDetails() {
    }

    # general static functions
    public static function loginExists($login) {
      $test = dibi::query('SELECT [login] FROM [users] WHERE [login] = %s', $login);
      return (count($test) >= 1 ) ? TRUE : FALSE;
    }

    public static function create($login, $password) {
      $array = array('login' => $login, 'password' => $password, 'registration_date' => date("Y-m-d H:i:s"));
      return (dibi::query('INSERT INTO [user]', $userdata)) ? TRUE : FALSE;
    }

    public static function remove($username) {
      if ($this -> exists($username)) {
        dibi::query('DELETE FROM [users] WHERE [username] = %s', $username);
        return TRUE;
      } else return FALSE; // user with this ID does not exist
    }

    /* userdata reflects columns in the table users */
    public function edit($userid, $userdata) {
      if ($this -> exists($userid)) {
        # clean the input
        foreach ($userdata as $key => $value) { $userdata[$key] = htmlspecialchars(trim($value)); }
        return (dibi::query('UPDATE `users` SET', $userdata, 'WHERE `id` = %i', $userid))
          ? TRUE : FALSE;
      } else return FALSE; // user does not exist
    }

    public static function isBlocked($userid) {
      if ($this -> accessLevel == 0) return TRUE;
      else return FALSE;
    }

    public static function isLoggedIn() {
      if (isset($_SESSION['user'])) return TRUE;
      else return FALSE;
    }

  }
?>
