<?php
  class ProfileWork extends Profile {
    public function __construct($profileDetails) {
      $cleaned_profile_details = array();

      foreach ($profileDetails as $key => $value) {
        if (preg_match('/profile_add_work/',$key)) {
          $cleaned_key = str_replace('profile_add_work_', '', $key);
          $cleaned_profile_details[$cleaned_key] = $value;
        }

        if (preg_match('/profile_edit_work/',$key)) {
          $cleaned_key = str_replace('profile_edit_work_', '', $key);
          $cleaned_profile_details[$cleaned_key] = $value;
        }
      }
      unset($cleaned_profile_details['submit']);
      $this -> profileDetails = $cleaned_profile_details;
    }

    public function validate() {
      $return_string = '';
      $validated = Validation::is_valid($this -> profileDetails, array(
          'startYear'   => 'required|max_len,4|min_len,4|numeric',
          'endYear'     => 'required|max_len,4|min_len,4|numeric',
          'employer'    => 'required|max_len,255',
          'position'    => 'required|max_len,255',
          'description' => 'max_len,255',
        )
      );

      if ($validated === TRUE) return TRUE;
      else {
        foreach ($validated as $key => $value) {  $return_string = $return_string .  $value . ' <br /> ' ; }
        return $return_string;
      }
    }
  }
?>
