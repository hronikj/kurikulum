<?php
  class Creator {
    public function getAllConfigurations($userid) {
      $configurations = dibi::query('SELECT * FROM [cv_configurations] WHERE [userid] = %i', $userid) -> fetchAll();
      return ($configurations) ? $configurations : FALSE;
    }

    public function deleteConfiguration($id, $userid) {
      return (dibi::query('DELETE FROM [cv_configurations] WHERE [id] = %i AND [userid] = %i', $id, $userid)) ? TRUE : FALSE;
    }

    public function loadConfiguration($id, $userid) {
      # load basic data

      $basic_data_query = dibi::query('SELECT * FROM [cv_configurations] WHERE [id] = %i AND [userid] = %i', $id, $userid) -> fetchAll();
      $_SESSION['creator_form']['graphics'] =  $basic_data_query[0]['graphics'];
      $_SESSION['creator_form']['name'] =  $basic_data_query[0]['name'];

      if ($basic_data_query[0]['id']) {
        $_SESSION['edit'] = TRUE; // will skip validation for the first step

        # load education
        $data = dibi::query('SELECT * FROM [cv_assets_education] WHERE [configid] = %i', $id) -> fetchAll();
        foreach ($data as $rowId => $object) {
          $_SESSION['creator_form_education']['id'][$rowId]        = $data[$rowId]['id'];
          $_SESSION['creator_form_education']['school'][$rowId]    = $data[$rowId]['school'];
          $_SESSION['creator_form_education']['degree'][$rowId]    = $data[$rowId]['degree'];
          $_SESSION['creator_form_education']['startYear'][$rowId] = $data[$rowId]['startYear'];
          $_SESSION['creator_form_education']['endYear'][$rowId]   = $data[$rowId]['endYear'];
          $_SESSION['creator_form_education']['title'][$rowId]     = $data[$rowId]['title'];
        }

        # load work
        $data = dibi::query('SELECT * FROM [cv_assets_work] WHERE [configid] = %i', $id) -> fetchAll();
        foreach ($data as $rowId => $object) {
          $_SESSION['creator_form_work']['id'][$rowId]          = $data[$rowId]['id'];
          $_SESSION['creator_form_work']['employer'][$rowId]    = $data[$rowId]['employer'];
          $_SESSION['creator_form_work']['startYear'][$rowId]   = $data[$rowId]['startYear'];
          $_SESSION['creator_form_work']['endYear'][$rowId]     = $data[$rowId]['endYear'];
          $_SESSION['creator_form_work']['position'][$rowId]    = $data[$rowId]['position'];
          $_SESSION['creator_form_work']['description'][$rowId] = $data[$rowId]['description'];
        }

        # load skills
        $data = dibi::query('SELECT * FROM [cv_assets_skills] WHERE [configid] = %i', $id) -> fetchAll();
        foreach ($data as $rowId => $object) {
          $_SESSION['creator_form_skills']['id'][$rowId]    = $data[$rowId]['id'];
          $_SESSION['creator_form_skills']['name'][$rowId]  = $data[$rowId]['name'];
          $_SESSION['creator_form_skills']['level'][$rowId] = $data[$rowId]['level'];
        }

        # load languages
        $data = dibi::query('SELECT * FROM [cv_assets_language] WHERE [configid] = %i', $id) -> fetchAll();
        foreach ($data as $rowId => $object) {
          $_SESSION['creator_form_languages']['id'][$rowId]   = $data[$rowId]['id'];
          $_SESSION['creator_form_languages']['name'][$rowId] = $data[$rowId]['name'];
          $_SESSION['creator_form_languages']['level'][$rowId] = $data[$rowId]['level'];
        }

        # load certifications
        $data = dibi::query('SELECT * FROM [cv_assets_certification] WHERE [configid] = %i', $id) -> fetchAll();
        foreach ($data as $rowId => $object) {
          $_SESSION['creator_form_certification']['id'][$rowId]     = $data[$rowId]['id'];
          $_SESSION['creator_form_certification']['name'][$rowId]   = $data[$rowId]['name'];
          $_SESSION['creator_form_certification']['year'][$rowId]   = $data[$rowId]['year'];
          $_SESSION['creator_form_certification']['school'][$rowId] = $data[$rowId]['school'];
        }
      }
    }

    public function editConfiguration($id, $data) {
      $config_data['name'] = $data['creator_form']['name'];
      $config_data['graphics'] = $data['creator_form']['graphics'];

      # prepare education data
      $degree        = $data['creator_form_education']['degree'];
      $school        = $data['creator_form_education']['school'];
      $startYear     = $data['creator_form_education']['startYear'];
      $endYear       = $data['creator_form_education']['endYear'];
      $title         = $data['creator_form_education']['title'];
      $education_id  = $data['creator_form_education']['id'];

      foreach ($degree as $key => $value) {
        $education_data[] = array('degree'    => $value,
                                  'id'        => $education_id[$key],
                                  'school'    => $school[$key],
                                  'startYear' => $startYear[$key],
                                  'endYear'   => $endYear[$key],
                                  'title'     => $title[$key]
                                  );
      }

      # prepare work data
      $employer    = $data['creator_form_work']['employer'];
      $startYear   = $data['creator_form_work']['startYear'];
      $endYear     = $data['creator_form_work']['endYear'];
      $description = $data['creator_form_work']['description'];
      $position    = $data['creator_form_work']['position'];
      $work_id     = $data['creator_form_work']['id'];

      foreach ($employer as $key => $value) {
        $work_data[] = array(  'employer'    => $value,
                               'id'          => $work_id[$key],
                               'description' => $description[$key],
                               'startYear'   => $startYear[$key],
                               'endYear'     => $endYear[$key],
                               'position'    => $position[$key]
                              );
      }

      # prepare skills data
      $name      = $data['creator_form_skills']['name'];
      $level     = $data['creator_form_skills']['level'];
      $skills_id = $data['creator_form_skills']['id'];

      foreach ($name as $key => $value) {
        $skills_data[] = array('name'     => $value,
                               'id'       => $skills_id[$key],
                               'level'    => $level[$key]
                              );
      }

      # prepare languages data
      $name  = $data['creator_form_languages']['name'];
      $level = $data['creator_form_languages']['level'];
      $languages_id = $data['creator_form_languages']['id'];

      foreach ($name as $key => $value) {
        $languages_data[] = array('name'     => $value,
                                  'id'       => $languages_id[$key],
                                  'level'    => $level[$key]
                                  );
      }

      # prepare certification data
      $name   = $data['creator_form_certification']['name'];
      $school = $data['creator_form_certification']['school'];
      $year   = $data['creator_form_certification']['year'];
      $certification_id = $data['creator_form_certification']['id'];

      foreach ($name as $key => $value) {
        $certification_data[] = array('name'     => $value,
                                      'id'       => $certification_id[$key],
                                      'school'   => $school[$key],
                                      'year'     => $year[$key]
                                     );
      }

      # updates
      dibi::begin();
        dibi::query('UPDATE [cv_configurations] SET', $config_data, 'WHERE [id] = %i', $id);

        foreach ($education_data as $key => $value) {
          $asset_id = $education_data[$key]['id'];
          unset($education_data[$key]['id']);

          dibi::query('UPDATE [cv_assets_education] SET', $education_data[$key], 'WHERE [configid] = %i AND [id] = %i', $id, $asset_id);
        }

        foreach ($work_data as $key => $value) {
          $asset_id = $work_data[$key]['id'];
          unset($work_data[$key]['id']);

          dibi::query('UPDATE [cv_assets_work] SET', $work_data[$key], 'WHERE [configid] = %i AND [id] = %i', $id, $asset_id);
        }

        foreach ($skills_data as $key => $value) {
          $asset_id = $skills_data[$key]['id'];
          unset($skills_data[$key]['id']);

          dibi::query('UPDATE [cv_assets_skills] SET', $skills_data[$key], 'WHERE [configid] = %i AND [id] = %i', $id, $asset_id);
        }

        foreach ($languages_data as $key => $value) {
          $asset_id = $languages_data[$key]['id'];
          unset($languages_data[$key]['id']);

          dibi::query('UPDATE [cv_assets_language] SET', $languages_data[$key], 'WHERE [configid] = %i AND [id] = %i', $id, $asset_id);
        }

        foreach ($certification_data as $key => $value) {
          $asset_id = $certification_data[$key]['id'];
          unset($certification_data[$key]['id']);

          dibi::query('UPDATE [cv_assets_certification] SET', $certification_data[$key], 'WHERE [configid] = %i AND [id] = %i', $id, $asset_id);
        }

      dibi::commit();

      return TRUE;
    }

    public function createCofiguration($name, $data, $userid) {
      if (!self::configurationExists($name, $userid)) {
        # start transaction
        dibi::begin();
          # create configuration
          $configuration_data = $data['creator_form'];
          $configuration_data['userid'] = $userid;

          dibi::query('INSERT INTO [cv_configurations]', $configuration_data);

          $configuration_id = dibi::insertId();


          # prepare education data
          $degree    = $data['creator_form_education']['degree'];
          $school    = $data['creator_form_education']['school'];
          $startYear = $data['creator_form_education']['startYear'];
          $endYear   = $data['creator_form_education']['endYear'];
          $title     = $data['creator_form_education']['title'];


          foreach ($degree as $key => $value) {
            $education_data[] = array('degree'    => $value,
                                      'school'    => $school[$key],
                                      'startYear' => $startYear[$key],
                                      'endYear'   => $endYear[$key],
                                      'title'     => $title[$key],
                                      'configid'  => $configuration_id
                                      );
          }

          # prepare work data
          $employer    = $data['creator_form_work']['employer'];
          $startYear   = $data['creator_form_work']['startYear'];
          $endYear     = $data['creator_form_work']['endYear'];
          $description = $data['creator_form_work']['description'];
          $position    = $data['creator_form_work']['position'];

          foreach ($employer as $key => $value) {
            $work_data[] = array('employer'    =>  $value,
                                   'description' => $description[$key],
                                   'startYear'   => $startYear[$key],
                                   'endYear'     => $endYear[$key],
                                   'position'    => $position[$key],
                                   'configid'    => $configuration_id
                                  );
          }

          # prepare skills data
          $name  = $data['creator_form_skills']['name'];
          $level = $data['creator_form_skills']['level'];

          foreach ($name as $key => $value) {
            $skills_data[] = array('name'     => $value,
                                   'level'    => $level[$key],
                                   'configid' => $configuration_id
                                  );
          }

          # prepare languages data
          $name  = $data['creator_form_languages']['name'];
          $level = $data['creator_form_languages']['level'];

          foreach ($name as $key => $value) {
            $languages_data[] = array('name'     => $value,
                                      'level'    => $level[$key],
                                      'configid' => $configuration_id
                                      );
          }

          # prepare certification data
          $name   = $data['creator_form_certification']['name'];
          $school = $data['creator_form_certification']['school'];
          $year   = $data['creator_form_certification']['year'];

          foreach ($name as $key => $value) {
            $certification_data[] = array('name'     => $value,
                                          'school'   => $school[$key],
                                          'year'     => $year[$key],
                                          'configid' => $configuration_id
                                         );
          }

          # inserts
          dibi::query('INSERT INTO [cv_assets_education] %ex', $education_data);
          dibi::query('INSERT INTO [cv_assets_work] %ex', $work_data);
          dibi::query('INSERT INTO [cv_assets_skills] %ex', $skills_data);
          dibi::query('INSERT INTO [cv_assets_language] %ex', $languages_data);
          dibi::query('INSERT INTO [cv_assets_certification] %ex', $certification_data);

        dibi::commit();
      } else return FALSE;
    }

    public static function configurationExists($name, $userid) {
      $test = dibi::query('SELECT [id] FROM [cv_configurations] WHERE [name] = %s AND [userid] = %i', $name, $userid)
        -> fetchSingle();

      if ($test) return TRUE;
      else return FALSE;
    }

  }
?>
