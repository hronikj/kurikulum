<?php
  class Pcskills extends CreatorAsset {
    public $word;
    public $excel;
    public $powerpoint;
    public $outlook;
    public $keyboard;

    public static $fieldset = array('word', 'excel', 'powerpoint', 'outlook', 'keyboard');

    public $validated_word        = FALSE;
    public $validated_excel       = FALSE;
    public $validated_powerpoint  = FALSE;
    public $validated_outlook     = FALSE;
    public $validated_keyboard    = FALSE;

    public $rules_word        = array('word' => 'required');
    public $rules_excel       = array('excel' => 'required');
    public $rules_powerpoint  = array('powerpoint' => 'required');
    public $rules_outlook     = array('outlook' => 'required');
    public $rules_keyboard    = array('keyboard' => 'required');
  }
?>
