<?php
	class Init {
		public function __construct() {
			$this -> parseConfigFile();
			$this -> connectDatabase();
			$this -> startSession();
		}

		private function parseConfigFile() {
			if (file_exists('config.php')) {
				require_once('config.php');

				define('DB_HOST',    $db_config['db_host']);
				define('DB_PORT', 	 $db_config['db_port']);
				define('DB_USER',    $db_config['db_user']);
				define('DB_PASSWD',  $db_config['db_passwd']);
				define('DB_CHARSET', $db_config['db_charset']);
				define('DB_DRIVER',  $db_config['db_driver']);
				define('DB_NAME',    $db_config['db_name']);

				define('SERVER_PATH', 				$server_config['server_path']);
				define('SITE_PATH', 					$server_config['site_path']);
				define('CLASS_PATH',  				$server_config['class_path']);
				define('CONTROLLER_PATH', 		$server_config['controller_path']);
				define('MAIL_TEMPLATES_PATH', $server_config['mail_templates_path']);
				define('PDF_TEMPLATES_PATH',  $server_config['pdf_templates_path']);
				define('TEMPLATES_PATH',			$server_config['templates_path']);

				define('MAIL_SMTPAUTH', 	$mail_config['smtpauth']);
				define('MAIL_HOST', 			$mail_config['host']);
				define('MAIL_SSLHOST', 		$mail_config['sslhost']);
				define('MAIL_SMTPSECURE', $mail_config['smtpsecure']);
				define('MAIL_USERNAME', 	$mail_config['username']);
				define('MAIL_PASSWORD', 	$mail_config['password']);
				define('MAIL_PORT', 			$mail_config['port']);

			} else die ('Config file not found!');
		}

		private function connectDatabase() {
			dibi::connect(array(
			    'driver'   => DB_DRIVER,
			    'host'     => DB_HOST,
					'port'		 => DB_PORT,
			    'username' => DB_USER,
			    'password' => DB_PASSWD,
			    'database' => DB_NAME,
			    'charset'  => DB_CHARSET,
			));
		}

		private function startSession() {
			session_start();
		}
	}
?>
