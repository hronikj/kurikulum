<?php
  class Article {
    public function create($category, $articleData) {
      // check if category exists
      $test = new Category();

      if ($test -> exists($category)) {
        $articleData['categoryid'] = $test -> getId($category);
        return (dibi::query('INSERT INTO [articles]', $articleData)) ? TRUE : FALSE;
      } else return FALSE;
    }

    public function get($articleId) {
      // check if article exists
      if ($this -> exists($articleId)) {
        return dibi::fetch('SELECT * FROM [articles] WHERE [id] = %i', $articleId);
      } else return FALSE;
    }

    public function getListFromCategory($category, $includeRestrictedArticles) {
      // check if category exists
      $test = new Category();
      if ($test -> exists($category)) {
        $categoryId = $test -> getId($category);
        if ($includeRestrictedArticles)
          $articleList = dibi::query('SELECT [headline], [rewrite_url], [perex] FROM [articles] WHERE [categoryid] = %i AND [visibility] >= 0 ', $categoryId);
        else
          $articleList = dibi::query('SELECT [headline], [rewrite_url], [perex] FROM [articles] WHERE [categoryid] = %i AND [visibility] = 0', $categoryId);
        return $articleList -> fetchAll();
      } else return FALSE;
    }

    public function exists($articleId) {
      if (is_numeric($articleId)) {
        $test = dibi::query('SELECT [id] FROM [articles] WHERE [id] = %i', $articleId) -> fetchSingle();
        return ($test) ? TRUE : FALSE;
      } else {
        $test = dibi::query('SELECT [id] FROM [articles] WHERE [rewrite_url] = %s', $articleId) -> fetchSingle();
        return ($test) ? TRUE : FALSE;
      }

    }

    public function delete($articleId) {
      if ($this -> exists($articleId)) {
        dibi::query('DELETE FROM [articles] WHERE [id] = %i', $articleId);
        return TRUE;
      } else return FALSE; // article with that ID does not exist
    }

    public function checkPermissions($articleId) {
      if (!is_numeric($articleId)) $articleId = $this -> getArticleId($articleId);

      if ($articleId && $this -> exists($articleId)) {
        $permission = dibi::query('SELECT [visibility] FROM [articles] WHERE [id] = %i', $articleId) -> fetchSingle();
        return $permission;
      } else return FALSE; // article with that ID does not exist
    }

    public function getArticleId($articleName) {
      $test = dibi::query('SELECT [id] FROM [articles] WHERE [rewrite_url] = %s', $articleName);
      if (count($test) >= 1) {
        return $test -> fetchSingle();
      } else return FALSE;
    }
  }
?>
