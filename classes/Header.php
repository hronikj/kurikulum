<?php
  class Header extends CreatorAsset {
    public $motto;
    public $header;

    public static $fieldset = array('motto', 'header');

    public $validated_motto  = FALSE;
    public $validated_header = FALSE;

    public $rules_motto  = array('motto'  => 'required');
    public $rules_header = array('header' => 'required');

  }
?>
