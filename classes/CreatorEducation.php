<?php
  class CreatorEducation {
    public function validate($data) {
      $return_string = '';
      $degree = $data['creator_form_education_degree'];
      $school = $data['creator_form_education_school'];
      $startYear = $data['creator_form_education_startYear'];
      $endYear   = $data['creator_form_education_endYear'];
      $title     = $data['creator_form_education_title'];

      foreach ($startYear as $key => $value) {
        $test['creator_form_education_startYear'] = $value;
        $validation = Validation::is_valid($test, array(
            'creator_form_education_startYear' => 'required|max_len,4|min_len,4|numeric',
          )
        );

        $_SESSION['validation_startYear'][$key] = $validation;
        if ($validation !== TRUE) {
          foreach ($validation as $key => $value) {  $return_string = $return_string .  $value . ' <br /> ' ; }
        }
      }

      foreach ($endYear as $key => $value) {
        $test['creator_form_education_endYear'] = $value;
        $validation = Validation::is_valid($test, array(
            'creator_form_education_endYear' => 'required|max_len,4|min_len,4|numeric',
          )
        );

        $_SESSION['validation_endYear'][$key] = $validation;
        if ($validation !== TRUE) {
          foreach ($validation as $key => $value) {  $return_string = $return_string .  $value . ' <br /> ' ; }
        }
      }

      foreach ($school as $key => $value) {
        $test['creator_form_education_school'] = $value;
        $validation = Validation::is_valid($test, array(
            'creator_form_education_school' => 'required|max_len,20',
          )
        );

        $_SESSION['validation_school'][$key] = $validation;
        if ($validation !== TRUE) {
          foreach ($validation as $key => $value) { $return_string = $return_string .  $value . ' <br /> ' ; }
        }
      }

      foreach ($title as $key => $value) {
        $test['creator_form_education_title'] = $value;
        $validation = Validation::is_valid($test, array(
            'creator_form_education_title' => 'required|max_len,20',
          )
        );

        $_SESSION['validation_title'][$key] = $validation;
        if ($validation !== TRUE) {
          foreach ($validation as $key => $value) {  $return_string = $return_string .  $value . ' <br /> ' ; }
        }
      }


      return (empty($return_string)) ? TRUE : $return_string;

    }

  }
?>
