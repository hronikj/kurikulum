<?php
	/*
	* Basic configuration file here.
	* Any changes must be also reflected in Init.php class!
	*	MVC is used, so let just be:
	* Models 			-> classes/
	*	Controllers -> controllers/
	*	Views 			-> templates/
	*/

	$db_config = array(
		'db_host' 		=> 'localhost',
		'db_port'			=> '3306',
		'db_user' 		=> 'root',
		'db_passwd' 	=> 'root',
		'db_name' 		=> 'kurikulum',
		'db_driver' 	=> 'mysql',
		'db_charset' 	=> 'utf8'
	);

  $server_config_root = $_SERVER['DOCUMENT_ROOT'] . '/';

	$server_config = array(
		'server_path' 				=> $server_config_root,
		'site_path'						=> '',
    'class_path'  				=> $server_config_root . 'classes/',
		'controller_path' 		=> $server_config_root . 'controllers/',
		'templates_path'			=> $server_config_root . 'templates/',
		'mail_templates_path' => $server_config_root . 'templates/mail/',
		'pdf_templates_path'	=> $server_config_root . 'templates/pdf/'
	);

	$mail_config = array(
		'smtpauth' 	 => TRUE,
		'host'		 	 => 'smtp.gmail.com',
		'sslhost'		 => 'ssl://smtp.gmail.com',
		'smtpsecure' => 'ssl',
		'username'	 => 'jiri.hronik@gmail.com',
		'password'	 => 'Saphire218449319',
		'port'			 => '465'
	);

?>
